#include "catch.hpp"
#include "matchers.hpp"

#include "rays.hpp"
#include "transforms.hpp"
#include "worldobjects.hpp"

SCENARIO("creating and querying a ray", "[Ray]")
{
    GIVEN("an origin and a direction")
    {
        auto origin = RT::Tuple::Point(1., 2., 3.);
        auto direction = RT::Tuple::Vector(4., 5., 6.);
        WHEN("used to create a ray")
        {
            auto r = RT::Ray{origin, direction};
            THEN("the ray can be queried")
            {
                REQUIRE_THAT(r.origin(), IsEqual(origin));
                REQUIRE_THAT(r.direction(), IsEqual(direction));
            }
        }
    }
}

SCENARIO("transforming a ray", "[Ray][Transform]")
{
    GIVEN("r = Ray(point(1,2,3),vector(0,1,0))")
    {
        auto r = RT::Ray{RT::Tuple::Point(1., 2., 3.),
                         RT::Tuple::Vector(0., 1., 0.)};
        AND_GIVEN("m = translation(3,4,5)")
        {
            auto m = RT::Transform::Translation(3., 4., 5.);
            WHEN("r2 = r * m")
            {
                auto r2 = r * m;
                THEN("r2.origin() == point(4,6,8)")
                {
                    REQUIRE_THAT(r2.origin(),
                                 IsEqual(RT::Tuple::Point(4., 6., 8.)));
                }
                THEN("r2.direction() == vector(0,1,0)")
                {
                    REQUIRE_THAT(r2.direction(),
                                 IsEqual(RT::Tuple::Vector(0., 1., 0.)));
                }
            }
        }
        AND_GIVEN("m = scaling(2,3,4)")
        {
            auto m = RT::Transform::Scaling(2., 3., 4.);
            WHEN("r2 = r * m")
            {
                auto r2 = r * m;
                THEN("r2.origin() == point(2,6,12)")
                {
                    REQUIRE_THAT(r2.origin(),
                                 IsEqual(RT::Tuple::Point(2., 6., 12.)));
                }
                THEN("r2.direction() == vector(0,3,0)")
                {
                    REQUIRE_THAT(r2.direction(),
                                 IsEqual(RT::Tuple::Vector(0., 3., 0.)));
                }
            }
        }
    }
}

