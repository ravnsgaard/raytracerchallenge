#include "catch.hpp"
#include "matchers.hpp"

#include "tuples.hpp"

using namespace Catch::Matchers;

namespace Catch
{
template<>
struct StringMaker<::RT::Tuple>
{
    static std::string convert(::RT::Tuple const & value)
    {
        std::ostringstream ss;
        ss << '[' << value(0) << ", " << value(1) << ", " << value(2) << ", "
           << value(3) << ']';
        return ss.str();
    }
};
} // namespace Catch

SCENARIO("A Tuple with w=1.0 is a point", "[Tuple]")
{
    GIVEN("A Tuple 'a'")
    {
        RT::Tuple a{4.3, -4.2, 3.1, 1.};
        THEN("a's scalars can be accessed")
        {
            REQUIRE_THAT(a(0), IsEqual(4.3));
            REQUIRE_THAT(a(1), IsEqual(-4.2));
            REQUIRE_THAT(a(2), IsEqual(3.1));
            REQUIRE_THAT(a(3), IsEqual(1.));
        }
        AND_THEN("a is a point, not a vector")
        {
            REQUIRE(a.isPoint());
            REQUIRE_FALSE(a.isVector());
        }
    }
}

SCENARIO("A Tuple with w=0.0 is a vector", "[Tuple]")
{
    GIVEN("A Tuple 'a'")
    {
        RT::Tuple a{4.3l, -4.2l, 3.1l, 0.};
        THEN("a's last member is zero")
        {
            REQUIRE_THAT(a(3), IsEqual(0.l));
        }
        AND_THEN("a is a vector, not a point")
        {
            REQUIRE(a.isVector());
            REQUIRE_FALSE(a.isPoint());
        }
    }
}

SCENARIO("Tuple::Point() creates tuples with w = 1", "[Tuple]")
{
    GIVEN("p = Point()")
    {
        auto p = RT::Tuple::Point(4.l, -4.l, 3.l);
        THEN("p is a Tuple with w = 1")
        {
            REQUIRE_THAT(p, IsEqual(RT::Tuple{4.l, -4.l, 3.l, 1.l}));
            REQUIRE(p.isPoint());
        }
    }
}

SCENARIO("Tuple::Vector() creates Tuples with w = 0", "[Tuple]")
{
    GIVEN("v = Vector()")
    {
        auto v = RT::Tuple::Vector(4.l, -4.l, 3.l);
        THEN("v is a Tuple with w = 0")
        {
            REQUIRE_THAT(v, IsEqual(RT::Tuple{4.l, -4.l, 3.l, 0.l}));
            REQUIRE(v.isVector());
        }
    }
}

SCENARIO("adding two Tuples", "[Tuple]")
{
    GIVEN("a Tuple 'a1', which is a point")
    {
        RT::Tuple a1{3.l, -2.l, 5.l, 1.l};
        AND_GIVEN("a Tuple 'a2', which is a vector")
        {
            RT::Tuple a2{-2.l, 3.l, 1.l, 0.l};
            THEN("a1 + a2 gives a Tuple with correct sum, which is a point")
            {
                auto a3 = a1 + a2;
                REQUIRE_THAT(a3, IsEqual(RT::Tuple{1.l, 1.l, 6.l, 1.l}));
                REQUIRE(a3.isPoint());
            }
        }
    }

    GIVEN("a Tuple 'a1', which is a vector")
    {
        RT::Tuple a1{3.l, -2.l, 5.l, 0.l};
        AND_GIVEN("a Tuple 'a2', which is a vector")
        {
            RT::Tuple a2{1.l, 2.l, 3.l, 0.l};
            THEN("a1 + a2 gives a Tuple with correct sum, which is a vector")
            {
                auto a3 = a1 + a2;
                REQUIRE_THAT(a3, IsEqual(RT::Tuple{4.l, 0.l, 8.l, 0.l}));
                REQUIRE(a3.isVector());
            }
        }
    }

    GIVEN("a Tuple 'a1', which is a point")
    {
        RT::Tuple a1{3.l, -2.l, 5.l, 1.l};
        AND_GIVEN("a Tuple 'a2', which is a point")
        {
            RT::Tuple a2{-1.l, -2.l, -3.l, 1.l};
            THEN("a1 + a2 gives a Tuple with correct sum, which is neither a "
                 "vector nor a point")
            {
                auto a3 = a1 + a2;
                REQUIRE_THAT(a3, IsEqual(RT::Tuple{2.l, -4.l, 2.l, 2.l}));
                REQUIRE_FALSE(a3.isVector());
                REQUIRE_FALSE(a3.isPoint());
            }
        }
    }
}

SCENARIO("subtracting two points", "[Tuple]")
{
    GIVEN("a point 'p1'")
    {
        auto p1 = RT::Tuple::Point(3., 2., 1.);
        AND_GIVEN("a point 'p2'")
        {
            auto p2 = RT::Tuple::Point(5., 6., 7.);
            THEN("p1 - p2 gives a vector from p2 to p1")
            {
                auto p3 = p1 - p2;
                REQUIRE_THAT(p3, IsEqual(RT::Tuple::Vector(-2., -4., -6.)));
            }
        }
    }
}

SCENARIO("subtracting a vector and a point", "[Tuple]")
{
    GIVEN("a point 'p'")
    {
        auto p = RT::Tuple::Point(3., 2., 1.);
        AND_GIVEN("a vector 'v'")
        {
            auto v = RT::Tuple::Vector(5., 6., 7.);
            THEN("p - v gives a new point")
            {
                auto p3 = p - v;
                REQUIRE(p3.isPoint());
                REQUIRE_THAT(p3, IsEqual(RT::Tuple::Point(-2., -4., -6.)));
            }
            AND_THEN("v - p gives a Tuple, which is neither point nor vector")
            {
                auto p4 = v - p;
                REQUIRE_FALSE(p4.isPoint());
                REQUIRE_FALSE(p4.isVector());
                REQUIRE_THAT(p4, IsEqual(RT::Tuple{2., 4., 6., -1.}));
            }
        }
    }
}

SCENARIO("subtracting two vectors", "[Tuple]")
{
    GIVEN("a vector 'v1'")
    {
        auto v1 = RT::Tuple::Vector(3., 2., 1.);
        AND_GIVEN("a vector 'v2'")
        {
            auto v2 = RT::Tuple::Vector(5., 6., 7.);
            THEN("v1 - v2 is a vector")
            {
                auto v3 = v1 - v2;
                REQUIRE(v3.isVector());
                REQUIRE_THAT(v3, IsEqual(RT::Tuple::Vector(-2., -4., -6.)));
            }
        }
    }
}

SCENARIO("negating a Tuple", "[Tuple]")
{
    GIVEN("a Tuple 'a'")
    {
        RT::Tuple a{1., -2., 3., -4.};
        THEN("-a is the negated Tuple")
        {
            REQUIRE_THAT(-a, IsEqual(RT::Tuple{-1., 2., -3., 4.}));
        }
    }
}

SCENARIO("multiplying a Tuple by a scalar", "[Tuple]")
{
    GIVEN("a Tuple 'a'")
    {
        RT::Tuple a{1., -2., 3., -4.};
        AND_GIVEN("a scalar 's'")
        {
            auto s = 3.5;
            WHEN("r1 = a * s")
            {
                auto r1 = a * s;
                THEN("r1 is a correctly scaled Tuple")
                {
                    REQUIRE_THAT(r1, IsEqual(RT::Tuple{3.5, -7., 10.5, -14.}));
                }
                AND_WHEN("r2 = s * a")
                {
                    auto r2 = s * a;
                    THEN("r1 == r2")
                    {
                        REQUIRE_THAT(r1, IsEqual(r2));
                    }
                }
            }
        }
        AND_GIVEN("a fractional scalar 'f'")
        {
            auto f = 0.5;
            WHEN("r1 = a * f")
            {
                auto r1 = a * f;
                THEN("r1 is a correctly (down)scaled Tuple")
                {
                    REQUIRE_THAT(r1, IsEqual(RT::Tuple{.5, -1., 1.5, -2.}));
                }
            }
        }
    }
}

SCENARIO("Dividing a Tuple by a scalar", "[Tuple]")
{
    GIVEN("a Tuple 'a'")
    {
        RT::Tuple a{1., -2., 3., -4.};
        THEN("a / 2 gives a \"half-size\" Tuple.")
        {
            auto r = a / 2.;
            REQUIRE_THAT(r, IsEqual(RT::Tuple{.5, -1., 1.5, -2.}));
        }
    }
}

SCENARIO("computing the magnitude of a vector", "[Tuple]")
{
    GIVEN("a vector 'v' <1,0,0>")
    {
        auto v = RT::Tuple::Vector(1., 0., 0.);
        THEN("v's magnitude is 1")
        {
            REQUIRE_THAT(v.norm(), IsEqual(1.));
        }
    }
    GIVEN("a vector 'v' <0,1,0>")
    {
        auto v = RT::Tuple::Vector(0., 1., 0.);
        THEN("v's magnitude is 1")
        {
            REQUIRE_THAT(v.norm(), IsEqual(1.));
        }
    }
    GIVEN("a vector 'v' <0,0,1>")
    {
        auto v = RT::Tuple::Vector(0., 0., 1.);
        THEN("v's magnitude is 1")
        {
            REQUIRE_THAT(v.norm(), IsEqual(1.));
        }
    }
    GIVEN("a vector 'v' <1,2,3>")
    {
        auto v = RT::Tuple::Vector(1., 2., 3.);
        THEN("v's magnitude is sqrt(14)")
        {
            REQUIRE_THAT(v.norm(), IsEqual(std::sqrt(14.)));
        }
        AND_THEN("-v's magnitude is sqrt(14)")
        {
            REQUIRE_THAT((-v).norm(), IsEqual(std::sqrt(14.)));
        }
    }
}

SCENARIO("normalizing a vector", "[Tuple]")
{
    GIVEN("a vector 'v' <4,0,0>")
    {
        auto v = RT::Tuple::Vector(4., 0., 0.);
        WHEN("v is normalized")
        {
            v.normalize();
            THEN("v is <1,0,0>")
            {
                REQUIRE_THAT(v, IsEqual(RT::Tuple::Vector(1., 0., 0.)));
            }
            AND_THEN("v is a unit vector")
            {
                REQUIRE_THAT(v.norm(), IsEqual(1.));
            }
        }
    }
    GIVEN("a vector 'v1' <1,2,3>")
    {
        auto v1 = RT::Tuple::Vector(1., 2., 3.);
        WHEN("v2 is v1 normalized")
        {
            auto v2 = v1.normalized();
            THEN("v2 is <1/sqrt(14),2/sqrt(14),3/sqrt(14)>")
            {
                REQUIRE_THAT(v2,
                             IsEqual(RT::Tuple::Vector(1. / std::sqrt(14.),
                                                       2. / std::sqrt(14.),
                                                       3. / std::sqrt(14.))));
            }
            AND_THEN("v2 is a unit vector")
            {
                REQUIRE_THAT(v2.norm(), IsEqual(1.));
            }
        }
    }
}

SCENARIO("dot product of two tuples", "[Tuple]")
{
    GIVEN("a vector 'a' <1,2,3>")
    {
        auto a = RT::Tuple::Vector(1., 2., 3.);
        AND_GIVEN("a vector 'b' <2,3,4>")
        {
            auto b = RT::Tuple::Vector(2., 3., 4.);
            THEN("the dot product of a and b is 20")
            {
                REQUIRE_THAT(a.dot(b), IsEqual(20.));
            }
        }
    }
}

SCENARIO("cross product of two vectors", "[Tuple]")
{
    GIVEN("a vector 'a' <1,2,3>")
    {
        auto a = RT::Tuple::Vector(1., 2., 3.);
        AND_GIVEN("a vector 'b' <2,3,4>")
        {
            auto b = RT::Tuple::Vector(2., 3., 4.);
            THEN("the cross product of a and b is <-1,2,-1>")
            {
                REQUIRE_THAT(a.cross(b),
                             IsEqual(RT::Tuple::Vector(-1., 2., -1.)));
            }
            THEN("the cross product of b and a is <1,-2,1>")
            {
                REQUIRE_THAT(b.cross(a),
                             IsEqual(RT::Tuple::Vector(1., -2., 1.)));
            }
        }
    }
}

SCENARIO("iterator access to elements", "[Tuple]")
{
    GIVEN("a Tuple 't' <1,2,3,4>")
    {
        RT::Tuple t{1., 2., 3., 4.};
        WHEN("iterating over the Tuple")
        {
            auto iter = std::cbegin(t);
            THEN("each value can be accessed")
            {
                REQUIRE_THAT(*iter, IsEqual(1.));
                ++iter;
                REQUIRE_THAT(*iter, IsEqual(2.));
                ++iter;
                REQUIRE_THAT(*iter, IsEqual(3.));
                ++iter;
                REQUIRE_THAT(*iter, IsEqual(4.));
                ++iter;
                AND_THEN("the end compares equal")
                {
                    REQUIRE(iter == std::cend(t));
                }
            }
        }
    }
}

SCENARIO("reflecting a vector approaching at 45°", "[Tuple]")
{
    GIVEN("v = vector(1,-1,0)")
    {
        auto v = RT::Tuple::Vector(1., -1., 0.);
        AND_GIVEN("n = vector(0,1,0)")
        {
            auto n = RT::Tuple::Vector(0., 1., 0.);
            WHEN("r = v.reflect(n)")
            {
                auto r = v.reflect(n);
                THEN("r == vector(1,1,0)")
                {
                    REQUIRE_THAT(r, IsEqual(RT::Tuple::Vector(1., 1., 0.)));
                }
            }
        }
    }
}

SCENARIO("reflecting a vector off a slanted surface", "[Tuple]")
{
    GIVEN("v = vector(0,-1,0)")
    {
        auto v = RT::Tuple::Vector(0., -1., 0.);
        AND_GIVEN("n = vector(sqrt(2)/2, sqrt(2)/2, 0)")
        {
            auto n = RT::Tuple::Vector(std::sqrt(2.) / 2., std::sqrt(2.) / 2.,
                                       0.);
            WHEN("r = v.reflect(n)")
            {
                auto r = v.reflect(n);
                THEN("r == vector(1,0,0)")
                {
                    REQUIRE_THAT(r, IsEqual(RT::Tuple::Vector(1., 0., 0.)));
                }
            }
        }
    }
}

