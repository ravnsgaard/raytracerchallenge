#include "catch.hpp"
#include "matchers.hpp"

#include "materials.hpp"

SCENARIO("the default material", "[Material]")
{
    GIVEN("m = Material()")
    {
        auto m = RT::Material{};
        THEN("m.color() == white")
        {
            REQUIRE_THAT(m.color(), IsEqual(RT::Color::White()));
        }
        THEN("m.ambient() == 0.1")
        {
            REQUIRE_THAT(m.ambient(), IsEqual(0.1));
        }
        THEN("m.diffuse() == 0.9")
        {
            REQUIRE_THAT(m.diffuse(), IsEqual(0.9));
        }
        THEN("m.specular() == 0.9")
        {
            REQUIRE_THAT(m.specular(), IsEqual(0.9));
        }
        THEN("m.shininess() == 200")
        {
            REQUIRE_THAT(m.shininess(), IsEqual(200.));
        }
    }
}

SCENARIO("reflectivity for the default material", "[Material]")
{
    GIVEN("m = Material()")
    {
        auto m = RT::Material{};
        THEN("m.reflective = 0.0")
        {
            REQUIRE_THAT(m.reflective(), IsEqual(0.));
        }
    }
}

SCENARIO("transparency and refractive index for the default material", "[Material][Refraction]")
{
    GIVEN("m = material()")
    {
        auto m = RT::Material{};
        THEN("m.transparency = 0.0")
        {
            REQUIRE_THAT(m.transparency(), IsEqual(0.));
        }
        THEN("m.refractiveIndex = 1.0")
        {
            REQUIRE_THAT(m.refractiveIndex(), IsEqual(1.));
        }
    }
}
