#include "catch.hpp"
#include "matchers.hpp"

#include "matrices.hpp"

SCENARIO("constructing and inspecting a 4x4 matrix", "[Matrix4]")
{
    GIVEN("an order 4 matrix m")
    {
        RT::Matrix4 m{1.l, 2.l,  3.l,  4.l,  5.5l,  6.5l,  7.5l,  8.5l,
                      9.l, 10.l, 11.l, 12.l, 13.5l, 14.5l, 15.5l, 16.5l};
        THEN("access works")
        {
            REQUIRE_THAT(m(0, 0), IsEqual(1.l));
            REQUIRE_THAT(m(0, 3), IsEqual(4.l));
            REQUIRE_THAT(m(1, 0), IsEqual(5.5l));
            REQUIRE_THAT(m(1, 2), IsEqual(7.5l));
            REQUIRE_THAT(m(2, 2), IsEqual(11.l));
            REQUIRE_THAT(m(3, 0), IsEqual(13.5l));
            REQUIRE_THAT(m(3, 2), IsEqual(15.5l));
        }
    }
}

SCENARIO("a 2x2 matrix should be representable", "[Matrix2]")
{
    GIVEN("a 2x2 matrix 'm'")
    {
        RT::Matrix2 m{-3.l, 5.l, 1.l, -2.l};
        THEN("access works")
        {
            REQUIRE_THAT(m(0, 0), IsEqual(-3.l));
            REQUIRE_THAT(m(0, 1), IsEqual(5.l));
            REQUIRE_THAT(m(1, 0), IsEqual(1.l));
            REQUIRE_THAT(m(1, 1), IsEqual(-2.l));
        }
    }
}

SCENARIO("a 3x3 matrix should be representable", "[Matrix3]")
{
    GIVEN("a 3x3 matrix 'm'")
    {
        RT::Matrix3 m{-3.l, 5.l, 0.l, 1.l, -2.l, -7.l, 9.l, 1.l, 1.l};
        THEN("access works")
        {
            REQUIRE_THAT(m(0, 0), IsEqual(-3.l));
            REQUIRE_THAT(m(1, 1), IsEqual(-2.l));
            REQUIRE_THAT(m(2, 2), IsEqual(1.l));
            REQUIRE_THAT(m(2, 0), IsEqual(9.l));
        }
    }
}

SCENARIO("matrix equality", "[Matrix4]")
{
    GIVEN("a matrix 'a'")
    {
        RT::Matrix4 a{1.l, 2.l, 3.l, 4.l, 5.l, 6.l, 7.l, 8.l,
                      9.l, 8.l, 7.l, 6.l, 5.l, 4.l, 3.l, 2.l};
        AND_GIVEN("an identical matrix 'b'")
        {
            RT::Matrix4 b{1.l, 2.l, 3.l, 4.l, 5.l, 6.l, 7.l, 8.l,
                          9.l, 8.l, 7.l, 6.l, 5.l, 4.l, 3.l, 2.l};
            THEN("a compares equal to b")
            {
                REQUIRE_THAT(a, IsEqual(b));
            }
        }
        AND_GIVEN("a different matrix 'c'")
        {
            RT::Matrix4 c{1.l, 0.l, 0.l, 0.l, 0.l, 1.l, 0.l, 0.l,
                          0.l, 0.l, 1.l, 0.l, 0.l, 0.l, 0.l, 1.l};
            THEN("a does not compare equal to c")
            {
                REQUIRE_THAT(a, IsNotEqual(c));
            }
        }
    }
}

SCENARIO("Matrix2 equality", "[Matrix2]")
{
    GIVEN("a matrix 'a'")
    {
        RT::Matrix2 a{1.l, 2.l, 3.l, 4.l};
        AND_GIVEN("an identical matrix 'b'")
        {
            RT::Matrix2 b{1.l, 2.l, 3.l, 4.l};
            THEN("a compares equal to b")
            {
                REQUIRE_THAT(a, IsEqual(b));
            }
        }
        AND_GIVEN("a different matrix 'c'")
        {
            RT::Matrix2 c{1.l, 0.l, 0.l, 1.l};
            THEN("a does not compare equal to c")
            {
                REQUIRE_THAT(a, IsNotEqual(c));
            }
        }
    }
}

SCENARIO("Matrix3 equality", "[Matrix3]")
{
    GIVEN("a matrix 'a'")
    {
        RT::Matrix3 a{1.l, 2.l, 3.l, 4.l, 5.l, 6.l, 7.l, 8.l, 9.l};
        AND_GIVEN("an identical matrix 'b'")
        {
            RT::Matrix3 b{1.l, 2.l, 3.l, 4.l, 5.l, 6.l, 7.l, 8.l, 9.l};
            THEN("a compares equal to b")
            {
                REQUIRE_THAT(a, IsEqual(b));
            }
        }
        AND_GIVEN("a different matrix c")
        {
            RT::Matrix3 c{1.l, 0.l, 0.l, 0.l, 1.l, 0.l, 0.l, 0.l, 1.l};
            THEN("a does not compare equal to c")
            {
                REQUIRE_THAT(a, IsNotEqual(c));
            }
        }
    }
}

SCENARIO("multiplying matrices", "[Matrix4]")
{
    GIVEN("a matrix 'a'")
    {
        RT::Matrix4 a{1.l, 2.l, 3.l, 4.l, 5.l, 6.l, 7.l, 8.l,
                      9.l, 8.l, 7.l, 6.l, 5.l, 4.l, 3.l, 2.l};
        AND_GIVEN("a different matrix 'b'")
        {
            RT::Matrix4 b{-2.l, 1.l, 2.l, 3.l, 3.l, 2.l, 1.l, -1.l,
                          4.l,  3.l, 6.l, 5.l, 1.l, 2.l, 7.l, 8.l};
            WHEN("matrix 'c' is "
                 "<20,22,50,48,44,54,114,108,40,58,110,102,16,26,46,42>")
            {
                RT::Matrix4 c{20.l,  22.l,  50.l, 48.l, 44.l,  54.l,
                              114.l, 108.l, 40.l, 58.l, 110.l, 102.l,
                              16.l,  26.l,  46.l, 42.l};
                THEN("a * b == c")
                {
                    REQUIRE_THAT(a * b, IsEqual(c));
                }
            }
        }
        AND_GIVEN("the identity matrix 'i'")
        {
            auto i = RT::Matrix4::Identity();
            THEN("a * i == a")
            {
                REQUIRE_THAT(a * i, IsEqual(a));
                REQUIRE_THAT(i * a, IsEqual(a));
            }
        }
        AND_GIVEN("a tuple 'b'")
        {
            RT::Tuple b{1.l, 2.l, 3.l, 1.l};
            THEN("a * b gives a new Tuple")
            {
                REQUIRE_THAT(a * b,
                             IsEqual(RT::Tuple{18.l, 46.l, 52.l, 24.l}));
            }
        }
    }
}

SCENARIO("Transposing a matrix", "[Matrix4]")
{
    GIVEN("a matrix 'a'")
    {
        RT::Matrix4 a{0.l, 9.l, 3.l, 0.l, 9.l, 8.l, 0.l, 8.l,
                      1.l, 8.l, 5.l, 3.l, 0.l, 0.l, 5.l, 8.l};
        THEN("the transpose matrix has columns and rows interchanged")
        {
            REQUIRE_THAT(a.transposed(),
                         IsEqual(RT::Matrix4{0.l, 9.l, 1.l, 0.l, 9.l, 8.l, 8.l,
                                             0.l, 3.l, 0.l, 5.l, 5.l, 0.l, 8.l,
                                             3.l, 8.l}));
        }
        AND_WHEN("a is transposed in-place")
        {
            a.transpose();
            THEN("a has its columns and rows interchanged")
            {
                REQUIRE_THAT(a,
                             IsEqual(RT::Matrix4{0.l, 9.l, 1.l, 0.l, 9.l, 8.l,
                                                 8.l, 0.l, 3.l, 0.l, 5.l, 5.l,
                                                 0.l, 8.l, 3.l, 8.l}));
            }
        }
    }
    GIVEN("the identity matrix")
    {
        THEN("the transpose is still the idenity matrix")
        {
            REQUIRE_THAT(RT::Matrix4::Identity().transposed(),
                         IsEqual(RT::Matrix4::Identity()));
        }
    }
}

SCENARIO("calculating the determinant of a 2x2 matrix", "[Matrix2]")
{
    GIVEN("a matrix 'a' <1,5,-3,2>")
    {
        RT::Matrix2 a{1.l, 5.l, -3.l, 2.l};
        THEN("the determinant is 17")
        {
            REQUIRE_THAT(a.determinant(), IsEqual(17.l));
        }
    }
}

SCENARIO("a submatrix of an order-3 matrix is an order-2 matrix",
         "[Matrix2][Matrix3]")
{
    GIVEN("an order-3 matrix 'a'")
    {
        RT::Matrix3 a{1.l, 5.l, 0.l, -3.l, 2.l, 7.l, 0.l, 6.l, -3.l};
        WHEN("deleting the first row and the third column")
        {
            auto b = a.submatrix(0, 2);
            THEN("the result is a submatrix of a")
            {
                REQUIRE_THAT(b, IsEqual(RT::Matrix2{-3.l, 2.l, 0.l, 6.l}));
            }
        }
    }
}

SCENARIO("a submatrix of an order-4 matrix is an order-3 matrix",
         "[Matrix4][Matrix3]")
{
    GIVEN("an order-4 matrix 'a'")
    {
        RT::Matrix4 a{-6.l, 1.l, 1.l, 6.l, -8.l, 5.l, 8.l,  6.l,
                      -1.l, 0.l, 8.l, 2.l, -7.l, 1.l, -1.l, 1.l};
        WHEN("deleting the third row and the second column")
        {
            auto b = a.submatrix(2, 1);
            THEN("the result is a submatrix of a")
            {
                REQUIRE_THAT(b,
                             IsEqual(RT::Matrix3{-6.l, 1.l, 6.l, -8.l, 8.l,
                                                 6.l, -7.l, -1.l, 1.l}));
            }
        }
    }
}

SCENARIO("calculating the minor of an order-3 matrix", "[Matrix3]")
{
    GIVEN("an order-3 matrix 'a'")
    {
        RT::Matrix3 a{3.l, 5.l, 0.l, 2.l, -1.l, -7.l, 6.l, -1.l, 5.l};
        AND_GIVEN("'b' is 'a's submatrix at i,j=2,1")
        {
            auto b = a.submatrix(1, 0);
            THEN("'b's determinant is 25")
            {
                REQUIRE_THAT(b.determinant(), IsEqual(25.l));
            }
        }
        THEN("'a's minor at i,j=2,1 is 25")
        {
            REQUIRE_THAT(a.minorAt(1, 0), IsEqual(25.l));
        }
    }
}

SCENARIO("calculating a cofactor of an order-3 matrix", "[Matrix3]")
{
    GIVEN("an order-3 matrix 'a'")
    {
        RT::Matrix3 a{3.l, 5.l, 0.l, 2.l, -1.l, -7.l, 6.l, -1.l, 5.l};
        WHEN("i,j = 1,1")
        {
            RT::Matrix3::size_type i = 0, j = 0;
            THEN("a.minorAt(i, j) == -12")
            {
                REQUIRE_THAT(a.minorAt(i, j), IsEqual(-12.l));
            }
            AND_THEN("a.cofactorAt(i, j) == -12")
            {
                REQUIRE_THAT(a.cofactorAt(i, j), IsEqual(-12.l));
            }
        }
        AND_WHEN("i,j = 2,1")
        {
            RT::Matrix3::size_type i = 1, j = 0;
            THEN("a.minorAt(i,j) == 25")
            {
                REQUIRE_THAT(a.minorAt(i, j), IsEqual(25.l));
            }
            AND_THEN("a.cofactorAt(i,j) == -25")
            {
                REQUIRE_THAT(a.cofactorAt(i, j), IsEqual(-25.l));
            }
        }
    }
}

SCENARIO("calculating the determinant of an order-3 matrix", "[Matrix3]")
{
    GIVEN("a matrix 'a'")
    {
        RT::Matrix3 a{1.l, 2.l, 6.l, -5.l, 8.l, -4.l, 2.l, 6.l, 4.l};
        THEN("cofactors of the first row are 56, 12, and -46")
        {
            REQUIRE_THAT(a.cofactorAt(0, 0), IsEqual(56.l));
            REQUIRE_THAT(a.cofactorAt(0, 1), IsEqual(12.l));
            REQUIRE_THAT(a.cofactorAt(0, 2), IsEqual(-46.l));
        }
        AND_THEN("the determinant of 'a' is -196")
        {
            REQUIRE_THAT(a.determinant(), IsEqual(-196.l));
        }
    }
}

SCENARIO("calculating the determinant of an order-4 matrix", "[Matrix4]")
{
    GIVEN("a matrix 'a'")
    {
        RT::Matrix4 a{-2.l, -8.l, 3.l,  5.l, -3.l, 1.l, 7.l, 3.l,
                      1.l,  2.l,  -9.l, 6.l, -6.l, 7.l, 7.l, -9.l};
        THEN("cofactors of the first row are 690, 447, 210, and 51")
        {
            REQUIRE_THAT(a.cofactorAt(0, 0), IsEqual(690.l));
            REQUIRE_THAT(a.cofactorAt(0, 1), IsEqual(447.l));
            REQUIRE_THAT(a.cofactorAt(0, 2), IsEqual(210.l));
            REQUIRE_THAT(a.cofactorAt(0, 3), IsEqual(51.l));
        }
        AND_THEN("the determinant is -4071")
        {
            REQUIRE_THAT(a.determinant(), IsEqual(-4071.l));
        }
    }
}

SCENARIO("testing an invertible matrix for invertibility", "[Matrix4]")
{
    GIVEN("an invertible matrix 'a'")
    {
        RT::Matrix4 a{6.l, 4.l,  4.l, 4.l,  5.l, 5.l, 7.l, 6.l,
                      4.l, -9.l, 3.l, -7.l, 9.l, 1.l, 7.l, -6.l};
        THEN("the determinant is non-zero")
        {
            REQUIRE_THAT(a.determinant(), IsEqual(-2120.l));

            AND_THEN("the matrix is invertible")
            {
                REQUIRE(a.isInvertible());
            }
        }
    }
}

SCENARIO("testing a non-invertible matrix for invertibility", "[Matrix4]")
{
    GIVEN("a non-invertible matrix 'a'")
    {
        RT::Matrix4 a{-4.l, 2.l,  -2.l, -3.l, 9.l, 6.l, 2.l, 6.l,
                      0.l,  -5.l, 1.l,  -5.l, 0.l, 0.l, 0.l, 0.l};
        THEN("the determinant is zero")
        {
            REQUIRE_THAT(a.determinant(), IsEqual(0.l));

            AND_THEN("the matrix is not invertible")
            {
                REQUIRE_FALSE(a.isInvertible());
            }
        }
    }
}

SCENARIO("calculating the inverse of a matrix", "[Matrix4]")
{
    GIVEN("an order-4 matrix 'a'")
    {
        RT::Matrix4 a{-5.l, 2.l, 6.l,  -8.l, 1.l, -5.l, 1.l, 8.l,
                      7.l,  7.l, -6.l, -7.l, 1.l, -3.l, 7.l, 4.l};
        THEN("'a's determinant is 532")
        {
            CHECK_THAT(a.determinant(), IsEqual(532.l));
        }
        WHEN("'b' is the inverse of 'a'")
        {
            auto b = a.inversed();
            THEN("inspection reveals details about the inverse")
            {
                CHECK_THAT(a.cofactorAt(2, 3), IsEqual(-160.));
                REQUIRE_THAT(b(3, 2), IsEqual(-160. / 532.));

                CHECK_THAT(a.cofactorAt(3, 2), IsEqual(105.));
                REQUIRE_THAT(b(2, 3), IsEqual(105. / 532.));
            }
            AND_WHEN("a is multiplied by its inverse")
            {
                auto c = a * b;
                THEN("the product is the identity matrix")
                {
                    REQUIRE_THAT(c.determinant(), IsEqual(1.));
                    REQUIRE_THAT(c(0, 0), IsEqual(1.));
                    REQUIRE_THAT(c(1, 1), IsEqual(1.));
                }
            }
        }
    }
}

SCENARIO("calculating the inverse of another matrix", "[Matrix4]")
{
    GIVEN("another order-4 matrix 'a'")
    {
        RT::Matrix4 a{8.,  -5., 9., 2., 7.,  5., 6.,  1.,
                      -6., 0.,  9., 6., -3., 0., -9., -4.};
        THEN("a's determinant is")
        {
            REQUIRE_THAT(a.determinant(), IsEqual(-585.));
        }
        WHEN("b is the inverse of a")
        {
            auto b = a.inversed();
            THEN("we test a selection of elements")
            {
                CHECK_THAT(a.cofactorAt(0, 0), IsEqual(90.));
                REQUIRE_THAT(b(0, 0), IsEqual(90. / -585.));
                CHECK_THAT(a.cofactorAt(1, 2), IsEqual(-210.));
                REQUIRE_THAT(b(2, 1), IsEqual(-210. / -585.));
                CHECK_THAT(a.cofactorAt(3, 3), IsEqual(1125.));
                REQUIRE_THAT(b(3, 3), IsEqual(1125. / -585.));
            }
        }
    }
}

SCENARIO("multiplying a product by its inverse", "[Matrix4]")
{
    GIVEN("an order-4 matrix 'a'")
    {
        RT::Matrix4 a{3.,  -9., 7., 3., 3.,  -8., 2.,  -9.,
                      -4., 4.,  4., 1., -6., 5.,  -1., 1.};
        AND_GIVEN("another order-4 matrix 'b'")
        {
            RT::Matrix4 b{8., 2., 2., 2., 3., -1., 7., 0.,
                          7., 0., 5., 4., 6., -2., 0., 5.};
            WHEN("c = a * b")
            {
                auto c = a * b;
                THEN("c * b.inversed() == a")
                {
                    REQUIRE_THAT(c * b.inversed(), IsEqual(a));
                }
            }
        }
    }
}

SCENARIO("inverting the identity matrix")
{
    GIVEN("the identity matrix 'i'")
    {
        auto i = RT::Matrix4::Identity();
        THEN("'i's determinant is 1")
        {
            REQUIRE_THAT(i.determinant(), IsEqual(1.));
        }
        THEN("'i's inverse is also the identity matrix")
        {
            REQUIRE_THAT(i.inversed(), IsEqual(i));
        }
    }
}

