#include "catch.hpp"
#include "matchers.hpp"

#include "canvas.hpp"

SCENARIO("creating a canvas", "[Canvas]")
{
    GIVEN("a new canvas 'c'")
    {
        RT::Canvas c{10, 20};
        THEN("height and width matches")
        {
            REQUIRE_THAT(c.width(), IsEqual(10));
            REQUIRE_THAT(c.height(), IsEqual(20));
            AND_THEN("all pixels are black")
            {
                for(RT::Canvas::size_type row = 0; row != c.height(); ++row)
                    for(RT::Canvas::size_type column = 0; column != c.width();
                        ++column)
                        REQUIRE_THAT(c.at(column, row),
                                     IsEqual(RT::Color{0., 0., 0.}));
            }
        }
    }
}

SCENARIO("writing pixels to a canvas", "[Canvas]")
{
    GIVEN("a canvas 'c'")
    {
        RT::Canvas c{10, 20};
        AND_GIVEN("a red color")
        {
            auto red = RT::Color{1., 0., 0.};
            WHEN("writing the color to a pixel")
            {
                c.set(2, 3, red);
                THEN("that pixel changes to that color")
                {
                    REQUIRE_THAT(c.at(2, 3), IsEqual(red));
                }
            }
        }
    }
}

SCENARIO("constructing the PPM header")
{
    GIVEN("a canvas 'c'")
    {
        RT::Canvas c{5, 3};
        WHEN("serializing 'c' to a PPM")
        {
            std::stringstream ss;
            c.writeAsPPM(ss);
            THEN("the correct PPM header is in the first three lines")
            {
                std::string line;
                std::getline(ss, line);
                REQUIRE(line == "P3");
                std::getline(ss, line);
                REQUIRE(line == "5 3");
                std::getline(ss, line);
                REQUIRE(line == "255");
            }
        }
    }
}

SCENARIO("constructing the PPM pixel data")
{
    GIVEN("a canvas 'c'")
    {
        RT::Canvas c{5, 3};
        AND_GIVEN("three colors 'c1', 'c2', and 'c3'")
        {
            RT::Color c1{1.5l, 0.l, 0.l};
            RT::Color c2{0.l, .5l, 0.l};
            RT::Color c3{-.5l, 0.l, 1.l};
            WHEN("these colors are written to the canvas")
            {
                c.set(0, 0, c1);
                c.set(2, 1, c2);
                c.set(4, 2, c3);
                AND_WHEN("the canvas is written to PPM")
                {
                    std::stringstream ss;
                    c.writeAsPPM(ss);
                    THEN("lines 4-6 contain correct data")
                    {
                        std::string line;
                        // Header
                        std::getline(ss, line);
                        std::getline(ss, line);
                        std::getline(ss, line);

                        std::getline(ss, line);
                        REQUIRE(line == "255 0 0 0 0 0 0 0 0 0 0 0 0 0 0");
                        std::getline(ss, line);
                        REQUIRE(line == "0 0 0 0 0 0 0 128 0 0 0 0 0 0 0");
                        std::getline(ss, line);
                        REQUIRE(line == "0 0 0 0 0 0 0 0 0 0 0 0 0 0 255");
                    }
                }
            }
        }
    }
}

SCENARIO("splitting long lines in PPM files")
{
    GIVEN("a 10 pixel wide canvas 'c'")
    {
        RT::Canvas c{10, 2};
        WHEN("every pixel is set to color <1,.8,.6>")
        {
            RT::Color color{1.l, .8l, .6l};
            for(RT::Canvas::size_type row = 0; row != c.height(); ++row)
                for(RT::Canvas::size_type col = 0; col != c.width(); ++col)
                    c.set(col, row, color);
            AND_WHEN("'c' is written as a PPM file")
            {
                std::stringstream ss;
                c.writeAsPPM(ss);
                THEN("lines 4-7 of the PPM are correctly wrapped at 70 chars")
                {
                    std::string line;
                    // Skip header
                    std::getline(ss, line);
                    std::getline(ss, line);
                    std::getline(ss, line);

                    std::getline(ss, line);
                    REQUIRE(line ==
                            "255 204 153 255 204 153 255 204 153 255 204 153 "
                            "255 204 153 255 204");
                    std::getline(ss, line);
                    REQUIRE(line ==
                            "153 255 204 153 255 204 153 255 204 153 255 204 "
                            "153");
                    std::getline(ss, line);
                    REQUIRE(line ==
                            "255 204 153 255 204 153 255 204 153 255 204 153 "
                            "255 204 153 255 204");
                    std::getline(ss, line);
                    REQUIRE(line ==
                            "153 255 204 153 255 204 153 255 204 153 255 204 "
                            "153");
                }
            }
        }
    }
}

SCENARIO("PPM files are terminated by a newline character")
{
    GIVEN("a canvas 'c'")
    {
        RT::Canvas c{5, 3};
        WHEN("'c' is written as PPM")
        {
            std::stringstream ss;
            c.writeAsPPM(ss);
            THEN("the PPM ends with a newline character")
            {
                REQUIRE(ss.str().back() == '\n');
            }
        }
    }
}

