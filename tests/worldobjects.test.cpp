#include "catch.hpp"
#include "matchers.hpp"

#include "rays.hpp"
#include "utility.hpp"
#include "worldobjects.hpp"

SCENARIO("a ray intersects a sphere at two points", "[Ray][Sphere]")
{
    GIVEN("r = Ray(point(0,0,-5), vector(0,0,1))")
    {
        auto r = RT::Ray{RT::Tuple::Point(0., 0., -5.),
                         RT::Tuple::Vector(0., 0., 1.)};
        AND_GIVEN("a default sphere")
        {
            auto s = RT::Sphere{};
            WHEN("xs = s.intersects(r)")
            {
                auto xs = s.intersects(r);
                THEN("xs.size() == 2 (4 and 6)")
                {
                    REQUIRE_THAT(xs.size(), IsEqual(2));
                    REQUIRE_THAT(xs[0].time(), IsEqual(4.));
                    REQUIRE_THAT(xs[1].time(), IsEqual(6.));
                }
            }
        }
    }
}

SCENARIO("a ray intersects a sphere at a tangent", "[Ray][Sphere]")
{
    GIVEN("r = Ray(point(0,1,-5), vector(0, 0, 1))")
    {
        auto r = RT::Ray(RT::Tuple::Point(0., 1., -5.),
                         RT::Tuple::Vector(0., 0., 1.));
        AND_GIVEN("a default sphere 's'")
        {
            auto s = RT::Sphere{};
            WHEN("xs = s.intersects(r)")
            {
                auto xs = s.intersects(r);
                THEN("xs.size() == 2 (5 and 5)")
                {
                    REQUIRE_THAT(xs.size(), IsEqual(2));
                    REQUIRE_THAT(xs[0].time(), IsEqual(xs[1].time()));
                    REQUIRE_THAT(xs[0].time(), IsEqual(5.));
                    REQUIRE_THAT(xs[1].time(), IsEqual(5.));
                }
            }
        }
    }
}

SCENARIO("a ray misses a sphere", "[Ray][Sphere]")
{
    GIVEN("r = Ray(point(0,2,-5), vector(0,0,1))")
    {
        auto r = RT::Ray(RT::Tuple::Point(0., 2., -5.),
                         RT::Tuple::Vector(0., 0., -1.));
        AND_GIVEN("a default sphere")
        {
            auto s = RT::Sphere{};
            WHEN("xs = s.intersects(r)")
            {
                auto xs = s.intersects(r);
                THEN("xs.size() == 0")
                {
                    REQUIRE_THAT(xs.size(), IsEqual(0));
                }
            }
        }
    }
}

SCENARIO("a ray originates inside a sphere", "[Ray][Sphere]")
{
    GIVEN("r = Ray(point(0,0,0), vector(0,0,1))")
    {
        auto r = RT::Ray(RT::Tuple::Point(0., 0., 0.),
                         RT::Tuple::Vector(0., 0., 1.));
        AND_GIVEN("a default sphere")
        {
            auto s = RT::Sphere{};
            WHEN("xs = s.intersects(r)")
            {
                auto xs = s.intersects(r);
                THEN("xs.size() == 2 (-1 and 1)");
                {
                    REQUIRE_THAT(xs.size(), IsEqual(2));
                    REQUIRE_THAT(xs[0].time(), IsEqual(-1.));
                    REQUIRE_THAT(xs[1].time(), IsEqual(1.));
                }
            }
        }
    }
}

SCENARIO("a sphere is behind a ray", "[Ray][Sphere]")
{
    GIVEN("r = Ray(point(0,0,5), vector(0,0,1))")
    {
        auto r = RT::Ray(RT::Tuple::Point(0., 0., 5.),
                         RT::Tuple::Vector(0., 0., 1.));
        AND_GIVEN("a default sphere")
        {
            auto s = RT::Sphere{};
            WHEN("xs = s.intersects(r)")
            {
                auto xs = s.intersects(r);
                THEN("xs.size() == 2 (-6 and -4)")
                {
                    REQUIRE_THAT(xs.size(), IsEqual(2));
                    REQUIRE_THAT(xs[0].time(), IsEqual(-6.));
                    REQUIRE_THAT(xs[1].time(), IsEqual(-4.));
                }
            }
        }
    }
}

SCENARIO("an intersection encapsulates t and object", "[Intersection]")
{
    GIVEN("s = Sphere()")
    {
        auto s = RT::Sphere{};
        WHEN("i = Intersection(s, 3.5)")
        {
            auto i = RT::Intersection{&s, 3.5};
            THEN("i.time() == 3.5")
            {
                REQUIRE_THAT(i.time(), IsEqual(3.5));
                AND_THEN("i.object() == s")
                {
                    REQUIRE(i.object() == &s);
                }
            }
        }
    }
}

SCENARIO("intersect sets the object on the intersection",
         "[Ray][Intersection][Sphere]")
{
    GIVEN("r = Ray(point(0,0,-5), vector(0,0,1))")
    {
        auto r = RT::Ray{RT::Tuple::Point(0., 0., -5.),
                         RT::Tuple::Vector(0., 0., 1.)};
        AND_GIVEN("a default sphere")
        {
            auto s = RT::Sphere();
            WHEN("xs = s.intersects(r)")
            {
                auto xs = s.intersects(r);
                THEN("xs.size() == 2")
                {
                    REQUIRE_THAT(xs.size(), IsEqual(2));
                    AND_THEN("xs*.object() returns s")
                    {
                        REQUIRE(xs[0].object() == &s);
                        REQUIRE(xs[1].object() == &s);
                    }
                }
            }
        }
    }
}

SCENARIO("the hit, when all intersections have positive t",
         "[Intersection][Sphere]")
{
    GIVEN("a default sphere")
    {
        auto s = RT::Sphere{};
        AND_GIVEN("xs = two intersections with t = 1 and 2")
        {
            std::vector<RT::Intersection> xs;
            xs.emplace_back(&s, 2.0);
            xs.emplace_back(&s, 1.0);
            WHEN("i = hit(xs)")
            {
                auto i = hit(xs);
                THEN("i == &s")
                {
                    REQUIRE(i);
                    REQUIRE(i->object() == &s);
                    REQUIRE_THAT(i->time(), IsEqual(1.0));
                }
            }
        }
    }
}

SCENARIO("the hit, when some intersections have negative t", "[Intersection]")
{
    GIVEN("a default sphere")
    {
        auto s = RT::Sphere{};
        AND_GIVEN("some intersections, including negative times")
        {
            std::vector<RT::Intersection> xs;
            xs.emplace_back(&s, -1.);
            xs.emplace_back(&s, -3.);
            xs.emplace_back(&s, 2.);
            xs.emplace_back(&s, -2.);
            xs.emplace_back(&s, -5.);
            xs.emplace_back(&s, 4.);
            xs.emplace_back(&s, -4.);
            WHEN("i = hit(xs)")
            {
                auto i = RT::hit(xs);
                THEN("i->time() is the smallest positive value")
                {
                    REQUIRE(i);
                    REQUIRE(i->object() == &s);
                    REQUIRE_THAT(i->time(), IsEqual(2.));
                }
            }
        }
    }
}

SCENARIO("the hit, when all intersections have negative t", "[Intersection]")
{
    GIVEN("a sphere")
    {
        auto s = RT::Sphere{};
        AND_GIVEN("a collection of negative t intersections")
        {
            std::vector<RT::Intersection> xs;
            xs.emplace_back(&s, -2.);
            xs.emplace_back(&s, -1.);
            WHEN("i = hit(xs)")
            {
                auto i = RT::hit(xs);
                THEN("i is nothing")
                {
                    REQUIRE_FALSE(i);
                }
            }
        }
    }
}

SCENARIO("a sphere's default transform", "[WorldObject]")
{
    GIVEN("s = sphere()")
    {
        auto s = RT::Sphere{};
        THEN("s.transform() == Transform::identity()")
        {
            REQUIRE_THAT(s.transform(), IsEqual(RT::Transform::Identity()));
        }
    }
}

SCENARIO("changing a sphere's transform")
{
    GIVEN("s = Sphere()")
    {
        auto s = RT::Sphere{};
        AND_GIVEN("t = translation(2,3,4)")
        {
            auto t = RT::Transform::Translation(2., 3., 4.);
            WHEN("s.setTransform(t)")
            {
                s.setTransform(t);
                THEN("s.transform() == t")
                {
                    REQUIRE_THAT(s.transform(), IsEqual(t));
                }
            }
        }
    }
}

SCENARIO("intersecting a scaled sphere with a ray", "[WorldObject]")
{
    GIVEN("r = ray(point(0,0,-5), vector(0,0,1))")
    {
        auto r = RT::Ray{RT::Tuple::Point(0., 0., -5.),
                         RT::Tuple::Vector(0., 0., 1.)};
        AND_GIVEN("s = sphere()")
        {
            auto s = RT::Sphere{};
            WHEN("s.setTransform(scaling(2,2,2))")
            {
                s.setTransform(RT::Transform::Scaling(2., 2., 2.));
                AND_WHEN("xs = s.intersects(r)")
                {
                    auto xs = s.intersects(r);
                    THEN("two intersections a t = 3 and 7")
                    {
                        REQUIRE_THAT(xs.size(), IsEqual(2));
                        REQUIRE_THAT(xs[0].time(), IsEqual(3.));
                        REQUIRE_THAT(xs[1].time(), IsEqual(7.));
                    }
                }
            }
        }
    }
}

SCENARIO("the normal on a sphere at a point", "[Sphere]")
{
    GIVEN("s = sphere()")
    {
        auto s = RT::Sphere{};
        WHEN("n = a normal on the x-axis")
        {
            auto n = s.normalAt(RT::Tuple::Point(1., 0., 0.));
            THEN("n = vector(1,0,0)")
            {
                REQUIRE_THAT(n, IsEqual(RT::Tuple::Vector(1., 0., 0.)));
            }
        }
        WHEN("n = a normal on the y-axis")
        {
            auto n = s.normalAt(RT::Tuple::Point(0., 1., 0.));
            THEN("n == vector(0,1,0)")
            {
                REQUIRE_THAT(n, IsEqual(RT::Tuple::Vector(0., 1., 0.)));
            }
        }
        WHEN("n = a normal on the z-axis")
        {
            auto n = s.normalAt(RT::Tuple::Point(0., 0., 1.));
            THEN("n == vector(0,0,1)")
            {
                REQUIRE_THAT(n, IsEqual(RT::Tuple::Vector(0., 0., 1.)));
            }
        }
        WHEN("n = a normal at a nonaxial point")
        {
            auto scalar = std::sqrt(3.) / 3.;
            auto n = s.normalAt(RT::Tuple::Point(scalar, scalar, scalar));
            THEN("n = vector(sqrt(3)/3,sqrt(3)/3,sqrt(3)/3)")
            {
                REQUIRE_THAT(
                    n, IsEqual(RT::Tuple::Vector(scalar, scalar, scalar)));
                AND_THEN("the normal is always normalized")
                {
                    REQUIRE_THAT(n, IsEqual(n.normalized()));
                }
            }
        }
    }
}

SCENARIO("computing the normal on a translated sphere", "[Sphere]")
{
    GIVEN("s = sphere()")
    {
        auto s = RT::Sphere{};
        AND_GIVEN("s is translated by (0,1,0)")
        {
            s.setTransform(RT::Transform::Translation(0., 1., 0.));
            WHEN("n = s.normalAt(point(0,1.70711,-0.70711))")
            {
                auto n = s.normalAt(RT::Tuple::Point(0., 1.70711, -0.70711));
                THEN("n == vector(0, 0.70711,-0.70711)")
                {
                    REQUIRE_THAT(
                        n,
                        IsEqual(RT::Tuple::Vector(0., .707106781186547,
                                                  -.707106781186548)));
                }
            }
        }
    }
}

SCENARIO("computing the normal on a transformed sphere", "[Sphere]")
{
    GIVEN("s = sphere()")
    {
        auto s = RT::Sphere{};
        AND_GIVEN("s is scaled and rotated")
        {
            auto m = RT::Transform::Scaling(1., 0.5, 1.) *
                RT::Transform::RotationZ(RT::PI() / 5.);
            s.setTransform(m);
            WHEN("n = s.normalAt(point(0,sqrt(2)/2,-sqrt(2)/2))")
            {
                double const scalar = std::sqrt(2.) / 2.;
                auto n = s.normalAt(RT::Tuple::Point(0., scalar, -scalar));
                THEN("n == vector(0,0.97014,-0.24254)")
                {
                    REQUIRE_THAT(n(0), IsEqual(0.));
                    REQUIRE_THAT(n(1), IsEqual(0.970142500145332));
                    REQUIRE_THAT(n(2), IsEqual(-0.242535625036333));
                    REQUIRE_THAT(n(3), IsEqual(0.));
                }
            }
        }
    }
}

SCENARIO("a sphere has a default material", "[Sphere][Material]")
{
    GIVEN("s = sphere()")
    {
        auto s = RT::Sphere{};
        WHEN("m = s.material()")
        {
            auto m = s.material();
            THEN("m = material()")
            {
                REQUIRE_THAT(m, IsEqual(RT::Material{}));
            }
        }
    }
}

SCENARIO("a sphere may be assigned a material", "[Sphere]")
{
    GIVEN("s = sphere()")
    {
        auto s = RT::Sphere{};
        WHEN("m = s.material()")
        {
            auto m = s.material();
            AND_WHEN("m.setAmbient(1.)")
            {
                m.setAmbient(1.);
                AND_WHEN("s.setMaterial(m)")
                {
                    s.setMaterial(m);
                    THEN("s.material() == m")
                    {
                        REQUIRE_THAT(s.material(), IsEqual(m));
                    }
                }
            }
        }
    }
}

SCENARIO("precomputing the state of an intersection", "[Intersection]")
{
    GIVEN("r = ray(point(0,0,-5), vector(0,0,1))")
    {
        auto r = RT::Ray{RT::Tuple::Point(0., 0., -5.),
                         RT::Tuple::Vector(0., 0., 1.)};
        AND_GIVEN("a sphere and an intersection")
        {
            auto shape = RT::Sphere{};
            auto i = RT::Intersection{&shape, 4.};
            WHEN("comps = i.precompute(r)")
            {
                auto comps = i.precompute(r, {});
                THEN("comps has the precomputed values")
                {
                    REQUIRE_THAT(comps.time, IsEqual(i.time()));
                    REQUIRE_THAT(comps.object, IsEqual(i.object()));
                    REQUIRE_THAT(comps.point,
                                 IsEqual(RT::Tuple::Point(0., 0., -1.)));
                    REQUIRE_THAT(comps.eyeVector,
                                 IsEqual(RT::Tuple::Vector(0., 0., -1.)));
                    REQUIRE_THAT(comps.normalVector,
                                 IsEqual(RT::Tuple::Vector(0., 0., -1)));
                }
            }
        }
    }
}

SCENARIO("the hit, when an intersection occurs on the outside",
         "[Intersection]")
{
    GIVEN("r = ray(point(0,0,-5), vector(0,0,1))")
    {
        auto r = RT::Ray{RT::Tuple::Point(0., 0., -5.),
                         RT::Tuple::Vector(0., 0., 1.)};
        AND_GIVEN("a sphere and an intersection from the outside")
        {
            auto shape = RT::Sphere{};
            auto i = RT::Intersection{&shape, 4.};
            WHEN("comps = i.precompute(r)")
            {
                auto comps = i.precompute(r, {});
                THEN("comps.isInside == false")
                {
                    REQUIRE_FALSE(comps.isInside);
                }
            }
        }
    }
}

SCENARIO("the hit, when an intersection occurs on the inside",
         "[Intersection]")
{
    GIVEN("r = ray(point(0,0,0), vector(0,0,1))")
    {
        auto r = RT::Ray{RT::Tuple::Point(0., 0., 0.),
                         RT::Tuple::Vector(0., 0., 1.)};
        AND_GIVEN("a sphere and an intersection on the inside")
        {
            auto shape = RT::Sphere{};
            auto i = RT::Intersection{&shape, 1.};
            WHEN("comps = i.precompute(r)")
            {
                auto comps = i.precompute(r, {});
                THEN("comps.point = (0,0,1)")
                {
                    REQUIRE_THAT(comps.point,
                                 IsEqual(RT::Tuple::Point(0., 0., 1.)));
                }
                THEN("comps.eyeVector = (0,0,-1)")
                {
                    REQUIRE_THAT(comps.eyeVector,
                                 IsEqual(RT::Tuple::Vector(0., 0., -1.)));
                }
                THEN("comps.isInside == true")
                {
                    REQUIRE(comps.isInside);
                    AND_THEN("comps.normalVector = (0,0,-1)")
                    {
                        REQUIRE_THAT(comps.normalVector,
                                     IsEqual(RT::Tuple::Vector(0., 0., -1.)));
                    }
                }
            }
        }
    }
}

SCENARIO("the hit should offset the point", "[Intersection]")
{
    GIVEN("r = ray((0,0,-5), (0,0,1))")
    {
        auto r = RT::Ray{RT::Tuple::Point(0., 0., -5.),
                         RT::Tuple::Vector(0., 0., 1.)};
        AND_GIVEN("shape = sphere with translation(0,0,1)")
        {
            auto shape = RT::Sphere{RT::Transform::Translation(0., 0., 1.)};
            AND_GIVEN("i = intersection(shape, 5)")
            {
                auto i = RT::Intersection{&shape, 5};
                WHEN("comps = i.precompute(r)")
                {
                    auto comps = i.precompute(r, {});
                    THEN("comps.overPoint(2) < -EPSILON/2")
                    {
                        REQUIRE(comps.overPoint(2) <
                                -std::numeric_limits<double>::epsilon() / 2);
                        AND_THEN("comps.point(2) > comps.overPoint(2)")
                        {
                            REQUIRE(comps.point(2) > comps.overPoint(2));
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("precomputing the reflection vector", "[Intersection]")
{
    GIVEN("shape = plane()")
    {
        auto shape = RT::Plane{};
        AND_GIVEN("r = ray((0,1,-1),(0,-sqrt(2)/2,sqrt(2)/2)")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., 1., -1.),
                             RT::Tuple::Vector(0., -std::sqrt(2.) / 2.,
                                               std::sqrt(2.) / 2.)};
            AND_GIVEN("i = intersection(shape, sqrt(2))")
            {
                auto i = RT::Intersection{&shape, std::sqrt(2.)};
                WHEN("comps = i.precompute(r)")
                {
                    auto comps = i.precompute(r, {});
                    THEN("comps.reflectVector = (0, sqrt(2)/2, sqrt(2)/2)")
                    {
                        REQUIRE_THAT(
                            comps.reflectVector,
                            IsEqual(RT::Tuple::Vector(0., std::sqrt(2.) / 2.,
                                                      std::sqrt(2.) / 2.)));
                    }
                }
            }
        }
    }
}

SCENARIO("the normal of a plane is constant everywhere", "[Plane]")
{
    GIVEN("p = plane()")
    {
        auto p = RT::Plane{};
        WHEN("n1 = p.normalAt((0,0,0))")
        {
            auto n1 = p.normalAt(RT::Tuple::Point(0., 0., 0.));
            AND_WHEN("n2 = p.normalAt((10,0,-10))")
            {
                auto n2 = p.normalAt(RT::Tuple::Point(10., 0., -10.));
                AND_WHEN("n3 = p.normalAt((-5,0,150))")
                {
                    auto n3 = p.normalAt(RT::Tuple::Point(-5., 0., 150.));
                    THEN("n1 = n2 = n3 = vector(0,1,0)")
                    {
                        auto v = RT::Tuple::Vector(0., 1., 0.);
                        REQUIRE_THAT(n1, IsEqual(v));
                        REQUIRE_THAT(n2, IsEqual(v));
                        REQUIRE_THAT(n3, IsEqual(v));
                    }
                }
            }
        }
    }
}

SCENARIO("intersct with a ray parallel to a plane", "[Plane]")
{
    GIVEN("p = plane()")
    {
        auto p = RT::Plane{};
        AND_GIVEN("r = ray((0,10,0), (0,0,1))")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., 10., 0.),
                             RT::Tuple::Vector(0., 0., 1.)};
            WHEN("xs = p.intersects(r)")
            {
                auto xs = p.intersects(r);
                THEN("xs.size() == 0")
                {
                    REQUIRE_THAT(xs.size(), IsEqual(0));
                }
            }
        }
    }
}

SCENARIO("intersect with a coplanar ray", "[Plane]")
{
    GIVEN("p = plane()")
    {
        auto p = RT::Plane{};
        AND_GIVEN("r = ray(point(0,0,0), vector(0,0,1))")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., 0., 0.),
                             RT::Tuple::Vector(0., 0., 1.)};
            WHEN("xs = p.intersects(r)")
            {
                auto xs = p.intersects(r);
                THEN("xs.size() == 0")
                {
                    REQUIRE_THAT(xs.size(), IsEqual(0));
                }
            }
        }
    }
}

SCENARIO("a ray intersecting a plane from above", "[Plane]")
{
    GIVEN("p = plane()")
    {
        auto p = RT::Plane{};
        AND_GIVEN("r = ray((0,1,0), (0,-1,0))")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., 1., 0.),
                             RT::Tuple::Vector(0., -1., 0.)};
            WHEN("xs = p.intersects(r)")
            {
                auto xs = p.intersects(r);
                THEN("xs.size() = 1")
                {
                    REQUIRE_THAT(xs.size(), IsEqual(1));
                    AND_THEN("xs[0].time() = 1")
                    {
                        REQUIRE_THAT(xs[0].time(), IsEqual(1));
                        AND_THEN("xs[0].object == p")
                        {
                            REQUIRE(xs[0].object() == &p);
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("a ray intersecting a plane from below", "[Plane]")
{
    GIVEN("p = plane()")
    {
        auto p = RT::Plane{};
        AND_GIVEN("r = ray((0,-1,0), (0,1,0))")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., -1., 0.),
                             RT::Tuple::Vector(0., 1., 0.)};
            WHEN("xs = p.intersects(r)")
            {
                auto xs = p.intersects(r);
                THEN("xs.size() = 1")
                {
                    REQUIRE_THAT(xs.size(), IsEqual(1));
                    AND_THEN("xs[0].time() = 1")
                    {
                        REQUIRE_THAT(xs[0].time(), IsEqual(1));
                        AND_THEN("xs[0].object == p")
                        {
                            REQUIRE(xs[0].object() == &p);
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("a factory for a sphere with a glassy material",
         "[Sphere][Refraction]")
{
    GIVEN("s = Sphere::GlassSphere()")
    {
        auto s = RT::Sphere::GlassSphere();
        THEN("s.transform() = identity")
        {
            REQUIRE_THAT(s.transform(), IsEqual(RT::Transform::Identity()));
        }
        THEN("s.material().transparency() == 1.0")
        {
            REQUIRE_THAT(s.material().transparency(), IsEqual(1.));
        }
        THEN("s.material().refractiveIndex() == 1.5")
        {
            REQUIRE_THAT(s.material().refractiveIndex(), IsEqual(1.5));
        }
    }
}

SCENARIO("finding n1 and n2 at various intersections", "[Refrection]")
{
    GIVEN("a glass sphere with scaling(2,2,2)")
    {
        auto A = RT::Sphere::GlassSphere();
        A.setTransform(RT::Transform::Scaling(2., 2., 2.));
        AND_GIVEN("a glass sphere with translation(0,0,-.25) and refr.idx = "
                  "2.0")
        {
            auto B = RT::Sphere::GlassSphere();
            B.setTransform(RT::Transform::Translation(0., 0., -.25));
            auto m = B.material();
            m.setRefractiveIndex(2.);
            B.setMaterial(m);
            AND_GIVEN("a glass sphere with translation(0,0,0.25) and ref.idx "
                      "= 2.5")
            {
                auto C = RT::Sphere::GlassSphere();
                C.setTransform(RT::Transform::Translation(0., 0., .25));
                m = C.material();
                m.setRefractiveIndex(2.5);
                C.setMaterial(m);
                AND_GIVEN("r = ray(0,0,-4)->(0,0,1)")
                {
                    auto r = RT::Ray{RT::Tuple::Point(0., 0., -4.),
                                     RT::Tuple::Vector(0., 0., 1)};
                    AND_GIVEN("xs = intersections(A:2, B:2.75, C:3.25, "
                              "B:4.75, C:5.25, A:6)")
                    {
                        std::vector<RT::Intersection> xs{
                            {&A, 2.},   {&B, 2.75}, {&C, 3.25},
                            {&B, 4.75}, {&C, 5.25}, {&A, 6.}};
                        std::vector<std::pair<double, double>> ns = {
                            {1., 1.5},  {1.5, 2.},  {2., 2.5},
                            {2.5, 2.5}, {2.5, 1.5}, {1.5, 1.}};
                        WHEN("comps = xs[i].precompute(r)")
                        {
                            auto i = GENERATE(0, 1, 2, 3, 4, 5);
                            auto comps = xs[i].precompute(r, xs);
                            THEN("n1,n2 = correct values")
                            {
                                REQUIRE_THAT(comps.n1, IsEqual(ns[i].first));
                                REQUIRE_THAT(comps.n2, IsEqual(ns[i].second));
                            }
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("the under point is offset below the surface",
         "[Intersection][Refraction]")
{
    GIVEN("r = ray(0,0,-5)->(0,0,1)")
    {
        auto r = RT::Ray{RT::Tuple::Point(0., 0., -5.),
                         RT::Tuple::Vector(0., 0., 1.)};
        AND_GIVEN("shape = glass sphere with translation(0,0,1)")
        {
            auto shape = RT::Sphere::GlassSphere();
            shape.setTransform(RT::Transform::Translation(0., 0., 1.));
            AND_GIVEN("i = intersection(shape, 5)")
            {
                auto i = RT::Intersection{&shape, 5.};
                AND_GIVEN("xs = intersections(i)")
                {
                    std::vector<RT::Intersection> xs{i};
                    WHEN("comps = i.precompute(r, xs)");
                    {
                        auto comps = i.precompute(r, xs);
                        THEN("comps.underPoint(2) > EPSILON/2")
                        {
                            REQUIRE(comps.underPoint(2) >
                                    std::numeric_limits<double>::epsilon() /
                                        2.);
                            AND_THEN("comps.point(2) < comps.underPoint(2)")
                            {
                                REQUIRE(comps.point(2) < comps.underPoint(2));
                            }
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("the Schlick approximation under total internal reflection",
         "[Refraction]")
{
    GIVEN("shape = a glass sphere")
    {
        auto shape = RT::Sphere::GlassSphere();
        AND_GIVEN("r = ray(0,0,sqrt(2)/2)->(0,1,0))")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., 0., std::sqrt(2.) / 2.),
                             RT::Tuple::Vector(0., 1., 0.)};
            AND_GIVEN("xs = intersections(shape:-sqrt(2)/2, shape:sqrt(2)/2)")
            {
                std::vector<RT::Intersection> xs{{&shape, -std::sqrt(2.) / 2.},
                                                 {&shape, std::sqrt(2.) / 2.}};
                WHEN("comps = xs[1].precompute(r, xs)")
                {
                    auto comps = xs[1].precompute(r, xs);
                    AND_WHEN("reflectance = schlick(comps)")
                    {
                        auto reflectance = schlick(comps);
                        THEN("reflectance = 1.0")
                        {
                            REQUIRE_THAT(reflectance, IsEqual(1.));
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("schlick approximation with a perpendicular viewing angle",
         "[World][Refraction]")
{
    GIVEN("shape is a glass sphere")
    {
        auto shape = RT::Sphere::GlassSphere();
        AND_GIVEN("r = ray(0,0,0)->(0,1,0)")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., 0., 0.),
                             RT::Tuple::Vector(0., 1., 0.)};
            AND_GIVEN("xs = intersections(shape:-1, shape:1)")
            {
                std::vector<RT::Intersection> xs{{&shape, -1.}, {&shape, 1.}};
                WHEN("comps = xs[1].precompute(r, xs)")
                {
                    auto comps = xs[1].precompute(r, xs);
                    AND_WHEN("reflectance = schlick(comps)")
                    {
                        auto reflectance = schlick(comps);
                        THEN("reflectance = 0.04")
                        {
                            REQUIRE_THAT(reflectance, IsEqual(.04));
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("schlick approximation with small angle and n2 > n1",
         "[World][Refraction]")
{
    GIVEN("shape = glass sphere")
    {
        auto shape = RT::Sphere::GlassSphere();
        AND_GIVEN("r = ray(0,0.99,-2)->(0,0,1)")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., .99, -2.),
                             RT::Tuple::Vector(0., 0., 1.)};
            AND_GIVEN("xs = intersections(shape:1.8589)")
            {
                std::vector<RT::Intersection> xs{{&shape, 1.8589}};
                WHEN("comps = xs[0].precompute(r, xs)")
                {
                    auto comps = xs[0].precompute(r, xs);
                    AND_WHEN("reflectance = schlick(comps)")
                    {
                        auto reflectance = schlick(comps);
                        THEN("reflectance = 0.48873")
                        {
                            REQUIRE_THAT(reflectance,
                                         IsEqual(.48873081012212183));
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("a ray intersects a cube", "[Ray][Cube]")
{
    GIVEN("c = cube()")
    {
        auto c = RT::Cube{};
        AND_GIVEN("ray = ray")
        {
            auto r = GENERATE(table<RT::Tuple, RT::Tuple, double, double>(
                {{{5., .5, 0., 1.}, {-1., 0., 0., 0.}, 4., 6.},
                 {{-5., .5, 0., 1.}, {1., 0., 0., 0.}, 4., 6.},
                 {{.5, 5., 0., 1.}, {0., -1., 0., 0.}, 4., 6.},
                 {{.5, -5., 0., 1.}, {0., 1., 0., 0.}, 4., 6.},
                 {{.5, 0., 5., 1.}, {0., 0., -1., 0.}, 4., 6.},
                 {{.5, 0., -5., 1.}, {0., 0., 1., 0.}, 4., 6.},
                 {{0., .5, 0., 1.}, {0., 0., 1., 0.}, -1., 1.}}));
            auto ray = RT::Ray{std::get<0>(r), std::get<1>(r)};
            WHEN("xs = c.intersects(ray)")
            {
                auto xs = c.intersects(ray);
                THEN("xs.size = 2")
                {
                    REQUIRE_THAT(xs.size(), IsEqual(2));
                    AND_THEN("times are as per table")
                    {
                        REQUIRE_THAT(xs[0].time(), IsEqual(std::get<2>(r)));
                        REQUIRE_THAT(xs[1].time(), IsEqual(std::get<3>(r)));
                    }
                }
            }
        }
    }
}

SCENARIO("a ray misses a cube", "[Ray][Cube]")
{
    GIVEN("c = cube()")
    {
        auto c = RT::Cube{};
        AND_GIVEN("ray = ray")
        {
            auto r = GENERATE(table<RT::Tuple, RT::Tuple>(
                {{{-2., 0., 0., 1.}, {.2673, .5345, .8018, 0.}},
                 {{0., -2., 0., 1.}, {.8018, .2673, .5345, 0.}},
                 {{0., 0., -2., 1.}, {.5345, .8018, .2673, 0.}},
                 {{2., 0., 2., 1.}, {0., 0., -1., 0.}},
                 {{0., 2., 2., 1.}, {0., -1., 0., 0.}},
                 {{2., 2., 0., 1.}, {-1., 0., 0., 0.}}}));
            auto ray = RT::Ray{std::get<0>(r), std::get<1>(r)};
            WHEN("xs = c.intersects(ray)")
            {
                auto xs = c.intersects(ray);
                THEN("xs.size = 0")
                {
                    REQUIRE_THAT(xs.size(), IsEqual(0));
                }
            }
        }
    }
}

SCENARIO("the normal on the surface of a cube", "[Ray][Cube]")
{
    GIVEN("c = cube()")
    {
        auto c = RT::Cube{};
        AND_GIVEN("p = <point>")
        {
            auto n = GENERATE(table<RT::Tuple, RT::Tuple>(
                {{{1., .5, -.8, 1.}, {1., 0., 0., 0.}},
                 {{-1., -.2, .9, 1.}, {-1., 0., 0., 0.}},
                 {{-.4, 1., -.1, 1.}, {0., 1., 0., 0.}},
                 {{.3, -1., -.7, 1.}, {0., -1., 0., 0.}},
                 {{-.6, .3, 1., 1.}, {0., 0., 1., 0.}},
                 {{.4, .4, -1., 1.}, {0., 0., -1., 0.}},
                 {{1., 1., 1., 1.}, {1., 0., 0., 0.}},
                 {{-1., -1., -1., 1.}, {-1., 0., 0., 0.}}}));
            auto p = std::get<0>(n);
            WHEN("normal = c.normalAt(p)")
            {
                auto normal = c.normalAt(p);
                THEN("normal = <normal>")
                {
                    REQUIRE_THAT(normal, IsEqual(std::get<1>(n)));
                }
            }
        }
    }
}

SCENARIO("a ray misses a cylinder", "[Ray][Cylinder]")
{
    GIVEN("cyl = cylinder()")
    {
        auto cyl = RT::Cylinder{};
        AND_GIVEN("ray = <origin>, normalize<direction>")
        {
            auto tb = GENERATE(table<RT::Tuple, RT::Tuple>(
                {{{1., 0., 0., 1.}, {0., 1., 0., 0.}},
                 {{0., 0., 0., 1.}, {0., 1., 0., 0.}},
                 {{0., 0., -5., 1.}, {1., 1., 1., 0.}}}));
            auto direction = std::get<1>(tb);
            direction.normalize();
            auto ray = RT::Ray{std::get<0>(tb), direction};
            WHEN("xs = cyl.intersects(ray)")
            {
                auto xs = cyl.intersects(ray);
                THEN("xs is empty")
                {
                    REQUIRE(xs.empty());
                }
            }
        }
    }
}

SCENARIO("a ray strikes a cylinder", "[Ray][Cylinder]")
{
    GIVEN("cyl = cylinder()")
    {
        auto cyl = RT::Cylinder{};
        AND_GIVEN("ray = <origin>, normalize<direction>")
        {
            auto tb = GENERATE(table<RT::Tuple, RT::Tuple, double, double>(
                {{{1., 0., -5., 1.}, {0., 0., 1., 0.}, 5., 5.},
                 {{0., 0., -5., 1.}, {0., 0., 1., 0.}, 4., 6.},
                 {{0.5, 0., -5., 1.},
                  {.1, 1., 1., 0.},
                  6.8079819170273197,
                  7.0887234393788612}}));
            auto direction = std::get<1>(tb);
            direction.normalize();
            auto ray = RT::Ray{std::get<0>(tb), direction};
            WHEN("xs = cyl.intersects(ray)")
            {
                auto xs = cyl.intersects(ray);
                THEN("xs.size = 2")
                {
                    REQUIRE_THAT(xs.size(), IsEqual(2));
                    AND_THEN("xs[0].time = <t0>")
                    {
                        REQUIRE_THAT(xs[0].time(), IsEqual(std::get<2>(tb)));
                    }
                    AND_THEN("xs[1].time = <t1>")
                    {
                        REQUIRE_THAT(xs[1].time(), IsEqual(std::get<3>(tb)));
                    }
                }
            }
        }
    }
}

SCENARIO("normal vector on a cylinder", "[Ray][Cylinder]")
{
    GIVEN("cyl = cylinder")
    {
        auto cyl = RT::Cylinder();
        WHEN("n = cyl.normalAt(<point>)")
        {
            auto tb = GENERATE(table<RT::Tuple, RT::Tuple>(
                {{{1., 0., 0., 1.}, {1., 0., 0., 0.}},
                 {{0., 5., -1., 1.}, {0., 0., -1., 0.}},
                 {{0., -2., 1., 1.}, {0., 0., 1., 0.}},
                 {{-1., 1., 0., 1.}, {-1., 0., 0., 0.}}}));
            auto n = cyl.normalAt(std::get<0>(tb));
            THEN("n = <normal>")
            {
                REQUIRE_THAT(n, IsEqual(std::get<1>(tb)));
            }
        }
    }
}

SCENARIO("the default minimum and maximum for a cylinder", "[Cylinder]")
{
    GIVEN("cyl = cylinder")
    {
        auto cyl = RT::Cylinder{};
        THEN("cyl.minimum == -infinity")
        {
            REQUIRE_THAT(cyl.minimum(),
                         IsEqual(-std::numeric_limits<double>::infinity()));
        }
        THEN("cyl.maximum == infinity")
        {
            REQUIRE_THAT(cyl.maximum(),
                         IsEqual(std::numeric_limits<double>::infinity()));
        }
    }
}

SCENARIO("intersecting a constrained cylinder", "[Ray][Cylinder]")
{
    GIVEN("cyl = cylinder")
    {
        auto cyl = RT::Cylinder{};
        AND_GIVEN("cyl.min/max = 1/2")
        {
            cyl.setMinimum(1.);
            cyl.setMaximum(2.);
            AND_GIVEN("r = ray<point>->normalized<direction>")
            {
                auto tb = GENERATE(table<RT::Tuple, RT::Tuple, int>(
                    {{{0., 1.5, 0., 1.}, {.1, 1., 0., 0.}, 0},
                     {{0., -3., -5., 1.}, {0., 0., 1., 0.}, 0},
                     {{0., 0., -5., 1.}, {0., 0., 1., 0.}, 0},
                     {{0., 2., -5., 1.}, {0., 0., 1., 0.}, 0},
                     {{0., 1., -5., 1.}, {0., 0., 1., 0.}, 0},
                     {{0., 1.5, -2., 1.}, {0., 0., 1., 0.}, 2}}));
                auto r = RT::Ray{std::get<0>(tb), std::get<1>(tb)};
                WHEN("xs = cyl.intersects(r)")
                {
                    auto xs = cyl.intersects(r);
                    THEN("xs.size = <count>")
                    {
                        REQUIRE_THAT(xs.size(), IsEqual(std::get<2>(tb)));
                    }
                }
            }
        }
    }
}

SCENARIO("the default closed value for a cylinder", "[Cylinder]")
{
    GIVEN("cyl = cylinder")
    {
        auto cyl = RT::Cylinder{};
        THEN("cyl is closed")
        {
            REQUIRE_FALSE(cyl.isClosed());
        }
    }
}

SCENARIO("intersecting the caps of a closed cylinder", "[Cylinder]")
{
    GIVEN("cyl = cylinder(1;2) closed")
    {
        auto cyl = RT::Cylinder{};
        cyl.setMinimum(1.);
        cyl.setMaximum(2.);
        cyl.setClosed();
        AND_GIVEN("r = ray<point>->normalized<direction>")
        {
            auto tb = GENERATE(table<RT::Tuple, RT::Tuple, int>(
                {{{0., 3., 0., 1.}, {0., -1., 0., 0.}, 2},
                 {{0., 3., -2., 1.}, {0., -1., 2., 0.}, 2},
                 {{0., 4., -2., 1.}, {0., -1., 1., 0.}, 2},
                 {{0., 0., -2., 1.}, {0., 1., 2., 0.}, 2},
                 {{0., -1., -2., 1.}, {0., 1., 1., 0.}, 2}}));
            auto r = RT::Ray{std::get<0>(tb), std::get<1>(tb)};
            WHEN("xs = cyl.intersects(r)")
            {
                auto xs = cyl.intersects(r);
                THEN("xs.size = <count>")
                {
                    REQUIRE_THAT(xs.size(), IsEqual(std::get<2>(tb)));
                }
            }
        }
    }
}

SCENARIO("the normal vector on a cylinder's end caps", "[Cylinder]")
{
    GIVEN("cyl = cylinder (1;2) closed")
    {
        auto cyl = RT::Cylinder{};
        cyl.setMinimum(1.);
        cyl.setMaximum(2.);
        cyl.setClosed();
        WHEN("n = cyl.normalAt(<point>)")
        {
            auto tb = GENERATE(table<RT::Tuple, RT::Tuple>(
                {{{0., 1., 0., 1.}, {0., -1., 0., 0.}},
                 {{.5, 1., 0., 1.}, {0., -1., 0., 0.}},
                 {{0., 1., .5, 1.}, {0., -1., 0., 0.}},
                 {{0., 2., 0., 1.}, {0., 1., 0., 0.}},
                 {{.5, 2., 0., 1.}, {0., 1., 0., 0.}},
                 {{0., 2., .5, 1.}, {0., 1., 0., 0.}}}));
            auto n = cyl.normalAt(std::get<0>(tb));
            THEN("n = <normal>")
            {
                REQUIRE_THAT(n, IsEqual(std::get<1>(tb)));
            }
        }
    }
}

