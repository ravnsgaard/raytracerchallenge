#ifndef BRR_MATCHERS_HPP_
#define BRR_MATCHERS_HPP_

#include "catch.hpp"
#include <iomanip>
#include <limits>
#include <sstream>

template<typename T,
         typename = std::enable_if_t<std::is_floating_point<T>::value>>
class CompareFloatsAlmostEqual : public Catch::MatcherBase<T>
{
public:
    CompareFloatsAlmostEqual(T value) : value_(value)
    {
    }

    bool match(T const & val) const override
    {
        // NaN
        if(std::isnan(value_) || std::isnan(val))
            return false;

        // Infinity with same sign?
        if(std::isinf(value_))
        {
            if(std::isinf(val))
            {
                return std::signbit(value_) == std::signbit(val);
            }
            else
                return false;
        }
        if(std::isinf(val))
            return false;

        // Compare
        static constexpr int ULPs = 4;
        return std::abs(val - value_) < std::numeric_limits<T>::epsilon() *
                std::max(static_cast<T>(1.), std::abs(val + value_)) * ULPs ||
            std::abs(val - value_) < std::numeric_limits<T>::min();
    }

    virtual std::string describe() const override
    {
        std::ostringstream ss;
        ss << "compares equal to " << std::setprecision(10) << value_;
        return ss.str();
    }

private:
    T value_;
};

template<typename T,
         typename = std::enable_if_t<std::is_floating_point<T>::value>>
inline CompareFloatsAlmostEqual<T> IsEqual(T value)
{
    return CompareFloatsAlmostEqual<T>(value);
}

template<typename T, typename BinaryFunction,
         typename = std::enable_if_t<!std::is_floating_point<T>::value>>
class CompareGeneric : public Catch::MatcherBase<T>
{
public:
    CompareGeneric(T value, BinaryFunction f, std::string desc) :
        value_{value},
        f_{f},
        desc_{desc}
    {
    }

    bool match(T const & val) const override
    {
        return f_(val, value_);
    }

    std::string describe() const override
    {
        std::ostringstream ss;
        ss << desc_ << ' ' << value_;
        return ss.str();
    }

private:
    T value_;
    BinaryFunction f_;
    std::string desc_;
};

template<typename T,
         typename = std::enable_if_t<!std::is_floating_point<T>::value>>
inline auto IsEqual(T value)
{
    return CompareGeneric<T, decltype(std::equal_to<T>())>(
        value, std::equal_to<T>{}, "==");
}

template<typename T,
         typename = std::enable_if_t<!std::is_floating_point<T>::value>>
inline auto IsNotEqual(T value)
{
    return CompareGeneric<T, decltype(std::not_equal_to<T>())>(
        value, std::not_equal_to<T>{}, "!=");
}

#endif // BRR_MATCHERS_HPP_

