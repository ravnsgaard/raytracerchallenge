#include "catch.hpp"
#include "matchers.hpp"

#include "patterns.hpp"
#include "transforms.hpp"
#include "worldobjects.hpp"

SCENARIO("creating a stripe pattern", "[Pattern]")
{
    GIVEN("pattern = StripePattern(white, black)")
    {
        auto pattern = RT::StripePattern{RT::Color::White(),
                                         RT::Color::Black()};
        THEN("pattern.color1() == white")
        {
            REQUIRE_THAT(pattern.color1(), IsEqual(RT::Color::White()));
        }
        THEN("pattern.color2() == black")
        {
            REQUIRE_THAT(pattern.color2(), IsEqual(RT::Color::Black()));
        }
        THEN("pattern.transform() == identity()")
        {
            REQUIRE_THAT(pattern.transform(), IsEqual(RT::Transform::Identity()));
        }
    }
}

SCENARIO("a stripe pattern is constant in y", "[StripePattern]")
{
    GIVEN("pattern = StripePattern(white, black)")
    {
        auto pattern = RT::StripePattern{RT::Color::White(),
                                         RT::Color::Black()};
        THEN("pattern.colorAt((0,0,0)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 0., 0.)),
                         IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt((0,1,0)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 1., 0.)),
                         IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt((0,2,0)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 2., 0.)),
                         IsEqual(RT::Color::White()));
        }
    }
}

SCENARIO("a stripe pattern is constant in z", "[StripePattern]")
{
    GIVEN("pattern = StripePattern(white, black)")
    {
        auto pattern = RT::StripePattern{RT::Color::White(),
                                         RT::Color::Black()};
        THEN("pattern.colorAt((0,0,0)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 0., 0.)),
                         IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt((0,0,1)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 0., 1.)),
                         IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt((0,0,2)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 0., 2.)),
                         IsEqual(RT::Color::White()));
        }
    }
}

SCENARIO("a stripe patter alternates in x", "[StripePattern]")
{
    GIVEN("pattern = StripePattern(white, black)")
    {
        auto pattern = RT::StripePattern{RT::Color::White(),
                                         RT::Color::Black()};
        THEN("pattern.colorAt((0,0,0)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 0., 0.)),
                         IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt((.9,0,0)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(.9, 0., 0.)),
                         IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt((1,0,0)) = black")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(1., 0., 0.)),
                         IsEqual(RT::Color::Black()));
        }
        THEN("pattern.colorAt((-0.1,0,0)) = black")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(-.1, 0., 0.)),
                         IsEqual(RT::Color::Black()));
        }
        THEN("pattern.colorAt((-1,0,0)) = black")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(-1., 0., 0.)),
                         IsEqual(RT::Color::Black()));
        }
        THEN("pattern.colorAt((-1.1,0,0)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(-1.1, 0., 0.)),
                         IsEqual(RT::Color::White()));
        }
    }
}

SCENARIO("stripes with an object transformation", "[StripePattern]")
{
    GIVEN("object = sphere()")
    {
        auto object = RT::Sphere{};
        AND_GIVEN("object.setTransform(Scaling(2,2,2))")
        {
            object.setTransform(RT::Transform::Scaling(2., 2., 2.));
            AND_GIVEN("pattern = StripePattern(white, black)")
            {
                auto pattern = RT::StripePattern{RT::Color::White(),
                                                 RT::Color::Black()};
                WHEN("c = pattern.colorAt(object, point(1.5,0,0))")
                {
                    auto c = pattern.colorAt(object,
                                             RT::Tuple::Point(1.5, 0., 0.));
                    THEN("c == white")
                    {
                        REQUIRE_THAT(c, IsEqual(RT::Color::White()));
                    }
                }
            }
        }
    }
}

SCENARIO("stripes with a pattern transformation", "[Pattern]")
{
    GIVEN("object = sphere()")
    {
        auto object = RT::Sphere{};
        AND_GIVEN("pattern = StripePattern(white, black)")
        {
            auto pattern = RT::StripePattern{RT::Color::White(),
                                             RT::Color::Black()};
            AND_GIVEN("pattern.setTransform(scaling(2, 2, 2))")
            {
                pattern.setTransform(RT::Transform::Scaling(2., 2., 2.));
                WHEN("c = pattern.colorAt(object, (1.5,0,0))")
                {
                    auto c = pattern.colorAt(object,
                                             RT::Tuple::Point(1.5, 0., 0.));
                    THEN("c = white")
                    {
                        REQUIRE_THAT(c, IsEqual(RT::Color::White()));
                    }
                }
            }
        }
    }
}

SCENARIO("stripes with both an object and a pattern transform", "[Pattern]")
{
    GIVEN("object = sphere()")
    {
        auto object = RT::Sphere{};
        AND_GIVEN("object.setTransform(scaling(2, 2, 2))")
        {
            object.setTransform(RT::Transform::Scaling(2., 2., 2.));
            AND_GIVEN("pattern = StripePattern(white, black)")
            {
                auto pattern = RT::StripePattern{RT::Color::White(),
                                                 RT::Color::Black()};
                AND_GIVEN("pattern.setTransform(translation(0.5,0,0))")
                {
                    pattern.setTransform(
                        RT::Transform::Translation(.5, 0., 0.));
                    WHEN("c = pattern.colorAt(object, point(2.5,0,0))")
                    {
                        auto c = pattern.colorAt(
                            object, RT::Tuple::Point(2.5, 0., 0.));
                        THEN("c = white")
                        {
                            REQUIRE_THAT(c, IsEqual(RT::Color::White()));
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("a gradient linearly interpolates between colors",
         "[GradientPattern]")
{
    GIVEN("pattern = RT::GradientPattern(white, black)")
    {
        auto pattern = RT::GradientPattern{RT::Color::White(),
                                           RT::Color::Black()};
        THEN("pattern.colorAt(point(0,0,0)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 0., 0.)),
                         IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt(point(0.25,0,0)) = color(0.75,0.75,0.75)")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(.25, 0., 0.)),
                         IsEqual(RT::Color{.75, .75, .75}));
        }
        THEN("pattern.colorAt(point(0.5,0,0)) = color(0.5,0.5,0.5)")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(.5, 0., 0.)),
                         IsEqual(RT::Color{.5, .5, .5}));
        }
        THEN("pattern.colorAt(point(0.75,0,0)) = color(0.25,0.25,0.25)")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(.75, 0., 0.)),
                         IsEqual(RT::Color{.25, .25, .25}));
        }
    }
}

SCENARIO("a ring should extend in both x and z", "[RingPattern]")
{
    GIVEN("pattern = RingPattern(white, black)")
    {
        auto pattern = RT::RingPattern{RT::Color::White(), RT::Color::Black()};
        THEN("pattern.colorAt(point(0,0,0)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 0., 0.)),
                         IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt(point(1,0,0)) = black")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(1., 0., 0.)),
                         IsEqual(RT::Color::Black()));
        }
        THEN("pattern.colorAt(point(0,0,1)) = black")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 0., 1.)),
                         IsEqual(RT::Color::Black()));
        }
        THEN("pattern.colorAt(point(0.708,0,0.708)) = black")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(.708, 0., .708)),
                         IsEqual(RT::Color::Black()));
        }
        THEN("pattern.colorAt(point(0.7,0,0.7)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(.7, 0., .7)),
                         IsEqual(RT::Color::White()));
        }
    }
}

SCENARIO("checkers should repeat in x", "[CheckersPattern]")
{
    GIVEN("pattern = CheckersPattern(white, black)")
    {
        auto pattern = RT::CheckersPattern(RT::Color::White(),
                                           RT::Color::Black());
        THEN("pattern.colorAt(point(0,0,0)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 0., 0.)),
                         IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt(point(0.99,0,0)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(.99, 0., 0.)),
                         IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt(point(1.01,0,0)) = black")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(1.01, 0., 0.)),
                         IsEqual(RT::Color::Black()));
        }
    }
}

SCENARIO("checkers should repeat in y", "[CheckersPattern]")
{
    GIVEN("pattern = CheckersPattern(white, black)")
    {
        auto pattern = RT::CheckersPattern(RT::Color::White(),
                                           RT::Color::Black());
        THEN("pattern.colorAt(point(0,0,0)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 0., 0.)),
                         IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt(point(0,0.99,0)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., .99, 0.)),
                         IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt(point(0,1.01,0)) = black")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 1.01, 0.)),
                         IsEqual(RT::Color::Black()));
        }
    }
}

SCENARIO("checkers should repeat in z", "[CheckersPattern]")
{
    GIVEN("pattern = CheckersPattern(white, black)")
    {
        auto pattern = RT::CheckersPattern(RT::Color::White(),
                                           RT::Color::Black());
        THEN("pattern.colorAt(point(0,0,0)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 0., 0.)),
                         IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt(point(0,0,0.99)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 0., .99)),
                         IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt(point(0,0,1.01)) = black")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(0., 0., 1.01)),
                         IsEqual(RT::Color::Black()));
        }
    }
}

SCENARIO("a UV wrap pattern has the colors of its contained pattern", "[Pattern]")
{
    GIVEN("pattern = UVWrapPattern(CheckersPattern)")
    {
        auto pattern = RT::UVWrapPattern{
            RT::CheckersPattern{RT::Color::White(), RT::Color::Black()}};
        THEN("pattern.color1/2 = white/black")
        {
            REQUIRE_THAT(pattern.color1(), IsEqual(RT::Color::White()));
            REQUIRE_THAT(pattern.color2(), IsEqual(RT::Color::Black()));
        }
    }
}

SCENARIO("a UV wrap pattern has the transform of its contained pattern", "[Pattern]")
{
    GIVEN("cp = CheckersPattern(transform)")
    {
        auto cp = RT::CheckersPattern{RT::Color::White(), RT::Color::Black()};
        cp.setTransform(RT::Transform::Scaling(.5, .5, .5));
        AND_GIVEN("pattern = UVWrapPattern(cp)")
        {
            auto pattern = RT::UVWrapPattern{cp};
            THEN("pattern.transform() = cp.transform()")
            {
                REQUIRE_THAT(pattern.transform(), IsEqual(cp.transform()));
            }
        }
    }
}

SCENARIO("a UV wrap pattern maps the pattern on the surface of a sphere", "[Pattern]")
{
    GIVEN("pattern = wrapped checkers(white, black, (0.5,0.5,0.5)")
    {
        auto cp = RT::CheckersPattern{RT::Color::White(), RT::Color::Black()};
        cp.setTransform(RT::Transform::Scaling(.25, .5, .25));
        auto pattern = RT::UVWrapPattern{cp};
        auto const sqrt2by2 = std::sqrt(2.) / 2.;
        THEN("pattern.colorAt(point(-0.01,-0.01,-1)) = black")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(-.01, -.01, -1.)),
                    IsEqual(RT::Color::Black()));
        }
        THEN("pattern.colorAt(point(0.01,-0.01,-1)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(.01, -.01, -1.)),
                    IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt(point(0.01,0.01,-1)) = black")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(.01, .01, -1.)),
                    IsEqual(RT::Color::Black()));
        }
        THEN("pattern.colorAt(point(-0.01,0.01,-1)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(RT::Tuple::Point(-.01, .01, -1.)),
                    IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt(point(-sqrt(2)/2,-sqrt(2)/2,sqrt(2)/2)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(
                             RT::Tuple::Point(-sqrt2by2, -sqrt2by2, sqrt2by2)),
                         IsEqual(RT::Color::White()));
        }
        THEN("pattern.colorAt(point(-sqrt(2)/2,sqrt(2)/2,sqrt(2)/2)) = black")
        {
            REQUIRE_THAT(pattern.colorAt(
                             RT::Tuple::Point(-sqrt2by2, sqrt2by2, sqrt2by2)),
                         IsEqual(RT::Color::Black()));
        }
        THEN("pattern.colorAt(point(sqrt(2)/2,-sqrt(2)/2,sqrt(2)/2)) = black")
        {
            REQUIRE_THAT(pattern.colorAt(
                             RT::Tuple::Point(sqrt2by2, -sqrt2by2, sqrt2by2)),
                         IsEqual(RT::Color::Black()));
        }
        THEN("pattern.colorAt(point(sqrt(2)/2,sqrt(2)/2,sqrt(2)/2)) = white")
        {
            REQUIRE_THAT(pattern.colorAt(
                             RT::Tuple::Point(sqrt2by2, sqrt2by2, sqrt2by2)),
                         IsEqual(RT::Color::White()));
        }
    }
}

