#include "catch.hpp"
#include "matchers.hpp"

#include "transforms.hpp"
#include "tuples.hpp"

#include "utility.hpp"

SCENARIO("translating a point", "[Transform][Tuple]")
{
    GIVEN("a translation(5, -3, 2) transform")
    {
        auto t = RT::Transform::Translation(5., -3., 2.);
        AND_GIVEN("a point <-3,4,5>")
        {
            auto p = RT::Tuple::Point(-3., 4., 5.);
            THEN("multiplication gives a translated point")
            {
                REQUIRE_THAT(t * p, IsEqual(RT::Tuple::Point(2., 1., 7.)));
            }
        }
        AND_GIVEN("it is inverted")
        {
            auto inv = t.inversed();
            AND_GIVEN("a point <-3,4,5>")
            {
                auto p = RT::Tuple::Point(-3., 4., 5.);
                THEN("it translates in 'reverse'")
                {
                    REQUIRE_THAT(inv * p,
                                 IsEqual(RT::Tuple::Point(-8., 7., 3.)));
                }
            }
        }
        AND_GIVEN("a vector <-3,4,5>")
        {
            auto v = RT::Tuple::Vector(-3., 4., 5.);
            THEN("multiplication has no effect")
            {
                REQUIRE_THAT(t * v, IsEqual(v));
            }
        }
    }
}

SCENARIO("multiplying a scaling transform", "[Transform][Tuple]")
{
    GIVEN("a scaling transform (2, 3, 4)")
    {
        auto t = RT::Transform::Scaling(2., 3., 4.);
        AND_GIVEN("a point <-4,6,8>")
        {
            auto p = RT::Tuple::Point(-4., 6., 8.);
            THEN("multiplication scales the point")
            {
                REQUIRE_THAT(t * p, IsEqual(RT::Tuple::Point(-8., 18., 32.)));
                REQUIRE((t * p).isPoint());
            }
        }
        AND_GIVEN("it is inverted")
        {
            auto inv = t.inversed();
            AND_GIVEN("a vector <-4,6,8>")
            {
                auto v = RT::Tuple::Vector(-4., 6., 8.);
                THEN("it scales the opposite way")
                {
                    REQUIRE_THAT(inv * v,
                                 IsEqual(RT::Tuple::Vector(-2., 2., 2.)));
                }
            }
        }
        AND_GIVEN("a vector<-4,6,8>")
        {
            auto v = RT::Tuple::Vector(-4., 6., 8.);
            THEN("multiplication scales the vector")
            {
                REQUIRE_THAT(t * v, IsEqual(RT::Tuple::Vector(-8., 18., 32.)));
                REQUIRE((t * v).isVector());
            }
        }
    }
}

SCENARIO("reflection is scaling by a negative value", "[Transform][Tuple]")
{
    GIVEN("a scaling transform (-1,1,1)")
    {
        auto t = RT::Transform::Scaling(-1., 1., 1.);
        AND_GIVEN("a point <2,3,4>")
        {
            auto p = RT::Tuple::Point(2., 3., 4.);
            THEN("multiplication reflects the point around the x-axis")
            {
                REQUIRE_THAT(t * p, IsEqual(RT::Tuple::Point(-2., 3., 4.)));
            }
        }
    }
}

SCENARIO("rotating a point around the x-axis", "[Transform][Tuple]")
{
    GIVEN("a point (0,1,0)")
    {
        auto p = RT::Tuple::Point(0., 1., 0.);
        AND_GIVEN("half- and full-quarter rotation transforms")
        {
            auto half_quarter = RT::Transform::RotationX(RT::PI() / 4.);
            auto full_quarter = RT::Transform::RotationX(RT::PI() / 2.);
            THEN("multiplication rotates the point by 45 and 90 degrees")
            {
                REQUIRE_THAT(half_quarter * p,
                             IsEqual(RT::Tuple::Point(0., std::sqrt(2.) / 2.,
                                                      std::sqrt(2.) / 2.)));
                auto np = full_quarter * p;
                REQUIRE_THAT(np(0), IsEqual(0.));
                REQUIRE_THAT(np(1), IsEqual(0.));
                REQUIRE_THAT(np(2), IsEqual(1.));
                REQUIRE_THAT(np(3), IsEqual(1.));
            }
        }
    }
}

SCENARIO("Rotating a point around the y-axis", "[Transform][Tuple]")
{
    GIVEN("a point (0,0,1)")
    {
        auto p = RT::Tuple::Point(0., 0., 1.);
        AND_GIVEN("two rotation transforms for 45 and 90 degrees")
        {
            auto half_quarter = RT::Transform::RotationY(RT::PI() / 4.);
            auto full_quarter = RT::Transform::RotationY(RT::PI() / 2.);
            THEN("multiplication rotates around the y-axis")
            {
                REQUIRE_THAT(half_quarter * p,
                             IsEqual(RT::Tuple::Point(std::sqrt(2.) / 2., 0.,
                                                      std::sqrt(2.) / 2.)));
                auto np = full_quarter * p;
                REQUIRE_THAT(np(0), IsEqual(1.));
                REQUIRE_THAT(np(1), IsEqual(0.));
                REQUIRE_THAT(np(2), IsEqual(0.));
                REQUIRE_THAT(np(3), IsEqual(1.));
                REQUIRE_THAT(full_quarter * p, IsEqual(np));
                REQUIRE_THAT(full_quarter * p,
                             IsEqual(RT::Tuple::Point(1., 0., 0.)));
                REQUIRE_THAT(np, IsEqual(RT::Tuple::Point(1., 0., 0.)));
            }
        }
    }
}

SCENARIO("rotating a point around the z-axis", "[Transform][Tuple]")
{
    GIVEN("a point <0,1,0>")
    {
        auto p = RT::Tuple::Point(0., 1., 0.);
        AND_GIVEN("two rotation transforms for 45 and 90 degrees")
        {
            auto half_quarter = RT::Transform::RotationZ(RT::PI() / 4.);
            auto full_quarter = RT::Transform::RotationZ(RT::PI() / 2.);
            THEN("multiplying rotates the point around the z-axis")
            {
                REQUIRE_THAT(half_quarter * p,
                             IsEqual(RT::Tuple::Point(-std::sqrt(2.) / 2.,
                                                      std::sqrt(2.) / 2.,
                                                      0.)));
                REQUIRE_THAT(full_quarter * p,
                             IsEqual(RT::Tuple::Point(-1., 0., 0.)));
            }
        }
    }
}

SCENARIO("a shearing transform moves x in proportion to y",
         "[Transform][Tuple]")
{
    GIVEN("a shearing transform (1,0,0,0,0,0)")
    {
        auto t = RT::Transform::Shearing(1., 0., 0., 0., 0., 0.);
        AND_GIVEN("a point <2,3,4>")
        {
            auto p = RT::Tuple::Point(2., 3., 4.);
            THEN("x is 'skewed' in proportion to y")
            {
                REQUIRE_THAT(t * p, IsEqual(RT::Tuple::Point(5., 3., 4.)));
            }
        }
    }
}

SCENARIO("individual transforms are applied in sequence", "[Transform][Tuple]")
{
    GIVEN("a point <1,0,1>")
    {
        auto p = RT::Tuple::Point(1., 0., 1.);
        AND_GIVEN("a rotation transform")
        {
            auto A = RT::Transform::RotationX(RT::PI() / 2.);
            AND_GIVEN("a scaling transform")
            {
                auto B = RT::Transform::Scaling(5., 5., 5.);
                AND_GIVEN("a translation transform")
                {
                    auto C = RT::Transform::Translation(10., 5., 7.);
                    WHEN("apply rotation first")
                    {
                        auto p2 = A * p;
                        THEN("p2 is rotated")
                        {
                            REQUIRE_THAT(
                                p2, IsEqual(RT::Tuple::Point(1., -1., 0.)));
                        }
                        AND_WHEN("then apply scaling")
                        {
                            auto p3 = B * p2;
                            THEN("p3 is rotated and scaled")
                            {
                                REQUIRE_THAT(
                                    p3,
                                    IsEqual(RT::Tuple::Point(5., -5., 0.)));
                            }
                            AND_WHEN("then apply translation")
                            {
                                auto p4 = C * p3;
                                THEN("p4 is rotated, scaled, and translated")
                                {
                                    REQUIRE_THAT(p4,
                                                 IsEqual(RT::Tuple::Point(
                                                     15., 0., 7.)));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("chained transformations must be applied in reverse order")
{
    GIVEN("a point <1,0,1>")
    {
        auto p = RT::Tuple::Point(1., 0., 1.);
        AND_GIVEN("rotation, scaling, and translation transforms")
        {
            auto A = RT::Transform::RotationX(RT::PI() / 2.);
            auto B = RT::Transform::Scaling(5., 5., 5.);
            auto C = RT::Transform::Translation(10., 5., 7.);
            WHEN("T is C * B * A")
            {
                auto T = C * B * A;
                THEN("applying T rotates, scales, and then translates")
                {
                    REQUIRE_THAT(T * p,
                                 IsEqual(RT::Tuple::Point(15., 0., 7.)));
                }
            }
        }
    }
}

SCENARIO("the transformation matrix for the default orientation",
         "[Transform]")
{
    GIVEN("from = point(0,0,0)")
    {
        auto from = RT::Tuple::Point(0., 0., 0.);
        AND_GIVEN("to = point(0,0,-1)")
        {
            auto to = RT::Tuple::Point(0., 0., -1.);
            AND_GIVEN("up = vector(0., 1., 0.)")
            {
                auto up = RT::Tuple::Vector(0., 1., 0.);
                WHEN("t = Transform::View(from,to,up)")
                {
                    auto t = RT::Transform::View(from, to, up);
                    THEN("t = Transform::Identity()")
                    {
                        REQUIRE_THAT(t, IsEqual(RT::Transform::Identity()));
                    }
                }
            }
        }
    }
}

SCENARIO("a view transform looking in the positive z direction", "[Transform]")
{
    GIVEN("from = point(0,0,0)")
    {
        auto from = RT::Tuple::Point(0., 0., 0.);
        AND_GIVEN("to = point(0,0,1)")
        {
            auto to = RT::Tuple::Point(0., 0., 1.);
            AND_GIVEN("up = vector(0., 1., 0.)")
            {
                auto up = RT::Tuple::Vector(0., 1., 0.);
                WHEN("t = Transform::View(from,to,up)")
                {
                    auto t = RT::Transform::View(from, to, up);
                    THEN("t = Transform::scaling(-1,1,-1)")
                    {
                        REQUIRE_THAT(
                            t, IsEqual(RT::Transform::Scaling(-1., 1., -1.)));
                    }
                }
            }
        }
    }
}

SCENARIO("the view transform moves the world", "[Transform]")
{
    GIVEN("from = point(0,0,8)")
    {
        auto from = RT::Tuple::Point(0., 0., 8.);
        AND_GIVEN("to = point(0,0,0)")
        {
            auto to = RT::Tuple::Point(0., 0., 0.);
            AND_GIVEN("up = vector(0., 1., 0.)")
            {
                auto up = RT::Tuple::Vector(0., 1., 0.);
                WHEN("t = Transform::View(from,to,up)")
                {
                    auto t = RT::Transform::View(from, to, up);
                    THEN("t = Transform::translation(0,0,-8)")
                    {
                        REQUIRE_THAT(
                            t,
                            IsEqual(RT::Transform::Translation(0., 0., -8.)));
                    }
                }
            }
        }
    }
}

SCENARIO("an arbitrary view transform", "[Transform]")
{
    GIVEN("from = point(1,3,2)")
    {
        auto from = RT::Tuple::Point(1., 3., 2.);
        AND_GIVEN("to = point(4,-2,8)")
        {
            auto to = RT::Tuple::Point(4., -2., 8.);
            AND_GIVEN("up = vector(1., 1., 0.)")
            {
                auto up = RT::Tuple::Vector(1., 1., 0.);
                WHEN("t = Transform::View(from,to,up)")
                {
                    auto t = RT::Transform::View(from, to, up);
                    THEN("t = specific matrix pattern")
                    {
                        RT::Matrix4 mat{-0.50709255283710997,
                                        0.50709255283710997,
                                        0.67612340378281321,
                                        -2.3664319132398464,
                                        0.76771593385968018,
                                        0.60609152673132649,
                                        0.12121830534626529,
                                        -2.8284271247461903,
                                        -0.35856858280031811,
                                        0.59761430466719678,
                                        -0.71713716560063623,
                                        0.,
                                        0.,
                                        0.,
                                        0.,
                                        1.};
                        REQUIRE(t == mat);
                    }
                }
            }
        }
    }
}

