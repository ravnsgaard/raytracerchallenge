#include "catch.hpp"
#include "matchers.hpp"

#include "colors.hpp"
#include "lights.hpp"

SCENARIO("a point light has a position and intensity", "[PointLight]")
{
    GIVEN("intensity = color(1,1,1)")
    {
        auto intensity = RT::Color{1., 1., 1.};
        AND_GIVEN("position = point(0,0,0)")
        {
            auto position = RT::Tuple::Point(0., 0., 0.);
            WHEN("light = PointLight(position, intensity)")
            {
                auto light = RT::PointLight{position, intensity};
                THEN("light.position() == position")
                {
                    REQUIRE_THAT(light.position(), IsEqual(position));
                }
                THEN("light.intensity() == intensity")
                {
                    REQUIRE_THAT(light.intensity(), IsEqual(intensity));
                }
            }
        }
    }
}

