#include "catch.hpp"
#include "matchers.hpp"

#include "lights.hpp"
#include "materials.hpp"
#include "renderer.hpp"
#include "worldobjects.hpp"

SCENARIO("lighting with various configurations")
{
    auto m = RT::Material{};
    auto position = RT::Tuple::Point(0., 0., 0.);
    auto inShadow = false;
    GIVEN("the eye between the light and the surface")
    {
        auto eyev = RT::Tuple::Vector(0., 0., -1.);
        auto normalv = RT::Tuple::Vector(0., 0., -1.);
        auto light = RT::PointLight{RT::Tuple::Point(0., 0., -10.),
                                    RT::Color{1., 1., 1.}};
        WHEN("result = lighting(m, light, position, eyev, normalv)")
        {
            auto result = RT::lighting(m, RT::Sphere{}, light, position, eyev,
                                       normalv, inShadow);
            THEN("result == color(1.9,1.9,1.9)")
            {
                REQUIRE_THAT(result, IsEqual(RT::Color{1.9, 1.9, 1.9}));
            }
        }
    }
    GIVEN("the eye between light and surface, eye offset 45°")
    {
        auto eyev = RT::Tuple::Vector(0., std::sqrt(2.) / 2.,
                                      std::sqrt(2.) / 2.);
        auto normalv = RT::Tuple::Vector(0., 0., -1.);
        auto light = RT::PointLight{RT::Tuple::Point(0., 0., -10.),
                                    RT::Color{1., 1., 1.}};
        WHEN("result = lighting(m, light, position, eyev, normalv)")
        {
            auto result = RT::lighting(m, RT::Sphere{}, light, position, eyev,
                                       normalv, inShadow);
            THEN("result == Color(1,1,1)")
            {
                REQUIRE_THAT(result, IsEqual(RT::Color{1., 1., 1.}));
            }
        }
    }
    GIVEN("the eye opposite surface, light offset 45°")
    {
        auto eyev = RT::Tuple::Vector(0., 0., -1.);
        auto normalv = RT::Tuple::Vector(0., 0., -1.);
        auto light = RT::PointLight{RT::Tuple::Point(0., 10., -10.),
                                    RT::Color{1., 1., 1.}};
        WHEN("result = lighting(m, light, position, eyev, normalv)")
        {
            auto result = RT::lighting(m, RT::Sphere{}, light, position, eyev,
                                       normalv, inShadow);
            THEN("result = Color(0.1 + 0.9*sqrt(2)/2)")
            {
                double const sc = .1 + .9 * std::sqrt(2.) / 2.;
                REQUIRE_THAT(result, IsEqual(RT::Color{sc, sc, sc}));
            }
        }
    }
    GIVEN("the light offset 45°, the eye in the reflection vector")
    {
        auto sc = std::sqrt(2.) / 2.;
        auto eyev = RT::Tuple::Vector(0., -sc, -sc);
        auto normalv = RT::Tuple::Vector(0., 0., -1.);
        auto light = RT::PointLight{RT::Tuple::Point(0., 10., -10.),
                                    RT::Color{1., 1., 1.}};
        WHEN("result = lighting(m, light, position, eyev, normalv)")
        {
            auto result = RT::lighting(m, RT::Sphere{}, light, position, eyev,
                                       normalv, inShadow);
            THEN("result = Color(0.1 + 0.9*sqrt(2)/2) + 0.9")
            {
                REQUIRE_THAT(result,
                             IsEqual(RT::Color{.1 + .9 * sc + .9,
                                               .1 + .9 * sc + .9,
                                               .1 + .9 * sc + .9}));
            }
        }
    }
    GIVEN("the light behind the surface")
    {
        auto eyev = RT::Tuple::Vector(0., 0., -1.);
        auto normalv = RT::Tuple::Vector(0., 0., -1.);
        auto light = RT::PointLight{RT::Tuple::Point(0., 0., 10.),
                                    RT::Color{1., 1., 1.}};
        WHEN("result = lighting(m, light, position, eyev, normalv)")
        {
            auto result = RT::lighting(m, RT::Sphere{}, light, position, eyev,
                                       normalv, inShadow);
            THEN("result = Color(0.1, 0.1, 0.1)")
            {
                REQUIRE_THAT(result, IsEqual(RT::Color{.1, .1, .1}));
            }
        }
    }
    GIVEN("the surface in shadow")
    {
        auto eyev = RT::Tuple::Vector(0., 0., -1.);
        auto normalv = RT::Tuple::Vector(0., 0., -1.);
        auto light = RT::PointLight{RT::Tuple::Point(0., 0., -10.),
                                    RT::Color{1., 1., 1.}};
        auto inShadow = true;
        WHEN("result = lighting(m, light, position, eyev, normalv, inShadow)")
        {
            auto result = RT::lighting(m, RT::Sphere{}, light, position, eyev,
                                       normalv, inShadow);
            THEN("result == color(0.1,0.1,0.1)")
            {
                REQUIRE_THAT(result, IsEqual(RT::Color{0.1, 0.1, 0.1}));
            }
        }
    }
}

SCENARIO("lighting with a pattern applied", "[Renderer][StripePattern]")
{
    GIVEN("m = Material with stripe pattern")
    {
        auto m = RT::Material{RT::StripePattern{RT::Color::White(),
                                                RT::Color::Black()},
                              1., 0., 0., 200., 0., 0., 1.};
        AND_GIVEN("eyev = vector(0,0,-1)")
        {
            auto eyev = RT::Tuple::Vector(0., 0., -1.);
            AND_GIVEN("normalv = vector(0,0,-1)")
            {
                auto normalv = RT::Tuple::Vector(0., 0., -1.);
                AND_GIVEN("light = PointLight((0,0,-10), (1,1,1))")
                {
                    auto light = RT::PointLight{RT::Tuple::Point(0., 0., -10.),
                                                RT::Color::White()};
                    WHEN("c1 = lighting(m, light, (0.9,0,0), eyev, normalv, "
                         "false)")
                    {
                        auto c1 = RT::lighting(m, RT::Sphere{}, light,
                                               RT::Tuple::Point(.9, 0., 0.),
                                               eyev, normalv, false);
                        AND_WHEN("c2 = lighting(m, light, (1.1,0,0), eyev, "
                                 "normalv, false)")
                        {
                            auto c2 =
                                RT::lighting(m, RT::Sphere{}, light,
                                             RT::Tuple::Point(1.1, 0., 0.),
                                             eyev, normalv, false);
                            THEN("c1 = white")
                            {
                                REQUIRE_THAT(c1, IsEqual(RT::Color::White()));
                            }
                            THEN("c2 = black")
                            {
                                REQUIRE_THAT(c2, IsEqual(RT::Color::Black()));
                            }
                        }
                    }
                }
            }
        }
    }
}

