#include "catch.hpp"
#include "matchers.hpp"

#include "cameras.hpp"
#include "utility.hpp"
#include "worlds.hpp"

SCENARIO("constructing a camera", "[Camera]")
{
    GIVEN("sizes of 160 and 120 pixels")
    {
        int hsize = 160;
        int vsize = 120;
        AND_GIVEN("field of view of 90°")
        {
            double fov = RT::PI() / 2.;
            WHEN("c = camera(hsize, vsize, fov)")
            {
                auto c = RT::Camera(hsize, vsize, fov);
                THEN("c.hsize() == 160")
                {
                    REQUIRE_THAT(c.hsize(), IsEqual(hsize));
                }
                THEN("c.vsize() == 120")
                {
                    REQUIRE_THAT(c.vsize(), IsEqual(vsize));
                }
                THEN("c.FOV() == PI/2")
                {
                    REQUIRE_THAT(c.FOV(), IsEqual(fov));
                }
                THEN("c.transform() == RT::Transform::Identity()")
                {
                    REQUIRE(c.transform() == RT::Transform::Identity());
                }
            }
        }
    }
}

SCENARIO("the pixel size for a horizontal canvas", "[Camera]")
{
    GIVEN("c = camera(200, 125, PI/2)")
    {
        auto c = RT::Camera{200, 125, RT::PI() / 2.};
        THEN("c.pixelSize() == 0.01")
        {
            REQUIRE_THAT(c.pixelSize(), IsEqual(.01));
        }
    }
}

SCENARIO("the pixel size for a vertical canvas", "[Camera]")
{
    GIVEN("c = camera(125, 200, PI/2)")
    {
        auto c = RT::Camera{125, 200, RT::PI() / 2.};
        THEN("c.pixelSize() == 0.01")
        {
            REQUIRE_THAT(c.pixelSize(), IsEqual(.01));
        }
    }
}

SCENARIO("constructing a ray throught the center of the canvas", "[Camera]")
{
    GIVEN("c = camera(201,101,PI/2)")
    {
        auto c = RT::Camera{201, 101, RT::PI() / 2.};
        WHEN("r = c.rayForPixel(100, 50)")
        {
            auto r = c.rayForPixel(100, 50);
            THEN("r.origin() = point(0,0,0)")
            {
                REQUIRE_THAT(r.origin(),
                             IsEqual(RT::Tuple::Point(0., 0., 0.)));
                AND_THEN("r.direction() = vector(0., 0., -1)")
                {
                    REQUIRE_THAT(r.direction(),
                                 IsEqual(RT::Tuple::Vector(0., 0., -1)));
                };
            }
        }
    }
}

SCENARIO("constructing a ray through a corner of the canvas", "[Camera]")
{
    GIVEN("c = camera(201, 101,PI/2)")
    {
        auto c = RT::Camera{201, 101, RT::PI() / 2.};
        WHEN("r = c.rayForPixel(0, 0)")
        {
            auto r = c.rayForPixel(0, 0);
            THEN("r.origin() = point(0,0,0)")
            {
                REQUIRE_THAT(r.origin(),
                             IsEqual(RT::Tuple::Point(0., 0., 0.)));
            }
            THEN("r.direction() == vector(0.66519, 0.33259, -0.66851)")
            {
                REQUIRE_THAT(r.direction(),
                             IsEqual(RT::Tuple::Vector(0.66518642611945078,
                                                       0.33259321305972539,
                                                       -0.66851235825004807)));
            }
        }
    }
}

SCENARIO("constructing a ray when the camera is transformed", "[Camera]")
{
    GIVEN("c = camera(201, 101,PI/2)")
    {
        auto c = RT::Camera{201, 101, RT::PI() / 2.};
        WHEN("c.setTransform(rotY(PI/4) * trans(0,-2,5))")
        {
            c.setTransform(RT::Transform::RotationY(RT::PI() / 4.) *
                           RT::Transform::Translation(0., -2., 5.));
            AND_WHEN("r = c.rayForPixel(100, 50)")
            {
                auto r = c.rayForPixel(100, 50);
                THEN("r.origin() = point(0,2,-5)")
                {
                    REQUIRE_THAT(r.origin(),
                                 IsEqual(RT::Tuple::Point(0., 2., -5.)));
                }
                THEN("r.direction() == vector(sqrt(2)/2, 0.33259, -sqrt(2)/2)")
                {
                    REQUIRE_THAT(
                        r.direction(),
                        IsEqual(RT::Tuple::Vector(std::sqrt(2.) / 2., 0.,
                                                  -std::sqrt(2.) / 2.)));
                }
            }
        }
    }
}

SCENARIO("rendering a world with a camera", "[Camera]")
{
    GIVEN("w = DefaultWorld()")
    {
        auto w = RT::World::DefaultWorld();
        AND_GIVEN("c = camera(11, 11, PI/2)")
        {
            auto c = RT::Camera{11, 11, RT::PI() / 2.};
            AND_GIVEN("with a view from (0,0,-5) to (0,0,0) with up (0,1,0)")
            {
                auto from = RT::Tuple::Point(0., 0., -5.);
                auto to = RT::Tuple::Point(0., 0., 0.);
                auto up = RT::Tuple::Vector(0., 1., 0.);
                c.setTransform(RT::Transform::View(from, to, up));
                WHEN("image = c.render(w)")
                {
                    auto image = c.render(w);
                    THEN("image.at(5, 5) = color(0.38066,0.47583,0,2855)")
                    {
                        REQUIRE_THAT(image.at(5, 5),
                                     IsEqual(RT::Color{0.38066119308103435,
                                                       0.47582649135129296,
                                                       0.28549589481077575}));
                    }
                }
            }
        }
    }
}

SCENARIO("replicating the refractions example", "[Refractions]")
{
    using namespace RT;
    auto w = World{};
    w.addObject<Plane>(Material{CheckersPattern{Color::White(), Color::Black()},
            .1, .9, .9, 200., .1, .0, 1.});
    w.addObject<Plane>(
        Material{CheckersPattern{Color{.1, .8, .1}, Color{.05, .05, .05}}, .1,
                 .9, .9, 200., .1, .0, 1.},
        Transform::Translation(0., 0., 10.) * Transform::RotationX(PI() / 2.));
    w.addObject<Plane>(
        Material{CheckersPattern{Color{.1, .8, .1}, Color{.05, .05, .05}}, .1,
                 .9, .9, 200., .1, 0., 1.},
        Transform::Translation(5., 0., 0.) * Transform::RotationZ(PI() / 2.));
    auto sphere = w.addObject<Sphere>(Sphere::GlassSphere());
    sphere->setTransform(Transform::Translation(0., 1., 0.));
    w.addLight({Tuple::Point(-10., 10., -10), Color::White()});

    auto c = Camera(81, 45, PI() / 4.);
    auto from = Tuple::Point(-3., 3., -5.);
    auto to = Tuple::Point(0., 1., 0.);
    auto up = Tuple::Vector(0., 1., 0.);
    c.setTransform(Transform::View(from, to, up));

    auto r = c.rayForPixel(40, 30);
    auto color = w.colorAt(r);
    REQUIRE_THAT(color,
                 IsEqual(Color{0.076944321912638031, 0.57087174274414099,
                               0.076944321912638031}));
}
