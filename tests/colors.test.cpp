#include "catch.hpp"
#include "matchers.hpp"

#include "colors.hpp"

SCENARIO("colors are <red,green,blue> tuples", "[Color]")
{
    GIVEN("a color 'c' <-0.5,0.4,1.7>")
    {
        RT::Color c{-0.5l, 0.4l, 1.7l};
        THEN("individual components can be accessed")
        {
            REQUIRE_THAT(c.red(), IsEqual(-.5));
            REQUIRE_THAT(c.green(), IsEqual(.4));
            REQUIRE_THAT(c.blue(), IsEqual(1.7));
        }
    }
    GIVEN("a default-constructed color 'c'")
    {
        RT::Color c;
        THEN("the color is black")
        {
            REQUIRE_THAT(c.red(), IsEqual(0.));
            REQUIRE_THAT(c.green(), IsEqual(0.));
            REQUIRE_THAT(c.blue(), IsEqual(0.));
        }
    }
}

SCENARIO("adding colors", "[Color]")
{
    GIVEN("a color 'c1' <.9,.6,.75>")
    {
        RT::Color c1{.9l, .6l, .75l};
        AND_GIVEN("a color 'c2' <.7,.1,.25>")
        {
            RT::Color c2{.7l, .1l, .25l};
            THEN("c1 + c2 gives the sum of the components")
            {
                auto r = c1 + c2;
                REQUIRE_THAT(r.red(), IsEqual(1.6));
                REQUIRE_THAT(r.green(), IsEqual(0.7));
                REQUIRE_THAT(r.blue(), IsEqual(1.));
            }
        }
    }
}

SCENARIO("subtracting colors", "[Color]")
{
    GIVEN("a color 'c1' <.9,.6,.75>")
    {
        RT::Color c1{.9l, .6l, .75l};
        AND_GIVEN("a color 'c2' <.7,.1,.25>")
        {
            RT::Color c2{.7l, .1l, .25l};
            THEN("c1 - c2 gives the difference of the components")
            {
                auto r = c1 - c2;
                REQUIRE_THAT(r.red(), IsEqual(.2));
                REQUIRE_THAT(r.green(), IsEqual(.5));
                REQUIRE_THAT(r.blue(), IsEqual(.5));
            }
        }
    }
}

SCENARIO("multiplying a color by a scalar", "[Color]")
{
    GIVEN("a color 'c' <.2,.3,.4>")
    {
        RT::Color c{.2, .3, .4};
        AND_GIVEN("a scalar 's'")
        {
            auto s = 2.l;
            THEN("c * s multiplies all components with s")
            {
                REQUIRE_THAT(c * s, IsEqual(RT::Color{.4, .6, .8}));
            }
            THEN("s * c multiplies all components with s")
            {
                REQUIRE_THAT(s * c, IsEqual(RT::Color{.4, .6, .8}));
            }
        }
    }
}

SCENARIO("multiplying colors", "[Color]")
{
    GIVEN("a color 'c1' <1,.2,.4>")
    {
        RT::Color c1{1., .2, .4};
        AND_GIVEN("a color 'c2' <.9,1,.1>")
        {
            RT::Color c2{.9, 1., .1};
            THEN("c1 * c2 multiplies each component")
            {
                REQUIRE_THAT(c1 * c2, IsEqual(RT::Color{.9, .2, .04}));
            }
        }
    }
}

