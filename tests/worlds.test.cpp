#include "catch.hpp"
#include "matchers.hpp"

#include "lights.hpp"
#include "patterns.hpp"
#include "rays.hpp"
#include "worlds.hpp"

class TestPattern : public RT::Pattern
{
public:
    TestPattern() noexcept :
        RT::Pattern{RT::Color::Black(), RT::Color::Black()}
    {
    }

private:
    TestPattern * createClone() const override
    {
        return new TestPattern{*this};
    }

    RT::Color findColorAt(RT::Tuple const & point) const noexcept override
    {
        return {point(0), point(1), point(2)};
    }
};

SCENARIO("creating a world", "[World]")
{
    GIVEN("w = world()")
    {
        auto w = RT::World{};
        THEN("w is empty")
        {
            REQUIRE_THAT(w.objects().size(), IsEqual(0));
            REQUIRE_THAT(w.lights().size(), IsEqual(0));
        }
    }
}

SCENARIO("the default world", "[World]")
{
    GIVEN("light = PointLight(point(-10,10,-10), color(1,1,1))")
    {
        auto light = RT::PointLight{RT::Tuple::Point(-10., 10., -10.),
                                    RT::Color{1., 1., 1.}};
        AND_GIVEN("two spheres arranged 'inside out'")
        {
            auto s1 = RT::Sphere{
                RT::Material{{.8, 1., .6}, .1, .7, .2, 200., 0., 0., 1.}};

            auto s2 = RT::Sphere{RT::Transform::Scaling(.5, .5, .5)};
            WHEN("w = World::DefaultWorld()")
            {
                auto w = RT::World::DefaultWorld();
                THEN("w.light() = light")
                {
                    REQUIRE(std::find(w.lights().begin(), w.lights().end(),
                                      light) != w.lights().end());
                }
                THEN("w contains s1 and s2")
                {
                    REQUIRE(std::find_if(w.objects().begin(),
                                         w.objects().end(),
                                         [&s1](auto const & obj) {
                                             return s1 == *obj;
                                         }) != w.objects().end());
                    REQUIRE(std::find_if(w.objects().begin(),
                                         w.objects().end(),
                                         [&s2](auto const & obj) {
                                             return s2 == *obj;
                                         }) != w.objects().end());
                }
            }
        }
    }
}

SCENARIO("intersect a world with a ray", "[World]")
{
    GIVEN("w = DefaultWorld()")
    {
        auto w = RT::World::DefaultWorld();
        AND_GIVEN("r = ray(point(0,0,-5), vector(0,0,1))")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., 0., -5.),
                             RT::Tuple::Vector(0., 0., 1.)};
            WHEN("xs = w.intersect(r)")
            {
                auto xs = w.intersects(r);
                THEN("xs.size() == 4")
                {
                    REQUIRE_THAT(xs.size(), IsEqual(4));
                    AND_THEN("the t values are 4, 4.5, 5.5, and 6")
                    {
                        REQUIRE_THAT(xs[0].time(), IsEqual(4.));
                        REQUIRE_THAT(xs[1].time(), IsEqual(4.5));
                        REQUIRE_THAT(xs[2].time(), IsEqual(5.5));
                        REQUIRE_THAT(xs[3].time(), IsEqual(6.));
                    }
                }
            }
        }
    }
}

SCENARIO("shading an intersection", "[World]")
{
    GIVEN("w = DefaultWorld()")
    {
        auto w = RT::World::DefaultWorld();
        AND_GIVEN("r = ray((0,0,-5),(0,0,1))")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., 0., -5.),
                             RT::Tuple::Vector(0., 0., 1.)};
            AND_GIVEN("shape = the first object in w")
            {
                auto shape = w.objects().begin()->get();
                AND_GIVEN("i = intersection(shape, 4.)")
                {
                    auto i = RT::Intersection{shape, 4.};
                    WHEN("comps = i.precompute(r)")
                    {
                        auto comps = i.precompute(r, {});
                        AND_WHEN("c = w.shadeHit(comps)")
                        {
                            auto c = w.shadeHit(comps);
                            THEN("c is <0.38066,0.47583,0.2855>")
                            {
                                auto color = RT::Color{0.380661193081034,
                                                       0.47582649135129296,
                                                       0.285495894810776};
                                REQUIRE_THAT(c, IsEqual(color));
                            }
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("shading an intersection from the inside", "[World]")
{
    GIVEN("w = DefaultWorld()")
    {
        auto w = RT::World::DefaultWorld();
        AND_GIVEN("w.light = PointLight((0,0.25,0), (1,1,1))")
        {
            w.clearLights();
            w.addLight(RT::PointLight{RT::Tuple::Point(0., .25, 0.),
                                      RT::Color{1., 1., 1.}});
            AND_GIVEN("r = ray((0,0,0), (0,0,1))")
            {
                auto r = RT::Ray{RT::Tuple::Point(0., 0., 0.),
                                 RT::Tuple::Vector(0., 0., 1.)};
                AND_GIVEN("shape = the second object in w")
                {
                    auto shape = (++w.objects().begin())->get();
                    AND_GIVEN("i = intersection(shape, 0.5)")
                    {
                        auto i = RT::Intersection{shape, .5};
                        WHEN("comps = i.precompute(r)")
                        {
                            auto comps = i.precompute(r, {});
                            AND_WHEN("c = w.shadeHit(comps)")
                            {
                                auto c = w.shadeHit(comps);
                                THEN("c = color(0.90498,0.90498,0.90498)")
                                {
                                    RT::Color color{.904984472083258,
                                                    .904984472083258,
                                                    .904984472083258};
                                    REQUIRE_THAT(c, IsEqual(color));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("the color when a ray misses", "[World]")
{
    GIVEN("w = DefaultWorld()")
    {
        auto w = RT::World::DefaultWorld();
        AND_GIVEN("r = ray((0,0,-5), (0,1,0))")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., 0., -5.),
                             RT::Tuple::Vector(0., 1., 0.)};
            WHEN("c = w.colorAt(r)")
            {
                auto c = w.colorAt(r);
                THEN("c is black")
                {
                    REQUIRE_THAT(c, IsEqual(RT::Color::Black()));
                }
            }
        }
    }
}

SCENARIO("the color when the ray hits")
{
    GIVEN("w = DefaultWorld()")
    {
        auto w = RT::World::DefaultWorld();
        AND_GIVEN("r = ray((0,0,-5), (0,0,1))")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., 0., -5.),
                             RT::Tuple::Vector(0., 0., 1.)};
            WHEN("c = w.colorAt(r)")
            {
                auto c = w.colorAt(r);
                THEN("c = color(0.38066,0.47583,0.2855)")
                {
                    RT::Color color{0.380661193081034, 0.475826491351293,
                                    0.285495894810776};
                    REQUIRE_THAT(c, IsEqual(color));
                }
            }
        }
    }
}

SCENARIO("the color with an intersection behind the ray", "[World]")
{
    GIVEN("w = DefaultWorld")
    {
        auto w = RT::World::DefaultWorld();
        AND_GIVEN("outer = the first object in w (with ambience 1)")
        {
            auto iter = w.objects().begin();
            auto outer = iter->get();
            auto m = outer->material();
            m.setAmbient(1.);
            outer->setMaterial(m);
            AND_GIVEN("inner = the second object in w (with ambience 1)")
            {
                auto inner = (++iter)->get();
                m = inner->material();
                m.setAmbient(1.);
                inner->setMaterial(m);
                AND_GIVEN("r = ray((0,0,0.75), (0,0,-1))")
                {
                    auto r = RT::Ray{RT::Tuple::Point(0., 0., .75),
                                     RT::Tuple::Vector(0., 0., -1.)};
                    WHEN("c = w.colorAt(r)")
                    {
                        auto c = w.colorAt(r);
                        THEN("c = inner->material().color")
                        {
                            REQUIRE_THAT(c,
                                         IsEqual(inner->material().color()));
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("Cleaning house", "[World]")
{
    GIVEN("w = DefaultWorld()")
    {
        auto w = RT::World::DefaultWorld();
        WHEN("w.clearLights()")
        {
            w.clearLights();
            THEN("w.lights().size() == 0")
            {
                REQUIRE_THAT(w.lights().size(), IsEqual(0));
            }
        }
        WHEN("w.clearObjects()")
        {
            w.clearObjects();
            THEN("w.objects().size() == 0")
            {
                REQUIRE_THAT(w.objects().size(), IsEqual(0));
            }
        }
    }
}

SCENARIO("no shadow when nothing lies between the point and the light",
         "[World]")
{
    GIVEN("w = DefaultWorld()")
    {
        auto w = RT::World::DefaultWorld();
        AND_GIVEN("p = point(0,10,0)")
        {
            auto p = RT::Tuple::Point(0., 10., 0.);
            THEN("w.isShadowed(p) == false")
            {
                REQUIRE_FALSE(w.isShadowed(*(w.lights().begin()), p));
            }
        }
    }
}

SCENARIO("shadow when an object is between the point and the light", "[World]")
{
    GIVEN("w = DefaultWorld()")
    {
        auto w = RT::World::DefaultWorld();
        AND_GIVEN("p = point(10, -10, 10)");
        {
            auto p = RT::Tuple::Point(10., -10., 10.);
            THEN("w.isShadowed(p) == true")
            {
                REQUIRE(w.isShadowed(*(w.lights().begin()), p));
            }
        }
    }
}

SCENARIO("no shadow when an object is behind the light", "[World]")
{
    GIVEN("w = DefaultWorld()")
    {
        auto w = RT::World::DefaultWorld();
        AND_GIVEN("p = point(-20, 20, -20)")
        {
            auto p = RT::Tuple::Point(-20., 20., -20.);
            THEN("w.isShadowed(w) == false")
            {
                REQUIRE_FALSE(w.isShadowed(*(w.lights().begin()), p));
            }
        }
    }
}

SCENARIO("no shadow when an object is behind the point", "[World]")
{
    GIVEN("w = DefaultWorld()")
    {
        auto w = RT::World::DefaultWorld();
        AND_GIVEN("p = point(-2,2,-2)")
        {
            auto p = RT::Tuple::Point(-2., 2., -2.);
            THEN("w.isShadowed(p) == false")
            {
                REQUIRE_FALSE(w.isShadowed(*(w.lights().begin()), p));
            }
        }
    }
}

SCENARIO("shadeHit is given an intersection in shadow", "[World]")
{
    GIVEN("a specific world")
    {
        auto w = RT::World{};
        w.addLight({RT::Tuple::Point(0., 0., -10.), RT::Color{1., 1., 1.}});
        w.addObject<RT::Sphere>(RT::Material{},
                                RT::Transform::Translation(0., 0., 10.));
        w.addObject<RT::Sphere>(RT::Sphere{});
        AND_GIVEN("r = ray((0,0,5), (0,0,1))")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., 0., 5.),
                             RT::Tuple::Vector(0., 0., 1.)};
            AND_GIVEN("i = intersection(first sphere, 4)")
            {
                auto i = RT::Intersection{w.objects().front().get(), 4};
                WHEN("comps = i.precompute(r)")
                {
                    auto comps = i.precompute(r, {});
                    AND_WHEN("c = w.shadeHit(comps)")
                    {
                        auto c = w.shadeHit(comps);
                        THEN("c == color(.1, .1, .1)")
                        {
                            REQUIRE_THAT(c, IsEqual(RT::Color{.1, .1, .1}));
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("the reflected color for a nonreflective material",
         "[World][Reflection]")
{
    GIVEN("w = DefaultWorld()")
    {
        auto w = RT::World::DefaultWorld();
        AND_GIVEN("r = ray((0,0,0), (0,0,1))")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., 0., 0.),
                             RT::Tuple::Vector(0., 0., 1.)};
            AND_GIVEN("shape = the second object in w")
            {
                auto & shape = w.objects()[1];
                AND_GIVEN("shape.material.ambient = 1")
                {
                    auto m = shape->material();
                    m.setAmbient(1.);
                    shape->setMaterial(m);
                    AND_GIVEN("i = intersection(shape, 1)")
                    {
                        auto i = RT::Intersection{shape.get(), 1.};
                        WHEN("comps = i.precompute(r)")
                        {
                            auto comps = i.precompute(r, {});
                            AND_WHEN("color = w.reflectedColor(comps)")
                            {
                                auto color = w.reflectedColor(comps);
                                THEN("color = black")
                                {
                                    REQUIRE_THAT(color,
                                                 IsEqual(RT::Color::Black()));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("the reflected color for a reflective material",
         "[World][Reflection]")
{
    auto w = RT::World::DefaultWorld();
    GIVEN("add a plane with reflective = 0.5  at y=-1")
    {
        RT::Material m;
        m.setReflective(.5);
        auto shape =
            w.addObject<RT::Plane>(m, RT::Transform::Translation(0., -1., 0.));
        AND_GIVEN("r = ray((0,0,-3), (0,-sqrt(2)/2,sqrt(2)/2))")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., 0., -3.),
                             RT::Tuple::Vector(0., -std::sqrt(2.) / 2.,
                                               std::sqrt(2.) / 2.)};
            AND_GIVEN("i = intersection(plane, sqrt(2))")
            {
                auto i = RT::Intersection{shape, std::sqrt(2.)};
                WHEN("comps = i.precompute(r)")
                {
                    auto comps = i.precompute(r, {});
                    AND_WHEN("color = w.reflectedColor(comps)")
                    {
                        auto color = w.reflectedColor(comps);
                        THEN("color = (0.19032,0.2379,0.14274)")
                        {
                            REQUIRE_THAT(
                                color,
                                IsEqual(RT::Color{.19033059654114434,
                                                  .23791324567643041,
                                                  .14274794740585825}));
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("shadeHit() for a reflective material", "[World][Reflection]")
{
    auto w = RT::World::DefaultWorld();
    GIVEN("add a plane with reflective = 0.5  at y=-1")
    {
        RT::Material m;
        m.setReflective(.5);
        auto shape =
            w.addObject<RT::Plane>(m, RT::Transform::Translation(0., -1., 0.));
        AND_GIVEN("r = ray((0,0,-3), (0,-sqrt(2)/2,sqrt(2)/2))")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., 0., -3.),
                             RT::Tuple::Vector(0., -std::sqrt(2.) / 2.,
                                               std::sqrt(2.) / 2.)};
            AND_GIVEN("i = intersection(plane, sqrt(2))")
            {
                auto i = RT::Intersection{shape, std::sqrt(2.)};
                WHEN("comps = i.precompute(r)")
                {
                    auto comps = i.precompute(r, {});
                    AND_WHEN("color = w.shadeHit(comps)")
                    {
                        auto color = w.shadeHit(comps);
                        THEN("color = (0.87677,0.92436,0.82918)")
                        {
                            REQUIRE_THAT(
                                color,
                                IsEqual(RT::Color{.87675598552264578,
                                                  .92433863465793187,
                                                  .82917333638735968}));
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("colorAt() with mutually reflective surfaces", "[World][Reflection]")
{
    auto w = RT::World{};
    GIVEN("w.light = ((0,0,0),(1,1,1))")
    {
        w.addLight(
            RT::PointLight{RT::Tuple::Point(0., 0., 0.), RT::Color::White()});
        AND_GIVEN("two parallel reflective planes")
        {
            w.addObject<RT::Plane>(
                RT::Material{{1., 1., 1.}, .1, .9, .9, 200., 1., 0., 1.},
                RT::Transform::Translation(0., -1., 0.));
            w.addObject<RT::Plane>(
                RT::Material{{1., 1., 1.}, .1, .9, .9, 200., 1., 0., 1.},
                RT::Transform::Translation(0., 1., 0.));
            AND_GIVEN("r = ray((0,0,0), (0,1,0))")
            {
                auto r = RT::Ray{RT::Tuple::Point(0., 0., 0.),
                                 RT::Tuple::Vector(0., 1., 0.)};
                THEN("w.colorAt(r) should terminate successfully")
                {
                    CHECK_NOTHROW(w.colorAt(r));
                }
            }
        }
    }
}

SCENARIO("the reflected color at the maximum recursive depth",
         "[World][Reflection]")
{
    auto w = RT::World::DefaultWorld();
    GIVEN("add a plane with reflective = 0.5  at y=-1")
    {
        auto shape = w.addObject<RT::Plane>(
            RT::Material{{1., 1., 1.}, .1, .9, .9, 200., .5, 0., 1.},
            RT::Transform::Translation(0., -1., 0.));
        AND_GIVEN("r = ray((0,0,-3), (0,-sqrt(2)/2,sqrt(2)/2))")
        {
            auto r = RT::Ray{RT::Tuple::Point(0., 0., -3.),
                             RT::Tuple::Vector(0., -std::sqrt(2.) / 2.,
                                               std::sqrt(2.) / 2.)};
            AND_GIVEN("i = intersection(plane, sqrt(2))")
            {
                auto i = RT::Intersection{shape, std::sqrt(2.)};
                WHEN("comps = i.precompute(r)")
                {
                    auto comps = i.precompute(r, {});
                    AND_WHEN("color = w.reflectedColor(comps, 0)")
                    {
                        auto color = w.reflectedColor(comps, 0);
                        THEN("color = black")
                        {
                            REQUIRE_THAT(color, IsEqual(RT::Color::Black()));
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("the refracted color with an opaque surface", "[Refraction]")
{
    GIVEN("w = DefaultWorld()")
    {
        auto w = RT::World::DefaultWorld();
        AND_GIVEN("shape = the first object in w")
        {
            auto shape = w.objects().begin()->get();
            AND_GIVEN("r = ray(0,0,-5)->(0,0,1)")
            {
                auto r = RT::Ray{RT::Tuple::Point(0., 0., -5),
                                 RT::Tuple::Vector(0., 0., 1.)};
                AND_GIVEN("xs = intersections(shape:4, shape:6)")
                {
                    std::vector<RT::Intersection> xs{{shape, 4.}, {shape, 6.}};
                    WHEN("comps = xs[0].precompute(r, xs)")
                    {
                        auto comps = xs[0].precompute(r, xs);
                        AND_WHEN("c = w.refractedColor(comps)")
                        {
                            auto c = w.refractedColor(comps);
                            THEN("c = black")
                            {
                                REQUIRE_THAT(c, IsEqual(RT::Color::Black()));
                            }
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("the refracted color at the maximum recursive depth", "[Refraction]")
{
    auto w = RT::World::DefaultWorld();
    GIVEN("shape = first object in default world, w")
    {
        auto shape = w.objects().begin()->get();
        AND_GIVEN("shape's material is like glass")
        {
            auto m = shape->material();
            m.setTransparency(1.);
            m.setRefractiveIndex(1.5);
            shape->setMaterial(m);
            AND_GIVEN("r = ray(0,0,-5)->(0,0,1)")
            {
                auto r = RT::Ray{RT::Tuple::Point(0., 0., -5.),
                                 RT::Tuple::Vector(0., 0., 1.)};
                AND_GIVEN("xs = intersections(shape:4, shape:6)")
                {
                    std::vector<RT::Intersection> xs{{shape, 4.}, {shape, 6.}};
                    WHEN("comps = xs[0].precompute(r, xs)")
                    {
                        auto comps = xs[0].precompute(r, xs);
                        AND_WHEN("c = w.refractedColor(comps, 0)")
                        {
                            auto c = w.refractedColor(comps, 0);
                            THEN("c is black")
                            {
                                REQUIRE_THAT(c, IsEqual(RT::Color::Black()));
                            }
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("the refracted color under total internal reflection",
         "[World][Refraction]")
{
    auto w = RT::World::DefaultWorld();
    GIVEN("shape = the first object in the default world, w")
    {
        auto shape = w.objects().begin()->get();
        AND_GIVEN("shape's material is like glass")
        {
            auto m = shape->material();
            m.setTransparency(1.);
            m.setRefractiveIndex(1.5);
            shape->setMaterial(m);
            AND_GIVEN("r = ray(0,0,sqrt(2)/2)->(0,1,0)")
            {
                auto r = RT::Ray{RT::Tuple::Point(0., 0., std::sqrt(2.) / 2.),
                                 RT::Tuple::Vector(0., 1., 0.)};
                AND_GIVEN("xs = intersections(shape:-sqrt(2)/2, "
                          "shape:sqrt(2)/2)")
                {
                    std::vector<RT::Intersection> xs{
                        {shape, -std::sqrt(2.) / 2.},
                        {shape, std::sqrt(2.) / 2.}};
                    WHEN("comps = xs[1].precompute(r, xs)")
                    {
                        auto comps = xs[1].precompute(r, xs);
                        AND_WHEN("c = w.refractedColor(comps)")
                        {
                            auto c = w.refractedColor(comps);
                            THEN("c is black")
                            {
                                REQUIRE_THAT(c, IsEqual(RT::Color::Black()));
                            }
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("the refracted color with a refracted ray", "[World][Refraction]")
{
    auto w = RT::World::DefaultWorld();
    GIVEN("A = the first object in the default world, w, with 100% ambience")
    {
        auto A = w.objects().begin()->get();
        A->setMaterial({TestPattern{}, 1., .7, .2, 200., 0., 0., 1.});
        AND_GIVEN("B = the second object in w, in a glassy material")
        {
            auto B = (++(w.objects().begin()))->get();
            B->setMaterial(
                {RT::Color::Black(), .1, .9, .9, 200., 0., 1., 1.5});
            AND_GIVEN("r = ray(0,0,0.1)->(0,1,0)")
            {
                auto r = RT::Ray{RT::Tuple::Point(0., 0., .1),
                                 RT::Tuple::Vector(0., 1., 0.)};
                AND_GIVEN("xs = intersections(A:-.9899, B:-.4899, B:.4899, "
                          "A:.9899)")
                {
                    std::vector<RT::Intersection> xs{
                        {A, -.9899}, {B, -.4899}, {B, .4899}, {A, .9899}};
                    WHEN("comps = xs[2].precompute(r, xs)")
                    {
                        auto comps = xs[2].precompute(r, xs);
                        AND_WHEN("c = w.refractedColor(comps)")
                        {
                            auto c = w.refractedColor(comps);
                            THEN("c = color(0., 0.99888, 0.04725)")
                            {
                                REQUIRE_THAT(
                                    c,
                                    IsEqual(RT::Color{0., 0.99888468279918152,
                                                      0.047216421604969479}));
                            }
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("shadeHit() with a transparent material", "[World][Refraction]")
{
    auto w = RT::World::DefaultWorld();
    GIVEN("a glass floor in the default world")
    {
        auto floor = w.addObject<RT::Plane>(
            RT::Material{{1., 1., 1.}, .1, .9, .9, 200., 0., .5, 1.5},
            RT::Transform::Translation(0., -1., 0.));
        AND_GIVEN("a ball added below the floor")
        {
            w.addObject<RT::Sphere>(
                RT::Material{{1., 0., 0.}, .5, .9, .9, 200., 0., 0., 1.},
                RT::Transform::Translation(0, -3.5, -.5));
            AND_GIVEN("r = ray(0,0,-3)->(0,-sqrt(2)/2,sqrt(2)/2)")
            {
                auto r = RT::Ray{RT::Tuple::Point(0., 0., -3.),
                                 RT::Tuple::Vector(0., -std::sqrt(2.) / 2.,
                                                   std::sqrt(2.) / 2.)};
                AND_GIVEN("xs = intersections(floor:sqrt(2))")
                {
                    std::vector<RT::Intersection> xs{{floor, std::sqrt(2.)}};
                    WHEN("comps = xs[0].precompute(r, xs)")
                    {
                        auto comps = xs[0].precompute(r, xs);
                        AND_WHEN("color = w.shadeHit(comps)")
                        {
                            auto color = w.shadeHit(comps);
                            THEN("color = color(0.93642,0.68642,0.68642)")
                            {
                                REQUIRE_THAT(
                                    color,
                                    IsEqual(RT::Color{0.93642538898150141,
                                                      0.68642538898150141,
                                                      0.68642538898150141}));
                            }
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("shadeHit() with a transparent and reflective material",
         "[World][Refraction]")
{
    auto w = RT::World::DefaultWorld();
    GIVEN("a glass floor in the default world")
    {
        auto floor = w.addObject<RT::Plane>(
            RT::Material{{1., 1., 1.}, .1, .9, .9, 200., .5, .5, 1.5},
            RT::Transform::Translation(0., -1., 0.));
        AND_GIVEN("a ball added below the floor")
        {
            w.addObject<RT::Sphere>(
                RT::Material{{1., 0., 0.}, .5, .9, .9, 200., 0., 0., 1.},
                RT::Transform::Translation(0, -3.5, -.5));
            AND_GIVEN("r = ray(0,0,-3)->(0,-sqrt(2)/2,sqrt(2)/2)")
            {
                auto r = RT::Ray{RT::Tuple::Point(0., 0., -3.),
                                 RT::Tuple::Vector(0., -std::sqrt(2.) / 2.,
                                                   std::sqrt(2.) / 2.)};
                AND_GIVEN("xs = intersections(floor:sqrt(2))")
                {
                    std::vector<RT::Intersection> xs{{floor, std::sqrt(2.)}};
                    WHEN("comps = xs[0].precompute(r, xs)")
                    {
                        auto comps = xs[0].precompute(r, xs);
                        AND_WHEN("color = w.shadeHit(comps)")
                        {
                            auto color = w.shadeHit(comps);
                            THEN("color = color(0.93391,0.69643,0.69243)")
                            {
                                REQUIRE_THAT(
                                    color,
                                    IsEqual(RT::Color{0.9339151405502224,
                                                      0.69643422629376905,
                                                      0.69243069136886204}));
                            }
                        }
                    }
                }
            }
        }
    }
}

