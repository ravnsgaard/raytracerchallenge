#ifndef BRR_RT_WORLDOBJECTS_FWD_HPP_
#define BRR_RT_WORLDOBJECTS_FWD_HPP_

namespace RT
{
class WorldObject;
class Intersection;

} // namespace RT

#endif // BRR_RT_WORLDOBJECTS_FWD_HPP_

