#ifndef BRR_RT_RAYS_HPP_
#define BRR_RT_RAYS_HPP_

#include "rays_fwd.hpp"
#include "transforms_fwd.hpp"
#include "tuples.hpp"

namespace RT
{
class Ray
{
public:
    Ray(Tuple const & origin, Tuple const & direction) noexcept;

    Tuple origin() const noexcept;
    Tuple direction() const noexcept;

    Ray & operator*=(Transform const & t) noexcept;
    Ray operator*(Transform const & t) const noexcept;

private:
    Tuple origin_;
    Tuple direction_;
};

} // namespace RT

#endif // BRR_RT_RAYS_HPP_

