#ifndef BRR_RT_CAMERAS_HPP_
#define BRR_RT_CAMERAS_HPP_

#include "canvas.hpp"
#include "rays.hpp"
#include "transforms.hpp"
#include "worlds.hpp"

#include <cstddef>

namespace RT
{
class Camera
{
public:
    using size_type = std::size_t;
    using scalar_t = double;
    Camera(size_type hsize, size_type vsize, scalar_t fov) noexcept;

    size_type hsize() const noexcept;
    size_type vsize() const noexcept;
    scalar_t pixelSize() const noexcept;
    scalar_t FOV() const noexcept;
    Transform transform() const noexcept;
    void setTransform(Transform const & t) noexcept;

    Ray rayForPixel(size_type x, size_type y) const;
    Canvas render(World const & world) const;

private:
    /// \effects Calculates halfWidth_, halfHeight_, and pixelSize_.
    /// Uses hsize_, vsize_, and fov_ for the calculation
    /// Should be called if any of these figures change.
    /// \requires hsize_ must not be 0. fov_ cannot be `\pi`.
    void init() noexcept;

    size_type hsize_;
    size_type vsize_;
    scalar_t fov_;
    Transform transform_;
    scalar_t halfWidth_;
    scalar_t halfHeight_;
    scalar_t pixelSize_;
};
} // namespace RT

#endif // BRR_RT_CAMERAS_HPP_

