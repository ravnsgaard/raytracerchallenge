#ifndef BRR_RT_COLORS_HPP_
#define BRR_RT_COLORS_HPP_

#include "colors_fwd.hpp"

#include "tuples.hpp"

#include <ostream>

namespace RT
{
class Color
{
public:
    using scalar_t = Tuple::scalar_t;
    Color() noexcept;
    Color(scalar_t red, scalar_t green, scalar_t blue) noexcept;

    scalar_t red() const noexcept;
    scalar_t green() const noexcept;
    scalar_t blue() const noexcept;

    Color & operator+=(Color const & rhs) noexcept;
    Color & operator-=(Color const & rhs) noexcept;
    Color & operator*=(scalar_t rhs) noexcept;
    Color & operator*=(Color const & rhs);

    bool operator==(Color const & rhs) const noexcept;

    static Color White() noexcept;
    static Color Black() noexcept;

private:
    RT::Tuple components_;
};

Color operator+(Color lhs, Color const & rhs) noexcept;
Color operator-(Color lhs, Color const & rhs) noexcept;
Color operator*(Color lhs, Color::scalar_t rhs) noexcept;
Color operator*(Color::scalar_t lhs, Color rhs) noexcept;
Color operator*(Color lhs, Color const & rhs);

std::ostream& operator<<(std::ostream & os, Color const & value);

} // namespace RT

#endif // BRR_RT_COLORS_HPP_

