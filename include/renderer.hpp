#ifndef BRR_RT_RENDERER_HPP_
#define BRR_RT_RENDERER_HPP_

#include "colors_fwd.hpp"
#include "lights_fwd.hpp"
#include "materials_fwd.hpp"
#include "tuples_fwd.hpp"
#include "worldobjects_fwd.hpp"

namespace RT
{
Color lighting(Material const & mat, WorldObject const & object,
               PointLight const & light, Tuple const & position,
               Tuple const & eyev, Tuple const & normalv, bool inShadow);

Color lighting(WorldObject const & object, PointLight const & light,
               Tuple const & position, Tuple const & eyev,
               Tuple const & normalv, bool inShadow);
} // namespace RT

#endif // BRR_RT_RENDERER_HPP_

