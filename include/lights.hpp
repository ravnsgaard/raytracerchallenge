#ifndef BRR_RT_LIGHTS_HPP_
#define BRR_RT_LIGHTS_HPP_

#include "lights_fwd.hpp"

#include "colors.hpp"
#include "tuples.hpp"

namespace RT
{
class PointLight
{
public:
    PointLight(Tuple const & pos, Color const & intensity) noexcept;

    Tuple position() const noexcept;
    Color intensity() const noexcept;

    bool operator==(PointLight const & rhs) const noexcept;

private:
    Tuple position_;
    Color intensity_;
};
} // namespace RT

#endif // BRR_RT_LIGHTS_HPP_

