#ifndef BRR_RT_TUPLES_HPP_
#define BRR_RT_TUPLES_HPP_

#include "tuples_fwd.hpp"

#include <array>
#include <cstddef>
#include <iostream>

namespace RT
{
class Tuple
{
public:
    using scalar_t = double;

private:
    using array_t = std::array<scalar_t, 4>;

public:
    using iterator = array_t::iterator;
    using const_iterator = array_t::const_iterator;

    Tuple() noexcept = default;
    Tuple(scalar_t x, scalar_t y, scalar_t z, scalar_t w) noexcept;

    Tuple & operator+=(Tuple const & rhs);
    Tuple & operator-=(Tuple const & rhs);
    Tuple & operator*=(scalar_t rhs);
    Tuple & operator/=(scalar_t rhs);

    Tuple operator-() const noexcept;

    scalar_t operator()(std::size_t index) const noexcept;
    bool operator==(Tuple const & rhs) const noexcept;

    bool isPoint() const;
    bool isVector() const;

    scalar_t norm() const;
    void normalize();
    Tuple normalized() const;

    Tuple reflect(Tuple const & normal) const;

    scalar_t dot(Tuple const & rhs) const;
    Tuple cross(Tuple const & rhs) const;

    iterator begin();
    const_iterator begin() const;
    iterator end();
    const_iterator end() const;

    static Tuple Point(scalar_t x, scalar_t y, scalar_t z) noexcept;
    static Tuple Vector(scalar_t x, scalar_t y, scalar_t z) noexcept;

private:
    array_t values_;
};

Tuple operator+(Tuple lhs, Tuple const & rhs);
Tuple operator-(Tuple lhs, Tuple const & rhs);
Tuple operator*(Tuple lhs, Tuple::scalar_t rhs);
Tuple operator*(Tuple::scalar_t lhs, Tuple rhs);
Tuple operator/(Tuple lhs, Tuple::scalar_t rhs);

std::ostream & operator<<(std::ostream & os, Tuple const & value);

} // namespace RT

#endif // BRR_RT_TUPLES_HPP_

