#ifndef BRR_RT_CANVAS_HPP_
#define BRR_RT_CANVAS_HPP_

#include "colors.hpp"
#include "tuples.hpp"

#include <vector>

namespace RT
{
class Canvas
{
public:
    using size_type = std::vector<Color>::size_type;

    Canvas(size_type width, size_type height);

    size_type width() const noexcept;
    size_type height() const noexcept;

    /// \requires column < width_ and row < height_.
    Color at(size_type column, size_type row) const noexcept;
    /// \effects Set the pixel at (column, row) to the specified color.
    /// \requires column < width_ and row < height_.
    void set(size_type column, size_type row, Color const & color) noexcept;

    void writeAsPPM(std::ostream & out) const;

private:
    std::vector<std::vector<Color>> buffer_;
    size_type width_;
    size_type height_;
};

} // namespace RT

#endif // BRR_RT_CANVAS_HPP_

