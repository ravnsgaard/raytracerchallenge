#ifndef BRR_RT_WORLDS_HPP_
#define BRR_RT_WORLDS_HPP_

#include "lights.hpp"
#include "worldobjects.hpp"

#include <memory>

namespace RT
{
class World
{
public:
    using size_type = std::vector<WorldObject>::size_type;
    std::vector<std::unique_ptr<WorldObject>> & objects() noexcept;
    std::vector<PointLight> & lights() noexcept;

    void addLight(PointLight light);

    template<typename T, typename... Args>
    WorldObject * addObject(Args &&... args);

    void clearObjects() noexcept;
    void clearLights() noexcept;

    std::vector<Intersection> intersects(Ray const & ray) const;

    Color shadeHit(SurfaceComputations const & comps,
                   size_type remaining = 7) const noexcept;
    Color colorAt(Ray const & ray, size_type remaining = 7) const;
    bool isShadowed(PointLight const & light, Tuple const & point) const
        noexcept;
    Color reflectedColor(SurfaceComputations const & comps,
                         size_type remaining = 7) const noexcept;
    Color refractedColor(SurfaceComputations const & comps,
                         size_type remaining = 7) const noexcept;

    static World DefaultWorld() noexcept;

private:
    std::vector<std::unique_ptr<WorldObject>> objects_;
    std::vector<PointLight> lights_;
};

template<typename T, typename... Args>
WorldObject * World::addObject(Args &&... args)
{
    std::unique_ptr<WorldObject> obj(new T{std::forward<Args>(args)...});
    objects_.push_back(std::move(obj));
    return objects_.back().get();
}

} // namespace RT

#endif

