#ifndef BRR_RT_WORLDOBJECTS_HPP_
#define BRR_RT_WORLDOBJECTS_HPP_

#include "worldobjects_fwd.hpp"

#include "tuples.hpp"

#include "rays_fwd.hpp"
#include "materials.hpp"
#include "transforms.hpp"

#include <optional>
#include <vector>

namespace RT
{
class WorldObject
{
public:
    virtual ~WorldObject() = default;

    Material material() const noexcept;
    void setMaterial(Material const & mat) noexcept;

    Transform const & transform() const noexcept;
    void setTransform(Transform const & t) noexcept;

    Tuple normalAt(Tuple const & point) const noexcept;
    std::vector<Intersection> intersects(Ray ray) const;

    bool operator==(WorldObject const & rhs) const noexcept;

protected:
    WorldObject() noexcept;
    WorldObject(Material const & mat, Transform const & trns) noexcept;

private:
    virtual std::vector<Intersection>
    findIntersections(Ray const & ray) const = 0;
    virtual Tuple findNormal(Tuple const & point) const noexcept = 0;
    virtual bool compare(WorldObject const & object) const noexcept = 0;

    Material material_;
    Transform transform_;
};

class Sphere : public WorldObject
{
public:
    Sphere() noexcept;
    explicit Sphere(Transform const & trns) noexcept;
    explicit Sphere(Material const & mat) noexcept;
    Sphere(Material const & mat, Transform const & trns) noexcept;

    static Sphere GlassSphere() noexcept;

private:
    std::vector<Intersection>
    findIntersections(Ray const & ray) const override;
    Tuple findNormal(Tuple const & point) const noexcept override;
    bool compare(WorldObject const & object) const noexcept override;
};

class Plane : public WorldObject
{
public:
    Plane() noexcept;
    explicit Plane(Transform const & trns) noexcept;
    explicit Plane(Material const & mat) noexcept;
    Plane(Material const & mat, Transform const & trns) noexcept;

private:
    std::vector<Intersection>
        findIntersections(Ray const & ray) const override;
    Tuple findNormal(Tuple const & point) const noexcept override;
    bool compare(WorldObject const & object) const noexcept override;
};

class Cube : public WorldObject
{
public:
    Cube() noexcept = default;
    explicit Cube(Transform const & trns) noexcept;
    explicit Cube(Material const & mat) noexcept;
    Cube(Material const & mat, Transform const & trns) noexcept;

private:
    std::vector<Intersection>
        findIntersections(Ray const & ray) const override;
    Tuple findNormal(Tuple const & point) const noexcept override;
    bool compare(WorldObject const & object) const noexcept override;
};

class Cylinder : public WorldObject
{
public:
    Cylinder() noexcept = default;
    explicit Cylinder(Transform const & trns) noexcept;
    explicit Cylinder(Material const & mat) noexcept;
    Cylinder(Material const & mat, Transform const & trns) noexcept;
    Cylinder(Material const & mat, Transform const & trns, double minimum,
             double maximum, bool closed) noexcept;

    double minimum() const noexcept;
    double maximum() const noexcept;
    bool isClosed() const noexcept;

    void setMinimum(double minimum) noexcept;
    void setMaximum(double maximum) noexcept;
    void setClosed(bool close = true) noexcept;

private:
    std::vector<Intersection>
        findIntersections(Ray const & ray) const override;
    Tuple findNormal(Tuple const & point) const noexcept override;
    bool compare(WorldObject const & object) const noexcept override;

    bool checkCap(Ray const & ray, double t) const noexcept;
    std::vector<Intersection> intersectCaps(Ray const & ray) const;

    double minimum_{-std::numeric_limits<double>::infinity()};
    double maximum_{std::numeric_limits<double>::infinity()};
    bool closed_{false};
};

struct SurfaceComputations
{
    double time;
    WorldObject const * object;
    Tuple point;
    Tuple overPoint;
    Tuple underPoint;
    Tuple eyeVector;
    Tuple normalVector;
    Tuple reflectVector;
    double n1;
    double n2;
    bool isInside;
};

class Intersection
{
public:
    Intersection(WorldObject const * const object, double t) noexcept;
    Intersection(Intersection const & rhs) noexcept = default;

    Intersection & operator=(Intersection const & rhs) noexcept;

    bool operator==(Intersection const & rhs) const noexcept;

    SurfaceComputations
    precompute(Ray const & ray,
               std::vector<Intersection> const & xs) const noexcept;

    WorldObject const * object() const noexcept;
    double time() const noexcept;

private:
    WorldObject const * object_;
    double t_;
};

std::optional<Intersection>
hit(std::vector<Intersection> & intersections) noexcept;
Tuple position(Intersection const & intersection, Ray const & ray) noexcept;
double schlick(SurfaceComputations const & comps) noexcept;

} // namespace RT

#endif // BRR_RT_WORLDOBJECTS_HPP_

