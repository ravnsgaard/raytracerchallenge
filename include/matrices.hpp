#ifndef BRR_RT_MATRICES_HPP_
#define BRR_RT_MATRICES_HPP_

#include "tuples.hpp"

#include <array>

namespace RT
{
class Matrix2
{
    static constexpr std::size_t ORDER = 2;

public:
    using scalar_t = double;
    using size_type = std::array<scalar_t, ORDER * ORDER>::size_type;
    using iterator = std::array<scalar_t, ORDER * ORDER>::iterator;
    using const_iterator = std::array<scalar_t, ORDER * ORDER>::const_iterator;
    Matrix2() noexcept = default;
    Matrix2(scalar_t a11, scalar_t a12, scalar_t a21, scalar_t a22) noexcept;

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;

    scalar_t operator()(size_type i, size_type j) const noexcept;

    bool operator==(Matrix2 const & rhs) const;
    bool operator!=(Matrix2 const & rhs) const;

    scalar_t determinant() const;

private:
    std::array<scalar_t, ORDER * ORDER> values_;
};

std::ostream & operator<<(std::ostream & stream, Matrix2 const & matrix);

class Matrix3
{
    static constexpr std::size_t ORDER = 3;

public:
    using scalar_t = double;
    using size_type = std::array<scalar_t, ORDER * ORDER>::size_type;
    using iterator = std::array<scalar_t, ORDER * ORDER>::iterator;
    using const_iterator = std::array<scalar_t, ORDER * ORDER>::const_iterator;
    Matrix3() noexcept = default;
    Matrix3(scalar_t a11, scalar_t a12, scalar_t a13, scalar_t a21,
            scalar_t a22, scalar_t a23, scalar_t a31, scalar_t a32,
            scalar_t a33) noexcept;

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;

    scalar_t operator()(size_type i, size_type j) const noexcept;

    Matrix2 submatrix(size_type i, size_type j) const;

    scalar_t minorAt(size_type i, size_type j) const;
    scalar_t cofactorAt(size_type i, size_type j) const;
    scalar_t determinant() const;

    bool operator==(Matrix3 const & rhs) const;
    bool operator!=(Matrix3 const & rhs) const;

private:
    std::array<scalar_t, ORDER * ORDER> values_;
};

std::ostream & operator<<(std::ostream & os, Matrix3 const & matrix);

class Matrix4
{
    static constexpr std::size_t ORDER = 4;

public:
    using scalar_t = double;
    using size_type = std::array<scalar_t, ORDER * ORDER>::size_type;
    using iterator = std::array<scalar_t, ORDER * ORDER>::iterator;
    using const_iterator = std::array<scalar_t, ORDER * ORDER>::const_iterator;
    Matrix4() noexcept = default;
    Matrix4(scalar_t a11, scalar_t a12, scalar_t a13, scalar_t a14,
            scalar_t a21, scalar_t a22, scalar_t a23, scalar_t a24,
            scalar_t a31, scalar_t a32, scalar_t a33, scalar_t a34,
            scalar_t a41, scalar_t a42, scalar_t a43, scalar_t a44) noexcept;

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;

    Matrix4 & operator*=(Matrix4 const & rhs) noexcept;
    Tuple operator*(Tuple const & rhs) const noexcept;

    void transpose();

    scalar_t operator()(size_type i, size_type j) const noexcept;

    Matrix3 submatrix(size_type i, size_type j) const noexcept;

    scalar_t minorAt(size_type i, size_type j) const;
    scalar_t cofactorAt(size_type i, size_type j) const;
    scalar_t determinant() const;

    bool operator==(Matrix4 const & rhs) const;
    bool operator!=(Matrix4 const & rhs) const;

    bool isInvertible() const;

    Matrix4 transposed() const noexcept;
    Matrix4 cofactors() const;
    Matrix4 inversed() const;
    static Matrix4 Identity() noexcept;

private:
    std::array<scalar_t, ORDER * ORDER> values_;
};

Matrix4 operator*(Matrix4 lhs, Matrix4 const & rhs) noexcept;

std::ostream & operator<<(std::ostream & stream, Matrix4 const & matrix);

} // namespace RT

#endif // BRR_RT_MATRICES_HPP_

