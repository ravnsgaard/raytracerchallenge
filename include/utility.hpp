#ifndef BRR_RT_UTILITY_HPP_
#define BRR_RT_UTILITY_HPP_

#include <cmath>
#include <limits>
#include <type_traits>

namespace RT
{
template<typename T,
         typename = std::enable_if_t<std::is_floating_point<T>::value>>
bool floatCompare(T lhs, T rhs) noexcept
{
    if(std::isnan(lhs) || std::isnan(rhs))
        return false;

    if(std::isinf(lhs))
    {
        if(std::isinf(rhs))
        {
            return std::signbit(lhs) == std::signbit(rhs);
        }
        else
            return false;
    }
    if(std::isinf(rhs))
        return false;

    static constexpr int ULPs = 4;
    return std::abs(lhs - rhs) < std::numeric_limits<T>::epsilon() *
            std::max(static_cast<T>(1.), std::abs(lhs + rhs)) * ULPs ||
        std::abs(lhs - rhs) < std::numeric_limits<T>::min();
}

inline constexpr double PI() noexcept
{
    return 3.14159265358979323846;
}

} // namespace RT

#endif // BRR_RT_UTILITY_HPP_

