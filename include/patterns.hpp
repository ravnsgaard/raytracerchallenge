#ifndef BRR_RT_PATTERNS_HPP_
#define BRR_RT_PATTERNS_HPP_

#include "colors.hpp"
#include "transforms.hpp"
#include "worldobjects_fwd.hpp"

#include <memory>

namespace RT
{
class Pattern
{
public:
    virtual ~Pattern() = default;

    Color colorAt(Tuple const & point) const noexcept;
    Color colorAt(WorldObject const & object, Tuple const & point) const
        noexcept;

    Color color1() const noexcept;
    Color color2() const noexcept;

    Transform transform() const noexcept;
    void setTransform(Transform const & trns) noexcept;

    std::unique_ptr<Pattern> clone() const;

protected:
    Pattern(Color const & c1, Color const & c2,
            Transform const & trns = Transform::Identity()) noexcept;
    Pattern(Pattern const & rhs) noexcept;

private:
    virtual Color getColor1() const noexcept;
    virtual Color getColor2() const noexcept;
    virtual Color findColorAt(Tuple const & point) const noexcept = 0;
    virtual Pattern * createClone() const = 0;
    virtual Transform getTransform() const noexcept;

    Color c1_;
    Color c2_;
    Transform transform_;
};

class StripePattern : public Pattern
{
public:
    StripePattern(Color const & c1, Color const & c2) noexcept;
    StripePattern(StripePattern const & rhs) noexcept;

private:
    Color findColorAt(Tuple const & point) const noexcept override;
    StripePattern * createClone() const override;
};

class GradientPattern : public Pattern
{
public:
    GradientPattern(Color const & c1, Color const & c2) noexcept;
    GradientPattern(GradientPattern const & rhs) noexcept;

private:
    Color findColorAt(Tuple const & point) const noexcept override;
    GradientPattern * createClone() const override;
};

class RingPattern : public Pattern
{
public:
    RingPattern(Color const & c1, Color const & c2) noexcept;
    RingPattern(RingPattern const & rhs) noexcept;

private:
    Color findColorAt(Tuple const & point) const noexcept override;
    RingPattern * createClone() const override;
};

class CheckersPattern : public Pattern
{
public:
    CheckersPattern(Color const & c1, Color const & c2) noexcept;
    CheckersPattern(CheckersPattern const & rhs) noexcept = default;

private:
    Color findColorAt(Tuple const & point) const noexcept override;
    CheckersPattern * createClone() const override;
};

class UVWrapPattern : public Pattern
{
public:
    UVWrapPattern(Pattern const & pattern);
    UVWrapPattern(UVWrapPattern const & rhs);

private:
    Color findColorAt(Tuple const & point) const noexcept override;
    UVWrapPattern * createClone() const override;
    Color getColor1() const noexcept override;
    Color getColor2() const noexcept override;
    Transform getTransform() const noexcept override;

    std::unique_ptr<Pattern> pattern_;
};

} // namespace RT

#endif // BRR_RT_PATTERNS_HPP_

