#ifndef BRR_RT_MATERIALS_HPP_
#define BRR_RT_MATERIALS_HPP_

#include "materials_fwd.hpp"

#include "colors.hpp"
#include "patterns.hpp"

#include <memory>

namespace RT
{
class Material
{
public:
    using scalar_t = Color::scalar_t;
    Material() noexcept;
    explicit Material(Color const & color) noexcept;
    Material(Color const & color, scalar_t ambient, scalar_t diffuse,
             scalar_t specular, scalar_t shine, scalar_t reflect,
             scalar_t transparency, scalar_t index) noexcept;
    Material(Pattern const & pattern, scalar_t ambient, scalar_t diffuse,
             scalar_t specular, scalar_t shine, scalar_t reflect,
             scalar_t transparency, scalar_t index);

    Material(Material const & rhs);
    Material(Material && rhs) = default;

    Material & operator=(Material const & rhs);
    Material & operator=(Material && rhs) = default;

    void swap(Material & rhs) noexcept;

    bool operator==(Material const & rhs) const noexcept;

    Color color() const noexcept;
    Pattern const * pattern() const noexcept;
    scalar_t ambient() const noexcept;
    scalar_t diffuse() const noexcept;
    scalar_t specular() const noexcept;
    scalar_t shininess() const noexcept;
    scalar_t reflective() const noexcept;
    scalar_t transparency() const noexcept;
    scalar_t refractiveIndex() const noexcept;

    bool hasPattern() const noexcept;

    void setColor(Color const & color) noexcept;
    void setPattern(Pattern const & pattern);
    void setAmbient(scalar_t amb) noexcept;
    void setDiffuse(scalar_t dif) noexcept;
    void setSpecular(scalar_t spec) noexcept;
    void setShininess(scalar_t shine) noexcept;
    void setReflective(scalar_t reflect) noexcept;
    void setTransparency(scalar_t transparency) noexcept;
    void setRefractiveIndex(scalar_t index) noexcept;

private:
    Color color_;
    std::unique_ptr<Pattern> pattern_;
    scalar_t ambient_{.1};
    scalar_t diffuse_{.9};
    scalar_t specular_{.9};
    scalar_t shininess_{200.};
    scalar_t reflective_{0.};
    scalar_t transparency_{0.};
    scalar_t refractiveIndex_{1.};
};

inline std::ostream & operator<<(std::ostream & os, Material const & material)
{
    os << "Material:" << &material;
    return os;
}
} // namespace RT

#endif // BRR_RT_MATERIALS_HPP_

