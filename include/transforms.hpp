#ifndef BRR_RT_TRANSFORMS_HPP_
#define BRR_RT_TRANSFORMS_HPP_

#include "transforms_fwd.hpp"
#include "matrices.hpp"
#include "tuples.hpp"

#include <iostream>

namespace RT
{
class Transform
{
public:
    using scalar_t = double;

    bool operator==(Transform const & rhs) const;
    bool operator==(Matrix4 const & rhs) const;
    Tuple operator*(Tuple const & t) const noexcept;
    Transform operator*(Transform const & rhs) const noexcept;

    Transform inversed() const noexcept;
    Transform transposed() const noexcept;

    static Transform Scaling(scalar_t x, scalar_t y, scalar_t z) noexcept;
    static Transform Translation(scalar_t x, scalar_t y, scalar_t z) noexcept;
    static Transform RotationX(scalar_t radians) noexcept;
    static Transform RotationY(scalar_t radians) noexcept;
    static Transform RotationZ(scalar_t radians) noexcept;
    static Transform Shearing(scalar_t xy, scalar_t xz, scalar_t yx,
                              scalar_t yz, scalar_t zx, scalar_t zy) noexcept;
    static Transform Identity() noexcept;

    static Transform View(Tuple const & from, Tuple const & to,
                          Tuple const & up) noexcept;

private:
    Transform(Matrix4 matrix) noexcept;

    Matrix4 matrix_;
    friend std::ostream & operator<<(std::ostream &, Transform const &);
};

std::ostream & operator<<(std::ostream & os, Transform const & trns);

} // namespace RT

#endif // BRR_RT_TRANSFORMS_HPP_

