#include "canvas.hpp"
#include "lights.hpp"
#include "rays.hpp"
#include "renderer.hpp"
#include "worldobjects.hpp"

#include <cstdlib>
#include <iostream>

int main(int argc, char * argv[])
{
    using namespace RT;
    int const pixels = 900;
    double const canvasSize = 7.;
    double const halfCanvas = canvasSize / 2.;
    double const canvasRatio = canvasSize / pixels;
    auto c = Canvas{pixels, pixels};
    auto s = Sphere{};
    auto m = s.material();
    m.setColor(Color{1., .2, 1.});
    s.setMaterial(m);
    auto lightPosition = Tuple::Point(-10., 10., -10.);
    auto lightColor = Color{1., 1., 1.};
    auto light = PointLight{lightPosition, lightColor};
    auto origin = Tuple::Point(0., 0., -5.);

    for(int x = 0; x != pixels; ++x)
        for(int y = 0; y != pixels; ++y)
        {
            auto point = Tuple::Point(canvasRatio * x - halfCanvas,
                                      canvasRatio * y - halfCanvas, 10.);
            auto ray = Ray(origin, (point - origin).normalized());
            auto i = s.intersects(ray);
            if(!i.size())
            {
                c.set(x, pixels - y - 1, Color{0., 0., 0.});
                continue;
            }
            auto theHit = hit(i);
            auto thePosition = position(*theHit, ray);
            auto normal = theHit->object()->normalAt(thePosition);
            auto eye = -ray.direction();
            auto color = lighting(*(theHit->object()), light, thePosition, eye,
                                  normal, false);

            c.set(x, pixels - y - 1, color);
        }

    c.writeAsPPM(std::cout);
    return EXIT_SUCCESS;
}

