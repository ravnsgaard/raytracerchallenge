#include "tuples.hpp"
#include "canvas.hpp"

#include <algorithm>
#include <cstdlib>
#include <iostream>

class Environment
{
public:
    Environment(RT::Tuple grav, RT::Tuple w) : gravity_(grav), wind_(w)
    {
    }

    RT::Tuple const & gravity() const
    {
        return gravity_;
    }
    RT::Tuple const & wind() const
    {
        return wind_;
    }

private:
    RT::Tuple gravity_;
    RT::Tuple wind_;
};

class Projectile
{
public:
    Projectile(RT::Tuple pos, RT::Tuple vel) : position_(pos), velocity_(vel)
    {
    }

    void tick(Environment const & env)
    {
        position_ += velocity_;
        velocity_ += env.gravity() + env.wind();
    }

    RT::Tuple position() const
    {
        return position_;
    }
    RT::Tuple velocity() const
    {
        return velocity_;
    }

private:
    RT::Tuple position_;
    RT::Tuple velocity_;
};

void writePixel(RT::Canvas & c, Projectile const & p)
{
    auto y = static_cast<std::size_t>(c.height() - p.position()(1));
    auto x = static_cast<std::size_t>(p.position()(0));

    x = std::clamp(x, 0ul, c.width() - 1);
    y = std::clamp(y, 0ul, c.height() - 1);

    c.set(x, y, RT::Color{1.l, 1.l, 1.l});
}

int main(int argc, char * argv[])
{
    auto start = RT::Tuple::Point(0.l, 1.l, 0.l);
    auto velocity = RT::Tuple::Vector(1.l, 1.8l, 0.l).normalized() * 11.25l;
    Projectile p{start, velocity};

    auto gravity = RT::Tuple::Vector(0.l, -.1l, 0.l);
    auto wind = RT::Tuple::Vector(-.01l, 0.l, 0.l);
    Environment e{gravity, wind};

    RT::Canvas c{900, 550};
    writePixel(c, p);
    do
    {
        p.tick(e);
        writePixel(c, p);
    }
    while(p.position()(1) > 0.l);
    
    c.writeAsPPM(std::cout);

    return EXIT_SUCCESS;
}

