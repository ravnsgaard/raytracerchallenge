#include "transforms.hpp"
#include "canvas.hpp"
#include "utility.hpp"

#include <iostream>

int main(int argc, char * argv[])
{
    RT::Canvas canvas{300, 300};
    auto p = RT::Tuple::Point(0., 125., 0.);
    auto t = RT::Transform::RotationZ(RT::PI() / 6.);
    auto view = RT::Transform::Translation(150., 150., 0.);
    for(auto i = 0; i != 12; ++i)
    {
        auto pv = view * p;
        auto y = static_cast<std::size_t>(canvas.height() - pv(1));
        auto x = static_cast<std::size_t>(pv(0));
        canvas.set(x, y, RT::Color{.8, .8, .8});
        p = t * p;
    }

    canvas.writeAsPPM(std::cout);

    return EXIT_SUCCESS;
}

