#include "tuples.hpp"

#include <cstdlib>
#include <iomanip>
#include <iostream>

class Environment
{
public:
    Environment(RT::Tuple grav, RT::Tuple w) : gravity_(grav), wind_(w)
    {
    }

    RT::Tuple const & gravity() const
    {
        return gravity_;
    }
    RT::Tuple const & wind() const
    {
        return wind_;
    }

private:
    RT::Tuple gravity_;
    RT::Tuple wind_;
};

class Projectile
{
public:
    Projectile(RT::Tuple pos, RT::Tuple vel) : position_(pos), velocity_(vel)
    {
    }

    void tick(Environment const & env)
    {
        position_ += velocity_;
        velocity_ += env.gravity() + env.wind();
    }

    RT::Tuple position() const
    {
        return position_;
    }
    RT::Tuple velocity() const
    {
        return velocity_;
    }

private:
    RT::Tuple position_;
    RT::Tuple velocity_;
};

std::string to_string(RT::Tuple point)
{
    std::ostringstream ss;
    ss << std::setprecision(7) << '[' << point(0) << ',' << point(1) << ','
       << point(2) << ']';
    return ss.str();
}

int main(int argc, char * argv[])
{
    auto p = Projectile{RT::Tuple::Point(0.l, 1.l, 0.l),
                        RT::Tuple::Vector(1.l, 1.l, 0.l).normalized()};

    auto e = Environment{RT::Tuple::Vector(0.l, -.1l, 0.l),
                         RT::Tuple::Vector(-.01l, 0.l, 0.l)};

    int i = 0;
    std::cout << to_string(p.position()) << '\t' << to_string(p.velocity())
              << '\n';
    do
    {
        p.tick(e);
        std::cout << to_string(p.position()) << '\t' << to_string(p.velocity())
                  << '\n';
    } while(p.position()(1) > 0.l);
    std::cout << "Total " << i << " ticks.\n";

    return EXIT_SUCCESS;
}

