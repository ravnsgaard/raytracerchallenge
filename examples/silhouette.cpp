#include "canvas.hpp"
#include "rays.hpp"
#include "worldobjects.hpp"

#include <cstdlib>
#include <iostream>

int main(int argc, char * argv[])
{
    using namespace RT;
    int const pixels = 900;
    double const canvasSize = 7.;
    double const halfCanvas = canvasSize / 2.;
    double const canvasRatio = canvasSize / pixels;
    auto c = Canvas{pixels, pixels};
    auto s = Sphere{};
    auto origin = Tuple::Point(0., 0., -5.);

    for(int x = 0; x != pixels; ++x)
        for(int y = 0; y != pixels; ++y)
        {
            auto point = Tuple::Point(canvasRatio * x - halfCanvas,
                    canvasRatio * y - halfCanvas, 10.);
            auto ray = Ray(origin, (point - origin).normalized());
            c.set(x, y,
                    s.intersects(ray).size() ? Color{0., .5, 1.} : Color{0., 0., 0.});
        }

    c.writeAsPPM(std::cout);
    return EXIT_SUCCESS;
}

