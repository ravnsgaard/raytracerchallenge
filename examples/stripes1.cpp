#include "cameras.hpp"
#include "canvas.hpp"
#include "lights.hpp"
#include "rays.hpp"
#include "renderer.hpp"
#include "utility.hpp"
#include "worldobjects.hpp"

#include <cstdlib>
#include <iostream>

int main(int argc, char * argv[])
{
    using namespace RT;

    auto w = World{};
    w.addLight({Tuple::Point(-10., 10., -10.), Color{1., 1., 1.}});
    auto floorMaterial = Material{{1., .9, .5}, .1, .9, .0, 200., 0.};
    w.addObject<Plane>(floorMaterial);
    auto pattern = StripePattern{{1., .1, .5}, {.1, .5, 1.}};
    pattern.setTransform(RT::Transform::Scaling(.2, .2, .2) *
                         RT::Transform::RotationZ(PI() / 6.));
    w.addObject<Sphere>(Material{pattern, .1, .7, .3, 200., 0.},
                        Transform::Translation(-.5, 1., .5));
    w.addObject<Sphere>(Material{{.5, 1., .1}, .1, .7, .3, 200., 0.},
                        Transform::Translation(1.5, .5, -.5) *
                            Transform::Scaling(.5, .5, .5));
    w.addObject<Sphere>(Material{{1., .8, .1}, .1, .7, .3, 200., 0.},
                        Transform::Translation(-1.5, .33, -.75) *
                            Transform::Scaling(.33, .33, .33));

    auto c = RT::Camera(800, 450, RT::PI() / 3.);
    auto from = RT::Tuple::Point(2., 2.5, -6.);
    auto to = RT::Tuple::Point(-.5, 1., .5);
    auto up = RT::Tuple::Vector(0., 1., 0.);
    c.setTransform(Transform::View(from, to, up));

    auto image = c.render(w);

    image.writeAsPPM(std::cout);
    return EXIT_SUCCESS;
}

