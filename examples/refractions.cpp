#include "cameras.hpp"
#include "worlds.hpp"
#include "utility.hpp"

#include <cstdlib>

int main(int argc, char * argv[])
{
    using namespace RT;
    auto w = World{};
    w.addObject<Plane>(Material{CheckersPattern{Color::White(), Color::Black()},
                       .1, .9, .9, 200., .1, .0, 1.});
    w.addObject<Plane>(
        Material{CheckersPattern{Color{.1, .8, .1}, Color{.05, .05, .05}}, .1,
                 .9, .9, 200., .1, .0, 1.},
        Transform::Translation(0., 0., 10.) * Transform::RotationX(PI() / 2.));
    w.addObject<Plane>(
        Material{CheckersPattern{Color{.1, .8, .1}, Color{.05, .05, .05}}, .1,
                 .9, .9, 200., .1, .0, 1.},
        Transform::Translation(5., 0., 0.) * Transform::RotationZ(PI() / 2.));
    auto sphere = w.addObject<Sphere>(Sphere::GlassSphere());
    sphere->setTransform(Transform::Translation(0., 1., 0.));
    w.addLight({Tuple::Point(-10., 10., -10.), Color::White()});

    auto c = Camera(800, 450, PI() / 4.);
    auto from = Tuple::Point(-3., 3., -5.);
    auto to = Tuple::Point(0., 1., 0.);
    auto up = Tuple::Vector(0., 1., 0.);
    c.setTransform(Transform::View(from, to, up));

    auto image = c.render(w);

    image.writeAsPPM(std::cout);

    return EXIT_SUCCESS;
}

