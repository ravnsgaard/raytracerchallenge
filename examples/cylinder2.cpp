#include "cameras.hpp"
#include "worlds.hpp"
#include "utility.hpp"

#include <cstdlib>

int main(int argc, char * argv[])
{
    using namespace RT;
    auto w = World{};
    w.addObject<Cube>(Material{{.4, .6, .8}, .1, .9, .9, 200., .0, .0, 1.},
                      Transform::Translation(0., 2.5, 0.) *
                          Transform::Scaling(10., 5., 10.));
    w.addObject<Plane>(
        Material{CheckersPattern{Color::White(), Color::Black()}, .1, .9, .3,
                 50, .1, 0., 1.},
        Transform::Translation(0., .01, 0.));

    w.addObject<Cylinder>(
        Material{{1., 1., 1.}, .25, .1, .478, 76.8, .9, .0, 1.},
        Transform::Scaling(.3, .5, .3), 0., 1.);

    w.addLight({Tuple::Point(-6., 4., -6), {1., 1., 1.}});
    auto c = Camera(800, 450, PI() / 5.);
    auto from = Tuple::Point(-1., 3., -5.);
    auto to = Tuple::Point(0., .5, 0.);
    auto up = Tuple::Vector(0., 1., 0.);
    c.setTransform(Transform::View(from, to, up));

    auto image = c.render(w);

    image.writeAsPPM(std::cout);

    return EXIT_SUCCESS;
}

