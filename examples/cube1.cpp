#include "cameras.hpp"
#include "worlds.hpp"
#include "utility.hpp"

#include <cstdlib>

int main(int argc, char * argv[])
{
    using namespace RT;
    auto w = World{};
    w.addObject<Cube>(Material{{.4, .6, .8}, .1, .9, .9, 200., .0, .0, 1.},
                      Transform::Translation(0., 2.5, 0.) *
                          Transform::Scaling(10., 5., 10.));
    w.addObject<Plane>(
        Material{CheckersPattern{Color::White(), Color::Black()}, .1, .9, .3,
                 50, .1, 0., 1.},
        Transform::Translation(0., .01, 0.));
    auto tableMat = Material{{.36, .22, .15}, .1, .9, .1, 5., .0, .0, 1.};
    w.addObject<Cube>(tableMat,
                      Transform::Translation(-.4, .375, .4) *
                          Transform::Scaling(.02, .375, .02));
    w.addObject<Cube>(tableMat,
                      Transform::Translation(.4, .375, .4) *
                          Transform::Scaling(.02, .375, .02));
    w.addObject<Cube>(tableMat,
                      Transform::Translation(.4, .375, -.4) *
                          Transform::Scaling(.02, .375, .02));
    w.addObject<Cube>(tableMat,
                      Transform::Translation(-.4, .375, -.4) *
                          Transform::Scaling(.02, .375, .02));
    w.addObject<Cube>(tableMat,
                      Transform::Translation(0., .76, 0.) *
                          Transform::Scaling(.5, .02, .5));
    auto sphere = w.addObject<Sphere>(Sphere::GlassSphere());
    sphere->setTransform(Transform::Translation(0., .87, 0.) *
                         Transform::Scaling(.1, .1, .1));
    w.addLight({Tuple::Point(-6., 4., -6), {1., 1., 1.}});
    auto c = Camera(800, 450, PI() / 5.);
    auto from = Tuple::Point(-1., 2., -5.);
    auto to = Tuple::Point(0., 1., 0.);
    auto up = Tuple::Vector(0., 1., 0.);
    c.setTransform(Transform::View(from, to, up));

    auto image = c.render(w);

    image.writeAsPPM(std::cout);

    return EXIT_SUCCESS;
}

