#include "cameras.hpp"
#include "canvas.hpp"
#include "lights.hpp"
#include "rays.hpp"
#include "renderer.hpp"
#include "worldobjects.hpp"
#include "utility.hpp"

#include <cstdlib>
#include <iostream>

int main(int argc, char * argv[])
{
    using namespace RT;

    auto w = World::DefaultWorld();
    w.clearObjects();
    w.addObject<Sphere>(Material{{1., .2, 1.}, .1, .9, .9, 200., 0., 0., 1.});

    auto c = RT::Camera(1280, 768, RT::PI() / 4.);
    auto from = RT::Tuple::Point(0., 0., -5.);
    auto to = RT::Tuple::Point(0., 0., 0.);
    auto up = RT::Tuple::Vector(0., 1., 0.);
    c.setTransform(Transform::View(from, to, up));

    auto image = c.render(w);

    image.writeAsPPM(std::cout);
    return EXIT_SUCCESS;
}

