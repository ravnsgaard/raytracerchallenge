#include "cameras.hpp"

#include <cmath>

namespace RT
{
Camera::Camera(size_type hsize, size_type vsize, scalar_t fov) noexcept :
    hsize_{hsize},
    vsize_{vsize},
    fov_{fov},
    transform_{Transform::Identity()}
{
    init();
}

Camera::size_type Camera::hsize() const noexcept
{
    return hsize_;
}
Camera::size_type Camera::vsize() const noexcept
{
    return vsize_;
}
void Camera::init() noexcept
{
    auto halfView = std::tan(fov_ / 2.);
    scalar_t ratio = static_cast<scalar_t>(hsize_) / vsize_;
    if(ratio >= 1.)
    {
        halfWidth_ = halfView;
        halfHeight_ = halfView / ratio;
    }
    else
    {
        halfWidth_ = halfView * ratio;
        halfHeight_ = halfView;
    }
    pixelSize_ = halfWidth_ * 2. / static_cast<scalar_t>(hsize_);
}

Camera::scalar_t Camera::pixelSize() const noexcept
{
    return pixelSize_;
}

Camera::scalar_t Camera::FOV() const noexcept
{
    return fov_;
}
Transform Camera::transform() const noexcept
{
    return transform_;
}

void Camera::setTransform(Transform const & t) noexcept
{
    transform_ = t;
}

Ray Camera::rayForPixel(size_type x, size_type y) const
{
    scalar_t xoffset = (static_cast<scalar_t>(x) + .5) * pixelSize_;
    scalar_t yoffset = (static_cast<scalar_t>(y) + .5) * pixelSize_;

    scalar_t worldx = halfWidth_ - xoffset;
    scalar_t worldy = halfHeight_ - yoffset;

    auto inv = transform_.inversed();
    auto pixel = inv * RT::Tuple::Point(worldx, worldy, -1.);
    auto origin = inv * RT::Tuple::Point(0., 0., 0.);
    auto direction = (pixel - origin).normalized();
    return {origin, direction};
}

Canvas Camera::render(World const & world) const
{
    RT::Canvas result{hsize_, vsize_};
    for(size_type y = 0; y != vsize_; ++y)
        for(size_type x = 0; x != hsize_; ++x)
        {
            auto ray = rayForPixel(x, y);
            auto color = world.colorAt(ray);
            result.set(x, y, color);
        }
    return result;
}

} // namespace RT

