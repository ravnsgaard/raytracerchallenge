#include "lights.hpp"

namespace RT
{
PointLight::PointLight(Tuple const & pos, Color const & intensity) noexcept :
    position_{pos},
    intensity_{intensity}
{
}

Tuple PointLight::position() const noexcept
{
    return position_;
}

Color PointLight::intensity() const noexcept
{
    return intensity_;
}

bool PointLight::operator==(PointLight const & rhs) const noexcept
{
    return position_ == rhs.position_ && intensity_ == rhs.intensity_;
}

} // namespace RT

