#include "renderer.hpp"

#include "colors.hpp"
#include "lights.hpp"
#include "materials.hpp"
#include "worldobjects.hpp"

#include <cassert>
#include <cmath>

namespace RT
{
Color lighting(WorldObject const & object, PointLight const & light,
               Tuple const & position, Tuple const & eyev,
               Tuple const & normalv, bool inShadow)
{
    return lighting(object.material(), object, light, position, eyev, normalv,
                    inShadow);
}

Color lighting(Material const & mat, WorldObject const & object,
               PointLight const & light, Tuple const & position,
               Tuple const & eyev, Tuple const & normalv, bool inShadow)
{
    assert(position.isPoint());
    assert(eyev.isVector());
    assert(normalv.isVector());
    auto actualColor = mat.hasPattern() ?
        mat.pattern()->colorAt(object, position) :
        mat.color();
    auto effectiveColor = actualColor * light.intensity();
    auto ambient = effectiveColor * mat.ambient();
    if(inShadow)
        return ambient;

    auto lightv = (light.position() - position).normalized();

    auto lightDotNormal = lightv.dot(normalv);
    if(lightDotNormal < 0.)
        return ambient;

    RT::Color diffuse = effectiveColor * mat.diffuse() * lightDotNormal;

    auto reflectv = (-lightv).reflect(normalv);
    auto reflectDotEye = reflectv.dot(eyev);

    if(reflectDotEye <= 0.)
        return ambient + diffuse;

    auto factor = std::pow(reflectDotEye, mat.shininess());
    Color specular = light.intensity() * mat.specular() * factor;
    return ambient + diffuse + specular;
}
} // namespace RT

