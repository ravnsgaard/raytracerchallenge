#include "materials.hpp"
#include "utility.hpp"

#include <cassert>
#include <utility>

namespace RT
{
Material::Material() noexcept : Material{{1., 1., 1.}}
{
}

Material::Material(Color const & color) noexcept : color_{color}
{
}

Material::Material(Color const & color, scalar_t ambient, scalar_t diffuse,
                   scalar_t specular, scalar_t shine, scalar_t reflect,
                   scalar_t transparency, scalar_t index) noexcept :
    color_{color},
    ambient_{ambient},
    diffuse_{diffuse},
    specular_{specular},
    shininess_{shine},
    reflective_{reflect},
    transparency_{transparency},
    refractiveIndex_{index}
{
}
Material::Material(Pattern const & pattern, scalar_t ambient, scalar_t diffuse,
                   scalar_t specular, scalar_t shine, scalar_t reflect,
                   scalar_t transparency, scalar_t index) :
    pattern_{pattern.clone()},
    ambient_{ambient},
    diffuse_{diffuse},
    specular_{specular},
    shininess_{shine},
    reflective_{reflect},
    transparency_{transparency},
    refractiveIndex_{index}
{
}

Material::Material(Material const & rhs) :
    color_{rhs.color_},
    pattern_{rhs.pattern_ ? rhs.pattern_->clone() : nullptr},
    ambient_{rhs.ambient_},
    diffuse_{rhs.diffuse_},
    specular_{rhs.specular_},
    shininess_{rhs.shininess_},
    reflective_{rhs.reflective_},
    transparency_{rhs.transparency_},
    refractiveIndex_{rhs.refractiveIndex_}
{
}

Material & Material::operator=(Material const & rhs)
{
    Material result{rhs};
    swap(result);
    return *this;
}

void Material::swap(Material & rhs) noexcept
{
    std::swap(color_, rhs.color_);
    std::swap(pattern_, rhs.pattern_);
    std::swap(ambient_, rhs.ambient_);
    std::swap(diffuse_, rhs.diffuse_);
    std::swap(specular_, rhs.specular_);
    std::swap(shininess_, rhs.shininess_);
    std::swap(reflective_, rhs.reflective_);
    std::swap(transparency_, rhs.transparency_);
    std::swap(refractiveIndex_, rhs.refractiveIndex_);
}

bool Material::operator==(Material const & rhs) const noexcept
{
    return pattern_ == rhs.pattern_ && color_ == rhs.color_ &&
        floatCompare(ambient_, rhs.ambient_) &&
        floatCompare(diffuse_, rhs.diffuse_) &&
        floatCompare(specular_, rhs.specular_) &&
        floatCompare(shininess_, rhs.shininess_) &&
        floatCompare(reflective_, rhs.reflective_) &&
        floatCompare(transparency_, rhs.transparency_) &&
        floatCompare(refractiveIndex_, rhs.refractiveIndex_);
}

Color Material::color() const noexcept
{
    return color_;
}

Pattern const * Material::pattern() const noexcept
{
    assert(hasPattern());
    return pattern_.get();
}

Material::scalar_t Material::ambient() const noexcept
{
    return ambient_;
}

Material::scalar_t Material::diffuse() const noexcept
{
    return diffuse_;
}

Material::scalar_t Material::specular() const noexcept
{
    return specular_;
}

Material::scalar_t Material::shininess() const noexcept
{
    return shininess_;
}

Material::scalar_t Material::reflective() const noexcept
{
    return reflective_;
}

Material::scalar_t Material::transparency() const noexcept
{
    return transparency_;
}

Material::scalar_t Material::refractiveIndex() const noexcept
{
    return refractiveIndex_;
}

bool Material::hasPattern() const noexcept
{
    return pattern_.get() != nullptr;
}

void Material::setColor(Color const & color) noexcept
{
    color_ = color;
}

void Material::setAmbient(scalar_t amb) noexcept
{
    ambient_ = amb;
}

void Material::setDiffuse(scalar_t dif) noexcept
{
    diffuse_ = dif;
}

void Material::setSpecular(scalar_t spec) noexcept
{
    specular_ = spec;
}

void Material::setShininess(scalar_t shine) noexcept
{
    shininess_ = shine;
}

void Material::setReflective(scalar_t reflect) noexcept
{
    reflective_ = reflect;
}

void Material::setTransparency(scalar_t transparency) noexcept
{
    transparency_ = transparency;
}

void Material::setRefractiveIndex(scalar_t index) noexcept
{
    refractiveIndex_ = index;
}

} // namespace RT

