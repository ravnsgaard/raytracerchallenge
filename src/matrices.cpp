#include "matrices.hpp"
#include "utility.hpp"

#include <algorithm>
#include <cassert>
#include <iomanip>
#include <numeric>
#include <ostream>

namespace RT
{
Matrix2::Matrix2(scalar_t a11, scalar_t a12, scalar_t a21,
                 scalar_t a22) noexcept :
    values_{a11, a12, a21, a22}
{
}

Matrix2::scalar_t Matrix2::operator()(size_type i, size_type j) const noexcept
{
    assert(i < ORDER && j < ORDER);
    return values_[ORDER * i + j];
}

Matrix2::iterator Matrix2::begin()
{
    return values_.begin();
}
Matrix2::iterator Matrix2::end()
{
    return values_.end();
}
Matrix2::const_iterator Matrix2::begin() const
{
    return values_.begin();
}
Matrix2::const_iterator Matrix2::end() const
{
    return values_.end();
}

bool Matrix2::operator==(Matrix2 const & rhs) const
{
    return std::equal(
        std::cbegin(values_), std::cend(values_),
        std::cbegin(rhs.values_), [](auto const & l, auto const & r) noexcept {
            return floatCompare(l, r);
        });
}

bool Matrix2::operator!=(Matrix2 const & rhs) const
{
    return !operator==(rhs);
}

Matrix2::scalar_t Matrix2::determinant() const
{
    return values_[0] * values_[3] - values_[1] * values_[2];
}

std::ostream & operator<<(std::ostream & stream, Matrix2 const & matrix)
{
    stream << "<";
    auto iter = std::begin(matrix);
    if(iter != std::end(matrix))
        stream << *iter++;
    for(; iter != std::end(matrix); ++iter)
        stream << "," << *iter;
    stream << ">";
    return stream;
}

Matrix3::Matrix3(scalar_t a11, scalar_t a12, scalar_t a13, scalar_t a21,
                 scalar_t a22, scalar_t a23, scalar_t a31, scalar_t a32,
                 scalar_t a33) noexcept :
    values_{a11, a12, a13, a21, a22, a23, a31, a32, a33}
{
}

Matrix3::iterator Matrix3::begin()
{
    return values_.begin();
}
Matrix3::iterator Matrix3::end()
{
    return values_.end();
}
Matrix3::const_iterator Matrix3::begin() const
{
    return values_.begin();
}
Matrix3::const_iterator Matrix3::end() const
{
    return values_.end();
}

Matrix3::scalar_t Matrix3::operator()(size_type i, size_type j) const noexcept
{
    assert(i < ORDER && j < ORDER);
    return values_[ORDER * i + j];
}

Matrix2 Matrix3::submatrix(size_type i, size_type j) const
{
    assert(i < ORDER && j < ORDER);
    Matrix2 result;
    auto rIter = result.begin();
    for(size_type r = 0; r != ORDER; ++r)
    {
        if(r == i)
            continue;
        for(size_type c = 0; c != ORDER; ++c)
        {
            if(c == j)
                continue;
            *rIter++ = values_[r * ORDER + c];
        }
    }
    return result;
}

Matrix3::scalar_t Matrix3::minorAt(size_type i, size_type j) const
{
    return submatrix(i, j).determinant();
}

Matrix3::scalar_t Matrix3::cofactorAt(size_type i, size_type j) const
{
    auto result = minorAt(i, j);
    return (i + j) % 2 ? -result : result;
}

Matrix3::scalar_t Matrix3::determinant() const
{
    return values_[0] * cofactorAt(0, 0) + values_[1] * cofactorAt(0, 1) +
        values_[2] * cofactorAt(0, 2);
}

bool Matrix3::operator==(Matrix3 const & rhs) const
{
    return std::equal(
        std::cbegin(values_), std::cend(values_), std::cbegin(rhs.values_),
        [](auto const & l, auto const & r) { return floatCompare(l, r); });
}

bool Matrix3::operator!=(Matrix3 const & rhs) const
{
    return !operator==(rhs);
}

std::ostream & operator<<(std::ostream & stream, Matrix3 const & matrix)
{
    stream << "<";
    auto iter = std::begin(matrix);
    if(iter != std::end(matrix))
        stream << *iter++;
    for(; iter != std::end(matrix); ++iter)
        stream << "," << *iter;
    stream << ">";
    return stream;
}

Matrix4::Matrix4(scalar_t a11, scalar_t a12, scalar_t a13, scalar_t a14,
                 scalar_t a21, scalar_t a22, scalar_t a23, scalar_t a24,
                 scalar_t a31, scalar_t a32, scalar_t a33, scalar_t a34,
                 scalar_t a41, scalar_t a42, scalar_t a43,
                 scalar_t a44) noexcept :
    values_{a11, a12, a13, a14, a21, a22, a23, a24,
            a31, a32, a33, a34, a41, a42, a43, a44}
{
}

Matrix4 & Matrix4::operator*=(Matrix4 const & rhs) noexcept
{
    std::array<scalar_t, ORDER * ORDER> transposed{
        rhs.values_[0], rhs.values_[4], rhs.values_[8],  rhs.values_[12],
        rhs.values_[1], rhs.values_[5], rhs.values_[9],  rhs.values_[13],
        rhs.values_[2], rhs.values_[6], rhs.values_[10], rhs.values_[14],
        rhs.values_[3], rhs.values_[7], rhs.values_[11], rhs.values_[15]};
    std::array<scalar_t, ORDER * ORDER> result;

    for(size_type c = 0; c != result.size(); ++c)
    {
        size_type const i = c / ORDER;
        size_type const j = c % ORDER;
        result[c] = std::inner_product(&values_[i * ORDER],
                                       &values_[i * ORDER] + ORDER,
                                       &transposed[j * ORDER], 0.0);
    }
    values_ = result;
    return *this;
}

Tuple Matrix4::operator*(Tuple const & rhs) const noexcept
{
    Tuple result;
    auto rIter = result.begin();
    for(size_type c = 0; c != ORDER; ++c, ++rIter)
    {
        *rIter = std::inner_product(std::cbegin(rhs), std::cend(rhs),
                                    &values_[c * ORDER], 0.0);
    }
    return result;
}

Matrix4::scalar_t Matrix4::operator()(size_type i, size_type j) const noexcept
{
    assert(i < ORDER && j < ORDER);
    return values_[ORDER * i + j];
}

Matrix3 Matrix4::submatrix(size_type i, size_type j) const noexcept
{
    assert(i < ORDER && j < ORDER);
    Matrix3 result;
    auto rIter = result.begin();
    for(size_type r = 0; r != ORDER; ++r)
    {
        if(r == i)
            continue;
        for(size_type c = 0; c != ORDER; ++c)
        {
            if(c == j)
                continue;
            *rIter++ = values_[r * ORDER + c];
        }
    }
    return result;
}

Matrix4::scalar_t Matrix4::minorAt(size_type i, size_type j) const
{
    return submatrix(i, j).determinant();
}

Matrix4::scalar_t Matrix4::cofactorAt(size_type i, size_type j) const
{
    auto result = minorAt(i, j);
    return (i + j) % 2 ? -result : result;
}

Matrix4::scalar_t Matrix4::determinant() const
{
    return values_[0] * cofactorAt(0, 0) + values_[1] * cofactorAt(0, 1) +
        values_[2] * cofactorAt(0, 2) + values_[3] * cofactorAt(0, 3);
}
bool Matrix4::operator==(Matrix4 const & rhs) const
{
    return std::equal(
        std::cbegin(values_), std::cend(values_), std::cbegin(rhs.values_),
        [](auto const & l, auto const & r) { return floatCompare(l, r); });
}

bool Matrix4::operator!=(Matrix4 const & rhs) const
{
    return !operator==(rhs);
}

Matrix4 operator*(Matrix4 lhs, Matrix4 const & rhs) noexcept
{
    return lhs *= rhs;
}

void Matrix4::transpose()
{
    auto temp = transposed();
    std::swap(values_, temp.values_);
}

bool Matrix4::isInvertible() const
{
    return determinant();
}

Matrix4 Matrix4::transposed() const noexcept
{
    return {values_[0], values_[4], values_[8],  values_[12],
            values_[1], values_[5], values_[9],  values_[13],
            values_[2], values_[6], values_[10], values_[14],
            values_[3], values_[7], values_[11], values_[15]};
}

Matrix4 Matrix4::cofactors() const
{
    Matrix4 result;
    for(size_type i = 0; i != ORDER; ++i)
        for(size_type j = 0; j != ORDER; ++j)
        {
            result.values_[i * ORDER + j] = cofactorAt(i, j);
        }

    return result;
}

Matrix4 Matrix4::inversed() const
{
    auto result = cofactors();
    result.transpose();
    auto const det = determinant();
    std::transform(std::begin(result.values_), std::end(result.values_),
                   std::begin(result.values_),
                   [&det](auto cf) { return cf / det; });
    return result;
}

Matrix4 Matrix4::Identity() noexcept
{
    return {1.l, 0.l, 0.l, 0.l, 0.l, 1.l, 0.l, 0.l,
            0.l, 0.l, 1.l, 0.l, 0.l, 0.l, 0.l, 1.l};
}

std::ostream & operator<<(std::ostream & stream, Matrix4 const & matrix)
{
    stream << "<";
    auto iter = std::begin(matrix);
    if(iter != std::end(matrix))
        stream << std::setprecision(10) << *iter++;
    for(; iter != std::end(matrix); ++iter)
        stream << "," << std::setprecision(10) << *iter;
    stream << ">";
    return stream;
}

Matrix4::iterator Matrix4::begin()
{
    return values_.begin();
}
Matrix4::iterator Matrix4::end()
{
    return values_.end();
}
Matrix4::const_iterator Matrix4::begin() const
{
    return values_.begin();
}
Matrix4::const_iterator Matrix4::end() const
{
    return values_.end();
}

} // namespace RT
