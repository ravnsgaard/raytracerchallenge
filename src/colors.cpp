#include "colors.hpp"

#include <algorithm>

namespace RT
{
Color::Color() noexcept : components_{0.l, 0.l, 0.l, 0.l}
{
}
Color::Color(scalar_t red, scalar_t green, scalar_t blue) noexcept :
    components_{red, green, blue, 0.l}
{
}

Color::scalar_t Color::red() const noexcept
{
    return components_(0);
}

Color::scalar_t Color::green() const noexcept
{
    return components_(1);
}

Color::scalar_t Color::blue() const noexcept
{
    return components_(2);
}

Color & Color::operator+=(Color const & rhs) noexcept
{
    components_ += rhs.components_;
    return *this;
}

Color & Color::operator-=(Color const & rhs) noexcept
{
    components_ -= rhs.components_;
    return *this;
}

Color & Color::operator*=(scalar_t rhs) noexcept
{
    components_ *= rhs;
    return *this;
}

Color & Color::operator*=(Color const & rhs)
{
    std::transform(std::begin(components_), std::end(components_),
                   std::cbegin(rhs.components_), std::begin(components_),
                   std::multiplies<>{});
    return *this;
}

bool Color::operator==(Color const & rhs) const noexcept
{
    return components_ == rhs.components_;
}

Color operator+(Color lhs, Color const & rhs) noexcept
{
    return lhs += rhs;
}

Color operator-(Color lhs, Color const & rhs) noexcept
{
    return lhs -= rhs;
}

Color operator*(Color lhs, Color::scalar_t rhs) noexcept
{
    return lhs *= rhs;
}

Color operator*(Color::scalar_t lhs, Color rhs) noexcept
{
    return rhs *= lhs;
}

Color operator*(Color lhs, Color const & rhs)
{
    return lhs *= rhs;
}

Color Color::White() noexcept
{
    return {1., 1., 1.};
}

Color Color::Black() noexcept
{
    return {0., 0., 0.};
}

std::ostream& operator<<(std::ostream & os, Color const & value)
{
    os << '(' << value.red() << ", " << value.green() << ", " << value.blue()
       << ')';
    return os;
}
} // namespace RT

