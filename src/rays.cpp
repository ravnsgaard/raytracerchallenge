#include "rays.hpp"
#include "utility.hpp"

#include "transforms.hpp"

#include <cassert>

namespace RT
{
Ray::Ray(Tuple const & origin, Tuple const & direction) noexcept :
    origin_{origin},
    direction_{direction}
{
    assert(origin.isPoint());
    assert(direction.isVector());
}

Tuple Ray::origin() const noexcept
{
    return origin_;
}
Tuple Ray::direction() const noexcept
{
    return direction_;
}

Ray & Ray::operator*=(Transform const & t) noexcept
{
    origin_ = t * origin_;
    direction_ = t * direction_;
    return *this;
}

Ray Ray::operator*(Transform const & t) const noexcept
{
    Ray result{*this};
    return result *= t;
}

} // namespace RT

