#include "worlds.hpp"

#include "rays.hpp"
#include "renderer.hpp"
#include "utility.hpp"

#include <algorithm>
#include <numeric>

namespace RT
{
std::vector<std::unique_ptr<WorldObject>> & World::objects() noexcept
{
    return objects_;
}
void World::clearObjects() noexcept
{
    objects_.clear();
}

std::vector<PointLight> & World::lights() noexcept
{
    return lights_;
}

void World::addLight(PointLight light)
{
    lights_.push_back(std::move(light));
}

void World::clearLights() noexcept
{
    lights_.clear();
}

std::vector<Intersection> World::intersects(Ray const & ray) const
{
    std::vector<Intersection> result;
    std::for_each(objects_.begin(), objects_.end(),
                  [&ray, &result](auto const & object) {
                      auto xs = object->intersects(ray);
                      result.insert(result.end(), xs.begin(), xs.end());
                  });
    std::sort(result.begin(), result.end(),
              [](auto const & l, auto const & r) {
                  return l.time() < r.time();
              });
    return result;
}

Color World::shadeHit(SurfaceComputations const & comps,
                      size_type remaining) const noexcept
{
    auto color = std::accumulate(
        lights_.begin(), lights_.end(), Color{0., 0., 0.},
        [&comps, this](auto const & color, auto const & light) {
            return color +
                lighting(comps.object->material(), *(comps.object), light,
                         comps.point, comps.eyeVector, comps.normalVector,
                         isShadowed(light, comps.overPoint));
        });
    auto reflected = reflectedColor(comps, remaining);
    auto refracted = refractedColor(comps, remaining);
    // Color refracted = Color::Black();
    auto material = comps.object->material();
    if(material.reflective() > 0. && material.transparency() > 0)
    {
        auto reflectance = schlick(comps);
        return color + reflected * reflectance + refracted * (1. - reflectance);
    }

    return color + reflected + refracted;
}

Color World::colorAt(Ray const & ray, size_type remaining) const
{
    auto xs = intersects(ray);
    auto h = hit(xs);
    if(!h)
        return {0., 0., 0.};

    auto comps = hit(xs)->precompute(ray, xs);
    return shadeHit(comps, remaining);
}
bool World::isShadowed(PointLight const & light, Tuple const & point) const noexcept
{
    auto lightVector = light.position() - point;
    auto distance = lightVector.norm();
    lightVector.normalize();
    auto ray = Ray{point, lightVector};
    auto xs = intersects(ray);
    auto theHit = hit(xs);
    return theHit.has_value() && theHit->time() < distance;
}

Color World::reflectedColor(SurfaceComputations const & comps,
                            size_type remaining) const noexcept
{
    if(remaining < 1 || comps.object->material().reflective() == 0)
        return Color::Black();

    auto reflectedRay = Ray{comps.overPoint, comps.reflectVector};
    auto color = colorAt(reflectedRay, remaining - 1);

    return color * comps.object->material().reflective();
}

Color World::refractedColor(SurfaceComputations const & comps,
                            size_type remaining) const noexcept
{
    if(remaining == 0)
        return Color::Black();
    if(floatCompare(comps.object->material().transparency(), 0.))
        return Color::Black();

    auto n_ratio = comps.n1 / comps.n2;
    auto cos_i = comps.eyeVector.dot(comps.normalVector);
    auto sin2_t = n_ratio * n_ratio * (1 - cos_i * cos_i);
    if(sin2_t > 1.)
        return Color::Black();

    auto cos_t = std::sqrt(1. - sin2_t);

    auto direction = comps.normalVector * (n_ratio * cos_i - cos_t) -
        comps.eyeVector * n_ratio;

    auto refractedRay = Ray{comps.underPoint, direction};
    auto color = colorAt(refractedRay, remaining - 1) *
        comps.object->material().transparency();

    return color;
}

World World::DefaultWorld() noexcept
{
    World result;
    result.addObject<Sphere>(Material{{.8, 1., .6}, .1, .7, .2, 200., 0., 0., 1.});
    result.addObject<Sphere>(Transform::Scaling(.5, .5, .5));
    result.addLight(
        PointLight{Tuple::Point(-10., 10., -10.), Color{1., 1., 1.}});
    return result;
}

} // namespace RT

