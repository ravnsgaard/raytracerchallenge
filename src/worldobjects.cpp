#include "worldobjects.hpp"

#include "rays.hpp"
#include "utility.hpp"

#include <algorithm>
#include <cassert>
#include <cmath>

namespace
{
void setWtoZero(RT::Tuple & vector) noexcept
{
    auto i = vector.begin();
    i += 3;
    *i = 0.;
}
void setWtoOne(RT::Tuple & point) noexcept
{
    auto i = point.begin();
    i += 3;
    *i = 1.;
}
std::tuple<double, double> checkAxis(double origin, double direction)
{
    auto tminNumerator = -1. - origin;
    auto tmaxNumerator = 1 - origin;

    double tmin, tmax;

    if(std::abs(direction) >= std::numeric_limits<double>::epsilon())
    {
        tmin = tminNumerator / direction;
        tmax = tmaxNumerator / direction;
    }
    else
    {
        tmin = tminNumerator * std::numeric_limits<double>::infinity();
        tmax = tmaxNumerator * std::numeric_limits<double>::infinity();
    }

    return std::minmax(tmin, tmax);
}
} // namespace

namespace RT
{
WorldObject::WorldObject() noexcept :
    WorldObject{Material{}, Transform::Identity()}
{
}

WorldObject::WorldObject(Material const & mat,
                         Transform const & trns) noexcept :
    material_{mat},
    transform_{trns}
{
}

Transform const & WorldObject::transform() const noexcept
{
    return transform_;
}
void WorldObject::setTransform(Transform const & t) noexcept
{
    transform_ = t;
}

Tuple WorldObject::normalAt(Tuple const & point) const noexcept
{
    auto normal = findNormal(transform_.inversed() * point);
    assert(normal.isVector());
    normal = transform_.inversed().transposed() * normal;
    setWtoZero(normal);
    normal.normalize();
    return normal;
}

std::vector<Intersection> WorldObject::intersects(Ray ray) const
{
    ray *= transform_.inversed();
    return this->findIntersections(std::move(ray));
}

bool WorldObject::operator==(WorldObject const & rhs) const noexcept
{
    return compare(rhs) && transform_ == rhs.transform_ &&
        material_ == rhs.material_;
}

Intersection::Intersection(WorldObject const * object, double time) noexcept :
    object_{object},
    t_{time}
{
}

Tuple position(Intersection const & intersection, Ray const & ray) noexcept
{
    return ray.origin() + ray.direction() * intersection.time();
}

double schlick(SurfaceComputations const & comps) noexcept
{
    auto cos = comps.eyeVector.dot(comps.normalVector);

    if(comps.n1 > comps.n2)
    {
        auto n = comps.n1 / comps.n2;
        auto sin2_t = n * n * (1. - cos * cos);
        if(sin2_t > 1.)
            return 1.;

        cos = std::sqrt(1. - sin2_t);
    }
    auto r0 = std::pow((comps.n1 - comps.n2) / (comps.n1 + comps.n2), 2.);

    return r0 + (1. - r0) * std::pow(1. - cos, 5);
}

WorldObject const * Intersection::object() const noexcept
{
    return object_;
}

double Intersection::time() const noexcept
{
    return t_;
}

Sphere::Sphere() noexcept
{
}

Sphere::Sphere(Transform const & trns) noexcept : WorldObject{Material{}, trns}
{
}

Sphere::Sphere(Material const & mat) noexcept :
    WorldObject{mat, Transform::Identity()}
{
}

Sphere::Sphere(Material const & mat, Transform const & trns) noexcept :
    WorldObject{mat, trns}
{
}

Intersection & Intersection::operator=(Intersection const & rhs) noexcept
{
    object_ = rhs.object_;
    t_ = rhs.t_;
    return *this;
}

Material WorldObject::material() const noexcept
{
    return material_;
}

void WorldObject::setMaterial(Material const & mat) noexcept
{
    material_ = mat;
}

Tuple Sphere::findNormal(Tuple const & point) const noexcept
{
    return point - Tuple::Point(0., 0., 0.);
}

std::vector<Intersection> Sphere::findIntersections(Ray const & ray) const
{
    assert(ray.direction().norm());
    auto sphere_to_ray = ray.origin() - Tuple::Point(0., 0., 0.);
    auto a = ray.direction().dot(ray.direction());
    auto b = 2. * ray.direction().dot(sphere_to_ray);
    auto c = sphere_to_ray.dot(sphere_to_ray) - 1.;
    auto discriminant = std::pow(b, 2.) - 4. * a * c;

    std::vector<Intersection> result;
    if(discriminant < 0.)
        return result;

    result.push_back(
        Intersection(this, (-b - std::sqrt(discriminant)) / (2. * a)));
    result.push_back(
        Intersection(this, (-b + std::sqrt(discriminant)) / (2. * a)));

    return result;
}

bool Intersection::operator==(Intersection const & rhs) const noexcept
{
    return object_ == rhs.object_ && floatCompare(t_, rhs.t_);
}

std::optional<Intersection>
hit(std::vector<Intersection> & intersections) noexcept
{
    auto pivot =
        std::partition(intersections.begin(), intersections.end(),
                       [](auto const & val) { return val.time() < 0.0; });
    if(pivot == intersections.end())
        return {};
    std::nth_element(intersections.begin(), pivot, intersections.end(),
                     [](auto const & l, auto const & r) {
                         return l.time() < r.time();
                     });
    return *pivot;
}

bool Sphere::compare(WorldObject const & object) const noexcept
{
    return dynamic_cast<Sphere const *>(&object) != nullptr;
}

Sphere Sphere::GlassSphere() noexcept
{
    return Sphere{Material{{0., 0., 0.}, .1, .9, .9, 200., 0., 1., 1.5}};
}

SurfaceComputations
Intersection::precompute(Ray const & ray,
                         std::vector<Intersection> const & xs) const noexcept
{
    SurfaceComputations result;
    result.time = t_;
    result.object = object_;
    result.point = ray.origin() + ray.direction() * t_;
    result.eyeVector = -ray.direction();
    result.normalVector = result.object->normalAt(result.point);
    if(result.normalVector.dot(result.eyeVector) < 0.)
    {
        result.isInside = true;
        result.normalVector = -result.normalVector;
    }
    else
        result.isInside = false;
    result.overPoint = result.point +
        result.normalVector * std::numeric_limits<double>::epsilon() * 16384.;
    result.underPoint = result.point -
        result.normalVector * std::numeric_limits<double>::epsilon() * 16348;
    setWtoOne(result.point);
    setWtoOne(result.overPoint);
    setWtoOne(result.underPoint);
    result.reflectVector = ray.direction().reflect(result.normalVector);

    if(xs.empty())
    {
        result.n1 = 1.;
        result.n2 = 1.;
    }
    else
    {
        std::vector<WorldObject const *> containers;
        for(auto const & intersection : xs)
        {
            if(operator==(intersection))
            {
                result.n1 = containers.empty() ?
                    1. :
                    containers.back()->material().refractiveIndex();
            }

            auto obj = std::find(containers.begin(), containers.end(),
                                 intersection.object());
            if(obj == containers.end())
                containers.push_back(intersection.object());
            else
                containers.erase(obj);

            if(operator==(intersection))
            {
                result.n2 = containers.empty() ?
                    1. :
                    containers.back()->material().refractiveIndex();
                break;
            }
        }
    }
    return result;
}

Plane::Plane(Material const & mat, Transform const & trns) noexcept :
    WorldObject{mat, trns}
{
}
Plane::Plane(Material const & mat) noexcept :
    WorldObject{mat, Transform::Identity()}
{
}
Plane::Plane(Transform const & trns) noexcept : WorldObject{Material{}, trns}
{
}
Plane::Plane() noexcept
{
}

Tuple Plane::findNormal(Tuple const & point) const noexcept
{
    assert(point.isPoint());
    return Tuple::Vector(0., 1., 0.);
}

bool Plane::compare(WorldObject const & object) const noexcept
{
    return dynamic_cast<Plane const *>(&object) != nullptr;
}

std::vector<Intersection> Plane::findIntersections(Ray const & ray) const
{
    std::vector<Intersection> result;
    // No slope? Parallel or even coplanar.
    if(floatCompare(ray.direction()(1), 0.))
        return result;
    // Otherwise there *will* be a hit, since the plane is endless.
    auto t = -ray.origin()(1) / ray.direction()(1);
    result.emplace_back(this, t);
    return result;
}

Cube::Cube(Material const & mat, Transform const & trns) noexcept :
    WorldObject{mat, trns}
{
}
Cube::Cube(Material const & mat) noexcept :
    WorldObject{mat, Transform::Identity()}
{
}
Cube::Cube(Transform const & trns) noexcept : WorldObject{Material{}, trns}
{
}

Tuple Cube::findNormal(Tuple const & point) const noexcept
{
    assert(point.isPoint());
    auto maxc =
        std::max({std::abs(point(0)), std::abs(point(1)), std::abs(point(2))});
    if(maxc == std::abs(point(0)))
        return Tuple::Vector(point(0), 0., 0.);
    if(maxc == std::abs(point(1)))
        return Tuple::Vector(0., point(1), 0.);
    return Tuple::Vector(0., 0., point(2));
}

bool Cube::compare(WorldObject const & object) const noexcept
{
    return dynamic_cast<Cube const *>(&object) != nullptr;
}

std::vector<Intersection> Cube::findIntersections(Ray const & ray) const
{
    auto [xtmin, xtmax] = checkAxis(ray.origin()(0), ray.direction()(0));
    auto [ytmin, ytmax] = checkAxis(ray.origin()(1), ray.direction()(1));
    auto [ztmin, ztmax] = checkAxis(ray.origin()(2), ray.direction()(2));

    auto tmin = std::max({xtmin, ytmin, ztmin});
    auto tmax = std::min({xtmax, ytmax, ztmax});

    std::vector<Intersection> result;
    if(tmin <= tmax)
    {
        result.emplace_back(this, tmin);
        result.emplace_back(this, tmax);
    }
    return result;
}

Cylinder::Cylinder(Transform const & trns) noexcept :
    WorldObject{Material{}, trns}
{
}
Cylinder::Cylinder(Material const & mat) noexcept :
    WorldObject{mat, Transform::Identity()}
{
}
Cylinder::Cylinder(Material const & mat, Transform const & trns) noexcept :
    WorldObject{mat, trns}
{
}
Cylinder::Cylinder(Material const & mat, Transform const & trns,
                   double minimum, double maximum, bool closed) noexcept :
    WorldObject{mat, trns},
    minimum_{minimum},
    maximum_{maximum},
    closed_{closed}
{
}

double Cylinder::minimum() const noexcept
{
    return minimum_;
}
double Cylinder::maximum() const noexcept
{
    return maximum_;
}
bool Cylinder::isClosed() const noexcept
{
    return closed_;
}

void Cylinder::setMinimum(double minimum) noexcept
{
    minimum_ = minimum;
}
void Cylinder::setMaximum(double maximum) noexcept
{
    maximum_ = maximum;
}
void Cylinder::setClosed(bool closed) noexcept
{
    closed_ = closed;
}

bool Cylinder::compare(WorldObject const & object) const noexcept
{
    auto rhs = dynamic_cast<Cylinder const *>(&object);
    return rhs != nullptr && floatCompare(minimum_, rhs->minimum()) &&
        floatCompare(maximum_, rhs->maximum());
}

Tuple Cylinder::findNormal(Tuple const & point) const noexcept
{
    static constexpr int ULPs = 4;
    auto dist = std::pow(point(0), 2.) + std::pow(point(2), 2.);
    if(dist < 1. &&
       point(1) >= (maximum_ - ULPs * std::numeric_limits<double>::epsilon()))
        return Tuple::Vector(0., 1., 0.);
    if(dist < 1. &&
       point(1) <= (minimum_ + ULPs * std::numeric_limits<double>::epsilon()))
        return Tuple::Vector(0., -1., 0.);

    return Tuple::Vector(point(0), 0., point(2));
}

std::vector<Intersection> Cylinder::findIntersections(Ray const & ray) const
{
    std::vector<Intersection> result;
    auto a = std::pow(ray.direction()(0), 2.) +
        std::pow(ray.direction()(2), 2.);
    if(!floatCompare(a, 0.))
    {
        auto b = 2. * ray.origin()(0) * ray.direction()(0) +
            2 * ray.origin()(2) * ray.direction()(2);
        auto c = std::pow(ray.origin()(0), 2.) +
            std::pow(ray.origin()(2), 2.) - 1.;

        auto discriminant = std::pow(b, 2.) - 4 * a * c;
        if(discriminant < 0.)
            return result;

        auto tc = std::sqrt(discriminant);
        auto t0 = (-b - tc) / (2. * a);
        auto t1 = (-b + tc) / (2. * a);
        std::tie(t0, t1) = std::minmax(t0, t1);

        auto y0 = ray.origin()(1) + t0 * ray.direction()(1);
        auto y1 = ray.origin()(1) + t1 * ray.direction()(1);

        if(minimum_ < y0 && y0 < maximum_)
            result.emplace_back(this, t0);
        if(minimum_ < y1 && y1 < maximum_)
            result.emplace_back(this, t1);
    }
    auto xsCaps = intersectCaps(ray);
    result.insert(result.end(), xsCaps.begin(), xsCaps.end());
    std::sort(result.begin(), result.end(),
              [](auto const & l, auto const & r) {
                  return l.time() < r.time();
              });

    return result;
}

bool Cylinder::checkCap(Ray const & ray, double t) const noexcept
{
    auto x = ray.origin()(0) + t * ray.direction()(0);
    auto z = ray.origin()(2) + t * ray.direction()(2);
    return (std::pow(x, 2.) + std::pow(z, 2.)) <= 1.;
}

std::vector<Intersection> Cylinder::intersectCaps(Ray const & ray) const
{
    std::vector<Intersection> result;
    if(!closed_ || floatCompare(ray.direction()(1), 0.))
        return result;

    auto t = (minimum_ - ray.origin()(1)) / ray.direction()(1);
    if(checkCap(ray, t))
        result.emplace_back(this, t);

    t = (maximum_ - ray.origin()(1)) / ray.direction()(1);
    if(checkCap(ray, t))
        result.emplace_back(this, t);

    return result;
}

} // namespace RT

