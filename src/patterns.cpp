#include "patterns.hpp"

#include "worldobjects.hpp"
#include "utility.hpp"

#include <cassert>
#include <cmath>

namespace RT
{
Pattern::Pattern(Color const & c1, Color const & c2, Transform const & trns) noexcept :
    c1_{c1},
    c2_{c2},
    transform_{trns}
{
}

Pattern::Pattern(Pattern const & rhs) noexcept :
    c1_{rhs.c1_},
    c2_{rhs.c2_},
    transform_{rhs.transform_}
{
}

Color Pattern::color1() const noexcept
{
    return getColor1();
}
Color Pattern::color2() const noexcept
{
    return getColor2();
}

Transform Pattern::transform() const noexcept
{
    return getTransform();
}
void Pattern::setTransform(Transform const & trns) noexcept
{
    transform_ = trns;
}

std::unique_ptr<Pattern> Pattern::clone() const
{
    return std::unique_ptr<Pattern>(createClone());
}

Color Pattern::colorAt(Tuple const & point) const noexcept
{
    assert(point.isPoint());
    return findColorAt(transform().inversed() * point);
}
Color Pattern::colorAt(WorldObject const & object, Tuple const & point) const noexcept
{
    assert(point.isPoint());
    return colorAt(object.transform().inversed() * point);
}
Color Pattern::getColor1() const noexcept
{
    return c1_;
}
Color Pattern::getColor2() const noexcept
{
    return c2_;
}
Transform Pattern::getTransform() const noexcept
{
    return transform_;
}

StripePattern::StripePattern(Color const & c1, Color const & c2) noexcept :
    Pattern{c1, c2}
{
}

StripePattern::StripePattern(StripePattern const & rhs) noexcept :
    Pattern{rhs}
{
}

Color StripePattern::findColorAt(Tuple const & point) const noexcept
{
    assert(point.isPoint());

    return static_cast<int>(std::floor(point(0))) % 2 == 0 ? color1() :
                                                             color2();
}

StripePattern * StripePattern::createClone() const
{
    return new StripePattern{*this};
}

GradientPattern::GradientPattern(Color const & c1, Color const & c2) noexcept :
    Pattern{c1, c2}
{
}

GradientPattern::GradientPattern(GradientPattern const & rhs) noexcept :
    Pattern{rhs}
{
}

Color GradientPattern::findColorAt(Tuple const & point) const noexcept
{
    assert(point.isPoint());
    auto distance = color2() - color1();
    auto fraction = point(0) - std::floor(point(0));
    return color1() + distance * fraction;
}

GradientPattern * GradientPattern::createClone() const
{
    return new GradientPattern{*this};
}

RingPattern::RingPattern(Color const & c1, Color const & c2) noexcept :
    Pattern{c1, c2}
{
}
RingPattern::RingPattern(RingPattern const & rhs) noexcept :
    Pattern{rhs}
{
}

Color RingPattern::findColorAt(Tuple const & point) const noexcept
{
    assert(point.isPoint());
    return static_cast<int>(std::floor(std::hypot(point(0), point(2)))) % 2 ==
            0 ?
        color1() :
        color2();
}

RingPattern * RingPattern::createClone() const
{
    return new RingPattern{*this};
}

CheckersPattern::CheckersPattern(Color const & c1, Color const & c2) noexcept :
    Pattern{c1, c2}
{
}

Color CheckersPattern::findColorAt(Tuple const & point) const noexcept
{
    if((static_cast<int>(
           std::floor(point(0) + std::numeric_limits<double>::epsilon() * 4) +
           std::floor(point(1) + std::numeric_limits<double>::epsilon() * 4) +
           std::floor(point(2) + std::numeric_limits<double>::epsilon() * 4))) %
           2 ==
       0)
        return color1();
    return color2();
}

CheckersPattern * CheckersPattern::createClone() const
{
    return new CheckersPattern{*this};
}

UVWrapPattern::UVWrapPattern(Pattern const & pattern) :
    Pattern{Color::Black(), Color::Black()},
    pattern_{pattern.clone()}
{
}

UVWrapPattern::UVWrapPattern(UVWrapPattern const & rhs) :
    Pattern{Color::Black(), Color::Black()},
    pattern_{rhs.pattern_->clone()}
{
}

Color UVWrapPattern::findColorAt(Tuple const & point) const noexcept
{
    auto d = transform() * point - RT::Tuple::Point(0., 0., 0.);
    d.normalize();

    auto u = .5 + (std::atan2(d(2), d(0)) / (2. * PI()));
    auto v = .5 - std::asin(d(1)) / PI();
    return pattern_->colorAt(RT::Tuple::Point(u, v, 0.));
}

UVWrapPattern * UVWrapPattern::createClone() const
{
    return new UVWrapPattern{*this};
}

Color UVWrapPattern::getColor1() const noexcept
{
    return pattern_->color1();
}
Color UVWrapPattern::getColor2() const noexcept
{
    return pattern_->color2();
}
Transform UVWrapPattern::getTransform() const noexcept
{
    return pattern_->transform();
}

} // namespace RT

