#include "transforms.hpp"

#include <cmath>

namespace RT
{
Transform::Transform(Matrix4 matrix) noexcept : matrix_{std::move(matrix)}
{
}

bool Transform::operator==(Transform const & rhs) const
{
    return matrix_ == rhs.matrix_;
}
bool Transform::operator==(Matrix4 const & rhs) const
{
    return matrix_ == rhs;
}

Tuple Transform::operator*(Tuple const & t) const noexcept
{
    return matrix_ * t;
}

Transform Transform::operator*(Transform const & rhs) const noexcept
{
    return matrix_ * rhs.matrix_;
}

Transform Transform::transposed() const noexcept
{
    return {matrix_.transposed()};
}

Transform Transform::inversed() const noexcept
{
    return {matrix_.inversed()};
}

Transform Transform::Translation(scalar_t x, scalar_t y, scalar_t z) noexcept
{
    auto tr = Matrix4::Identity();
    auto iter = std::begin(tr);
    std::advance(iter, 3);
    *iter = x;
    std::advance(iter, 4);
    *iter = y;
    std::advance(iter, 4);
    *iter = z;
    return {tr};
}

Transform Transform::Scaling(scalar_t x, scalar_t y, scalar_t z) noexcept
{
    return Matrix4{x,  0., 0., 0., 0., y,  0., 0.,
                   0., 0., z,  0., 0., 0., 0., 1.};
}

Transform Transform::RotationX(scalar_t radians) noexcept
{
    return Matrix4{1.,
                   0.,
                   0.,
                   0.,
                   0.,
                   std::cos(radians),
                   -std::sin(radians),
                   0.,
                   0.,
                   std::sin(radians),
                   std::cos(radians),
                   0.,
                   0.,
                   0.,
                   0.,
                   1.};
}
Transform Transform::RotationY(scalar_t radians) noexcept
{
    return Matrix4{
        std::cos(radians),  0., std::sin(radians), 0., 0., 1., 0., 0.,
        -std::sin(radians), 0., std::cos(radians), 0., 0., 0., 0., 1.};
}
Transform Transform::RotationZ(scalar_t radians) noexcept
{
    return Matrix4{std::cos(radians),
                   -std::sin(radians),
                   0.,
                   0.,
                   std::sin(radians),
                   std::cos(radians),
                   0.,
                   0.,
                   0.,
                   0.,
                   1.,
                   0.,
                   0.,
                   0.,
                   0.,
                   1.};
}

Transform Transform::Shearing(scalar_t xy, scalar_t xz, scalar_t yx,
                              scalar_t yz, scalar_t zx, scalar_t zy) noexcept
{
    return Matrix4{1., xy, xz, 0., yx, 1., yz, 0.,
                   zx, zy, 1., 0., 0., 0., 0., 1.};
}

Transform Transform::Identity() noexcept
{
    return Matrix4::Identity();
}

Transform Transform::View(Tuple const & from, Tuple const & to,
                          Tuple const & up) noexcept
{
    auto forward = to - from;
    forward.normalize();
    auto left = forward.cross(up.normalized());
    auto trueUp = left.cross(forward);
    auto mat = Matrix4{left(0),     left(1),     left(2),     0.,
                       trueUp(0),   trueUp(1),   trueUp(2),   0.,
                       -forward(0), -forward(1), -forward(2), 0.,
                       0.,          0.,          0.,          1.};
    return Transform{mat} * Translation(-from(0), -from(1), -from(2));
}

std::ostream & operator<<(std::ostream & os, Transform const & trns)
{
    os << trns.matrix_;
    return os;
}

} // namespace RT

