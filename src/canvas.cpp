#include "canvas.hpp"

#include <algorithm>
#include <locale>
#include <ostream>
#include <sstream>

#include <boost/algorithm/string.hpp>

namespace
{
std::string textwrap(std::string const & line, std::size_t maxLenght)
{
    std::ostringstream result;
    std::string tmp;
    char last = '\0';
    std::size_t i = 0;

    for(auto & c : line)
    {
        if(++i == maxLenght)
        {
            boost::trim_left(tmp);
            result << '\n' << tmp;
            i = tmp.size();
            tmp.clear();
        }
        else if(std::isspace(c, std::locale::classic()) &&
                !std::isspace(last, std::locale::classic()))
        {
            result << tmp;
            tmp.clear();
        }
        tmp += c;
        last = c;
    }
    if(tmp.size())
        result << tmp;
    return result.str();
}
} // namespace

namespace RT
{
Canvas::Canvas(size_type width, size_type height) :
    buffer_{},
    width_{width},
    height_{height}
{
    buffer_.resize(height_);
    for(auto & row : buffer_)
        row.resize(width_);
}

Canvas::size_type Canvas::width() const noexcept
{
    return width_;
}
Canvas::size_type Canvas::height() const noexcept
{
    return height_;
}

Color Canvas::at(size_type column, size_type row) const noexcept
{
    assert(column < width_);
    assert(row < height_);
    return buffer_[row][column];
}
void Canvas::set(size_type column, size_type row, Color const & color) noexcept
{
    assert(column < width_);
    assert(row < height_);
    buffer_[row][column] = color;
};

void Canvas::writeAsPPM(std::ostream & out) const
{
    out << "P3\n";
    out << width_ << ' ' << height_ << '\n';
    out << 255 << '\n';
    std::for_each(std::cbegin(buffer_), std::cend(buffer_), [&out](auto & row) {
        std::ostringstream line;
        for(auto & color : row)
        {
            line << std::clamp(static_cast<int>(256 * color.red()), 0, 255)
                 << ' '
                 << std::clamp(static_cast<int>(256 * color.green()), 0, 255)
                 << ' '
                 << std::clamp(static_cast<int>(256 * color.blue()), 0, 255)
                 << ' ';
        }
        out << textwrap(boost::trim_right_copy(line.str()), 70) << '\n';
    });
}

} // namespace RT

