#include "tuples.hpp"

#include "utility.hpp"

#include <algorithm>
#include <numeric>

#include <cassert>

namespace RT
{
Tuple::Tuple(scalar_t x, scalar_t y, scalar_t z, scalar_t w) noexcept :
    values_{x, y, z, w}
{
}

Tuple & Tuple::operator+=(Tuple const & rhs)
{
    std::transform(std::begin(values_), std::end(values_),
                   std::cbegin(rhs.values_), std::begin(values_),
                   std::plus<>{});
    return *this;
}

Tuple & Tuple::operator-=(Tuple const & rhs)
{
    return this->operator+=(-rhs);
}

Tuple & Tuple::operator*=(scalar_t rhs)
{
    std::transform(std::begin(values_), std::end(values_), std::begin(values_),
                   [&rhs](auto const & val) { return val * rhs; });
    return *this;
}

Tuple & Tuple::operator/=(scalar_t rhs)
{
    return operator*=(1.l / rhs);
}

Tuple Tuple::operator-() const noexcept
{
    return {-values_[0], -values_[1], -values_[2], -values_[3]};
}

Tuple::scalar_t Tuple::operator()(std::size_t index) const noexcept
{
    return values_[index];
}

bool Tuple::operator==(Tuple const & rhs) const noexcept
{
    return floatCompare(values_[0], rhs.values_[0]) &&
        floatCompare(values_[1], rhs.values_[1]) &&
        floatCompare(values_[2], rhs.values_[2]) &&
        floatCompare(values_[3], rhs.values_[3]);
}

bool Tuple::isPoint() const
{
    return floatCompare(values_.back(), scalar_t(1.));
}

bool Tuple::isVector() const
{
    return !isPoint() && !(values_.back() > 1.) && !(values_.back() < 0.);
}

Tuple::scalar_t Tuple::norm() const
{
    auto result = std::accumulate(std::begin(values_), std::end(values_), 0.l,
                                  [](scalar_t lhs, scalar_t rhs) {
                                      return lhs + std::pow(rhs, 2.l);
                                  });
    result = std::sqrt(result);
    return result;
}

void Tuple::normalize()
{
    // Too cryptic?
    operator/=(norm());
}

Tuple Tuple::normalized() const
{
    Tuple result(*this);
    result.normalize();
    return result;
}

Tuple::scalar_t Tuple::dot(Tuple const & rhs) const
{
    return std::inner_product(std::cbegin(values_), std::cend(values_),
                              std::cbegin(rhs.values_), 0.);
}

Tuple Tuple::cross(Tuple const & rhs) const
{
    assert(isVector() && rhs.isVector());
    return {values_[1] * rhs.values_[2] - values_[2] * rhs.values_[1],
            values_[2] * rhs.values_[0] - values_[0] * rhs.values_[2],
            values_[0] * rhs.values_[1] - values_[1] * rhs.values_[0], 0.};
}

Tuple::iterator Tuple::begin()
{
    return std::begin(values_);
}
Tuple::iterator Tuple::end()
{
    return std::end(values_);
}
Tuple::const_iterator Tuple::begin() const
{
    return std::cbegin(values_);
}
Tuple::const_iterator Tuple::end() const
{
    return std::cend(values_);
}

Tuple Tuple::Point(scalar_t x, scalar_t y, scalar_t z) noexcept
{
    return {x, y, z, 1.};
}

Tuple Tuple::Vector(scalar_t x, scalar_t y, scalar_t z) noexcept
{
    return {x, y, z, 0.};
}

Tuple operator+(Tuple lhs, Tuple const & rhs)
{
    return lhs += rhs;
}

Tuple operator-(Tuple lhs, Tuple const & rhs)
{
    return lhs -= rhs;
}

Tuple operator*(Tuple lhs, Tuple::scalar_t rhs)
{
    return lhs *= rhs;
}

Tuple operator*(Tuple::scalar_t lhs, Tuple rhs)
{
    return rhs *= lhs;
}

Tuple operator/(Tuple lhs, Tuple::scalar_t rhs)
{
    return lhs /= rhs;
}

Tuple Tuple::reflect(Tuple const & normal) const
{
    assert(normal.isVector() && isVector());
    Tuple result{*this};
    result -= normal * 2. * result.dot(normal);
    return result;
}

std::ostream & operator<<(std::ostream & os, Tuple const & value)
{
    os << '(' << value(0) << ", " << value(1) << ", " << value(2) << ", "
       << value(3) << ')';
    return os;
}
} // namespace RT

